﻿
namespace UtilityLayer.SharedEnums
{
    public enum CrudImplementation
    {
        Create = 1,
        Read = 2,
        Update = 3,
        Delete = 4
    }

    public enum Status
    {
        AlreadyExist = 10,
        LoginSuccess = 11,
        LoginFail = 12,
        Error = 13
    }
}
