﻿using Microsoft.Extensions.Configuration;
using System.IO;

namespace UtilityLayer.SharedFunctions
{
  public static class GetAppSettings
    {
        public static IConfiguration AppSetting { get; }

        static GetAppSettings()
        {
            AppSetting = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json")
                    .Build();
        }


        public static string GetAESKey()
        {
            return GetAppSettings.AppSetting["AESKey"];
        }
    }
}
