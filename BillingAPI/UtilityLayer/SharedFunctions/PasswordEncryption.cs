﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace UtilityLayer.SharedFunctions
{
   public static class PasswordEncryption
    {
        public static string SHA512Encrypt(string password)
        {
            try
            {
                UnicodeEncoding uEncode = new UnicodeEncoding();
                byte[] bytPassword = uEncode.GetBytes(password);
                SHA512Managed sha = new SHA512Managed();
                byte[] hash = sha.ComputeHash(bytPassword).Take(25).ToArray();
                return Convert.ToBase64String(hash);
            }
            catch
            {
                throw;
            }
        }
    }
}
