﻿

namespace UtilityLayer.SharedConstants
{
   public static class Constants
    {
        public static readonly string Gender = "Gender";
        public static readonly string BloodGroup = "BloodGroup";
        public static readonly string MaritalStatus = "MaritalStatus";
        public static readonly string Null = "null";
    }
}
