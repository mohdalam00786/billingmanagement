﻿using ProjectModels.RequestModels;
using ProjectModels.ResponseModels;
using System.Threading.Tasks;

namespace ProjectBusinessLayer.IService
{
    public interface IEmployeeDetailsService
    {
        Task<SuccessResponseModel> SaveEmployeeDetails(EmployeeDetailsRequestModel employeeDetailsRequestModel);
    }
}
