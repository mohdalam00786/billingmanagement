﻿using ProjectModels.RequestModels;
using ProjectModels.ResponseModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectBusinessLayer.IService
{
    public interface IAuthenticationService
    {
        Task<AuthenticationResponseModel> AuthenticationAsync(AuthenticationRequestModel loginModel);
        Task<List<CentreResponseModel>> GetAllCentreForLoginAsync();
    }
}
