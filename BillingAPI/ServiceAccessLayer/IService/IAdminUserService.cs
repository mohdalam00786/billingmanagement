﻿using ProjectModels.RequestModels;
using ProjectModels.ResponseModels;
using System.Threading.Tasks;

namespace ProjectBusinessLayer.IService
{
    public interface IAdminUserService
    {
        #region User master implementation
        Task<GetAllUserResponseModel> GetAllUsersAsync();
        Task<UserDropdownResponseModel> GetUserMasterDataAsync();
        Task<SuccessResponseModel> SaveUserInfomationAsync(UserRequestModel userRequestModel);
        Task<SuccessResponseModel> DeleteUserByIdAsync(DeleteRecordRequestModel deleteRecordRequestModel);
        #endregion
    }
}