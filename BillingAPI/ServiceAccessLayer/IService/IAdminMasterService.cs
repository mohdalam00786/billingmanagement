﻿using ProjectModels.Dto;
using ProjectModels.RequestModels;
using ProjectModels.ResponseModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectBusinessLayer.IService
{
    public interface IAdminMasterService
    {
        #region Centre master implementation
        Task<List<GetAllCentreResponseModel>> GetAllCentresAsync();
        Task<SuccessResponseModel> SaveCentreInfomationAsync(CentreMasterRequestModel centreMasterRequestModel);
        Task<SuccessResponseModel> DeleteCentreByIdAsync(DeleteRecordRequestModel deleteRecordRequestModel);
        #endregion

        #region Item master implementation

        Task<GetItemMasterResponseModel> GetItemMasterAsync();

        Task<SuccessResponseModel> SaveItemMasterInformationAsync(ItemMasterRequestModel ItemMasterRequestModel);

        Task<SuccessResponseModel> DeleteItemMasterByIdAsync(DeleteItemMasterRequestModel deleteItemMasterRequestModel);

        #endregion

        #region Client master implementation

        Task<GetClientMasterResponseModel> GetClientMasterAsync();

        Task<SuccessResponseModel> SaveClientMasterInformationAsync(ClientMasterRequestModel ClientMasterRequestModel);

        Task<SuccessResponseModel> DeleteClientMasterByIdAsync(DeleteClientMasterRequestModel deleteClientMasterRequestModel);



        #endregion

        #region Role master implementation
        Task<List<RolesResponseModel>> GetAllRolesAsync();
        Task<SuccessResponseModel> SaveRolesInfomationAsync(RoleMasterRequestModel roleMasterRequestModel);
        Task<SuccessResponseModel> DeleteRoleByIdAsync(DeleteRecordRequestModel deleteRecordRequestModel);
        #endregion

        #region Config master implementation
        Task<List<ConfigMasterResponseModel>> GetAllConfigAsync();
        Task<SuccessResponseModel> SaveConfigInfomationAsync(ConfigMasterRequestModel configMasterRequestModel);
        Task<SuccessResponseModel> DeleteConfigByIdAsync(DeleteRecordRequestModel deleteRecordRequestModel);
        #endregion
    }
}
