﻿using ProjectModels.RequestModels;
using ProjectModels.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectBusinessLayer.IService
{
    public interface IBillingService
    {
        #region Billing implementation

        Task<GetBillingResponseModel> GetBillingAsync();
        Task<CustomerDetailsResponseModel> GetCustomerDetailsByMobileNo(string mobileNumber);

        Task<SuccessResponseModel> SaveBillingInformationAsync(BillingRequestModel billingRequestModel);

        Task<SuccessResponseModel> DeleteBillingByIdAsync(DeleteBillingtRequestModel deleteBillingRequestModel);



        #endregion

    }
}
