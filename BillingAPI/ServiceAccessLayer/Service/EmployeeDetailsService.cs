﻿using AutoMapper;
using DataAccessLayer.UnitOfWork;
using ProjectBusinessLayer.IService;
using ProjectModels.RequestModels;
using ProjectModels.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityLayer.SharedFunctions;

namespace ProjectBusinessLayer.Service
{
    public class EmployeeDetailsService : IEmployeeDetailsService
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;
        public EmployeeDetailsService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<SuccessResponseModel> SaveEmployeeDetails(EmployeeDetailsRequestModel employeeDetailsRequestModel)
        {
            try
            {
                return await _unitOfWork.employeeDetailsRepository.SaveEmployeeDetails(employeeDetailsRequestModel).ConfigureAwait(false);
            }
            catch
            {
                throw;
            }
        }
    }
}
