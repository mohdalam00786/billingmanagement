﻿using AutoMapper;
using DataAccessLayer.UnitOfWork;
using ProjectBusinessLayer.IService;
using ProjectModels.Dto;
using ProjectModels.RequestModels;
using ProjectModels.ResponseModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UtilityLayer.SharedConstants;
using UtilityLayer.SharedFunctions;

namespace ProjectBusinessLayer.Service
{
    public class AdminMasterService : IAdminMasterService
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;
        public AdminMasterService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        #region Centre master implementation
        public async Task<List<GetAllCentreResponseModel>> GetAllCentresAsync()
        {
            try
            {
                return await _unitOfWork.adminMasterRepository
                    .GetAllCentresAsync().ConfigureAwait(false);
            }
            catch
            {
                throw;
            }
        }
        public async Task<SuccessResponseModel> SaveCentreInfomationAsync(CentreMasterRequestModel centreMasterRequestModel)
        {
            try
            {
                centreMasterRequestModel = new CentreMasterRequestModel
                {
                    CentreId = AESDecryption.ReturnDecryptString(centreMasterRequestModel.CentreId),
                    CentreName = AESDecryption.ReturnDecryptString(centreMasterRequestModel.CentreName),
                    CentreCode = AESDecryption.ReturnDecryptString(centreMasterRequestModel.CentreCode),
                    Website = AESDecryption.ReturnDecryptString(centreMasterRequestModel.Website),
                    Address = AESDecryption.ReturnDecryptString(centreMasterRequestModel.Address),
                    MobileNumber = AESDecryption.ReturnDecryptString(centreMasterRequestModel.MobileNumber),
                    LandlineNumber = AESDecryption.ReturnDecryptString(centreMasterRequestModel.LandlineNumber),
                    EmailId = AESDecryption.ReturnDecryptString(centreMasterRequestModel.EmailId),
                    IPAddress = centreMasterRequestModel.IPAddress,
                    LoggedInUserId = AESDecryption.ReturnDecryptString(centreMasterRequestModel.LoggedInUserId),

                };
                long centreId = 0;
                if (centreMasterRequestModel.CentreId != Constants.Null)
                {
                    centreId = Convert.ToInt64(centreMasterRequestModel.CentreId);
                }
                CentreMasterDto centreMasterDto = _mapper.Map<CentreMasterDto>(centreMasterRequestModel);
                return await _unitOfWork.adminMasterRepository.SaveCentreInfomationAsync(centreMasterDto, centreId).ConfigureAwait(false);
            }
            catch
            {
                throw;
            }
        }

        public async Task<SuccessResponseModel> DeleteCentreByIdAsync(DeleteRecordRequestModel deleteRecordRequestModel)
        {
            try
            {
                deleteRecordRequestModel = new DeleteRecordRequestModel
                {
                    LoggedInUserId = AESDecryption.ReturnDecryptString(deleteRecordRequestModel.LoggedInUserId),
                    DeleteRecordId = AESDecryption.ReturnDecryptString(deleteRecordRequestModel.DeleteRecordId),
                };

                return await _unitOfWork.adminMasterRepository.DeleteCentreByIdAsync(deleteRecordRequestModel).ConfigureAwait(false);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Item master implementation

        public async Task<GetItemMasterResponseModel> GetItemMasterAsync()
        {
            try
            {
                return await _unitOfWork.adminMasterRepository.GetItemMasterAsync().ConfigureAwait(false);
            }
            catch
            {
                throw;
            }
        }


        public async Task<SuccessResponseModel> SaveItemMasterInformationAsync(ItemMasterRequestModel ItemMasterRequestModel)
        {
            try
            {

                return await _unitOfWork.adminMasterRepository.SaveItemMasterInformationAsync(ItemMasterRequestModel).ConfigureAwait(false);
            }
            catch
            {
                throw;
            }
        }

        public async Task<SuccessResponseModel> DeleteItemMasterByIdAsync(DeleteItemMasterRequestModel deleteItemMasterRequestModel)
        {
            try
            {
                //deleteRecordRequestModel = new DeleteRecordRequestModel
                //{
                //LoggedInUserId = AESDecryption.ReturnDecryptString(deleteRecordRequestModel.LoggedInUserId),
                //DeleteRecordId = AESDecryption.ReturnDecryptString(deleteRecordRequestModel.DeleteRecordId),
                //};

                return await _unitOfWork.adminMasterRepository.DeleteItemMasterByIdAsync(deleteItemMasterRequestModel).ConfigureAwait(false);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Client master implementation

        public async Task<GetClientMasterResponseModel> GetClientMasterAsync()
        {
            try
            {
                return await _unitOfWork.clientMasterRepository.GetClientMasterAsync().ConfigureAwait(false);
            }
            catch
            {
                throw;
            }
        }


        public async Task<SuccessResponseModel> SaveClientMasterInformationAsync(ClientMasterRequestModel ClientMasterRequestModel)
        {
            try
            {

                return await _unitOfWork.clientMasterRepository.SaveClientMasterInformationAsync(ClientMasterRequestModel).ConfigureAwait(false);
            }
            catch
            {
                throw;
            }
        }

        public async Task<SuccessResponseModel> DeleteClientMasterByIdAsync(DeleteClientMasterRequestModel deleteClientMasterRequestModel)
        {
            try
            {
                //deleteRecordRequestModel = new DeleteRecordRequestModel
                //{
                //LoggedInUserId = AESDecryption.ReturnDecryptString(deleteRecordRequestModel.LoggedInUserId),
                //DeleteRecordId = AESDecryption.ReturnDecryptString(deleteRecordRequestModel.DeleteRecordId),
                //};

                return await _unitOfWork.clientMasterRepository.DeleteClientMasterByIdAsync(deleteClientMasterRequestModel).ConfigureAwait(false);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Role master implementation
        public async Task<List<RolesResponseModel>> GetAllRolesAsync()
        {
            try
            {
                return await _unitOfWork.adminMasterRepository.GetAllRolesAsync().ConfigureAwait(false);
            }
            catch
            {
                throw;
            }
        }
        public async Task<SuccessResponseModel> SaveRolesInfomationAsync(RoleMasterRequestModel roleMasterRequestModel)
        {
            try
            {
                roleMasterRequestModel = new RoleMasterRequestModel
                {
                    RoleId = AESDecryption.ReturnDecryptString(roleMasterRequestModel.RoleId),
                    RoleName = AESDecryption.ReturnDecryptString(roleMasterRequestModel.RoleName),
                    IPAddress = roleMasterRequestModel.IPAddress,
                    LoggedInUserId = AESDecryption.ReturnDecryptString(roleMasterRequestModel.LoggedInUserId),
                    RoleDescription = AESDecryption.ReturnDecryptString(roleMasterRequestModel.RoleDescription)
                };
                long roleId = 0;
                if (roleMasterRequestModel.RoleId != Constants.Null)
                {
                    roleId = Convert.ToInt64(roleMasterRequestModel.RoleId);
                }
                RoleMasterDto roleMasterDto = _mapper.Map<RoleMasterDto>(roleMasterRequestModel);
                return await _unitOfWork.adminMasterRepository.SaveRolesInfomationAsync(roleMasterDto, roleId).ConfigureAwait(false);
            }
            catch
            {
                throw;
            }
        }

        public async Task<SuccessResponseModel> DeleteRoleByIdAsync(DeleteRecordRequestModel deleteRecordRequestModel)
        {
            try
            {
                deleteRecordRequestModel = new DeleteRecordRequestModel
                {
                    LoggedInUserId = AESDecryption.ReturnDecryptString(deleteRecordRequestModel.LoggedInUserId),
                    DeleteRecordId = AESDecryption.ReturnDecryptString(deleteRecordRequestModel.DeleteRecordId),
                };
                return await _unitOfWork.adminMasterRepository.DeleteRoleByIdAsync(deleteRecordRequestModel).ConfigureAwait(false);
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Config master implementation
        public async Task<List<ConfigMasterResponseModel>> GetAllConfigAsync()
        {
            try
            {
                return await _unitOfWork.adminMasterRepository.GetAllConfigAsync().ConfigureAwait(false);
            }
            catch
            {
                throw;
            }
        }

        public async Task<SuccessResponseModel> SaveConfigInfomationAsync(ConfigMasterRequestModel configMasterRequestModel)
        {
            try
            {
                configMasterRequestModel = new ConfigMasterRequestModel
                {
                    ConfigId = AESDecryption.ReturnDecryptString(configMasterRequestModel.ConfigId),
                    ConfigKey = AESDecryption.ReturnDecryptString(configMasterRequestModel.ConfigKey),
                    ConfigValue = AESDecryption.ReturnDecryptString(configMasterRequestModel.ConfigValue),
                    IPAddress = configMasterRequestModel.IPAddress,
                    LoggedInUserId = AESDecryption.ReturnDecryptString(configMasterRequestModel.LoggedInUserId),
                };
                long configId = 0;
                if (configMasterRequestModel.ConfigId != Constants.Null)
                {
                    configId = Convert.ToInt64(configMasterRequestModel.ConfigId);
                }
                ConfigMasterDto configMasterDto = _mapper.Map<ConfigMasterDto>(configMasterRequestModel);
                return await _unitOfWork.adminMasterRepository.SaveConfigInfomationAsync(configMasterDto, configId).ConfigureAwait(false);
            }
            catch
            {
                throw;
            }
        }

        public async Task<SuccessResponseModel> DeleteConfigByIdAsync(DeleteRecordRequestModel deleteRecordRequestModel)
        {
            try
            {
                deleteRecordRequestModel = new DeleteRecordRequestModel
                {
                    LoggedInUserId = AESDecryption.ReturnDecryptString(deleteRecordRequestModel.LoggedInUserId),
                    DeleteRecordId = AESDecryption.ReturnDecryptString(deleteRecordRequestModel.DeleteRecordId),
                };
                return await _unitOfWork.adminMasterRepository.DeleteConfigByIdAsync(deleteRecordRequestModel).ConfigureAwait(false);
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}
  