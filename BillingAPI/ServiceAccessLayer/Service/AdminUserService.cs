﻿using AutoMapper;
using DataAccessLayer.UnitOfWork;
using ProjectBusinessLayer.IService;
using ProjectModels.Dto;
using ProjectModels.RequestModels;
using ProjectModels.ResponseModels;
using System;
using System.Threading.Tasks;
using UtilityLayer.SharedConstants;
using UtilityLayer.SharedFunctions;

namespace ProjectBusinessLayer.Service
{
    public class AdminUserService : IAdminUserService
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;
        public AdminUserService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        #region User master implementation
        public async Task<GetAllUserResponseModel> GetAllUsersAsync()
        {
            try
            {
                return await _unitOfWork.adminUserRepository.GetAllUsersAsync().ConfigureAwait(false);
            }
            catch
            {
                throw;
            }
        }
        public async Task<UserDropdownResponseModel> GetUserMasterDataAsync()
        {
            try
            {
                return await _unitOfWork.adminUserRepository.GetUserMasterDataAsync().ConfigureAwait(false);
            }
            catch
            {
                throw;
            }
        }

        public async Task<SuccessResponseModel> SaveUserInfomationAsync(UserRequestModel userRequestModel)
        {
            try
            {
                //userRequestModel = new UserRequestModel
                //{
                //    UserId = AESDecryption.ReturnDecryptString(userRequestModel.UserId),
                //    UserName = AESDecryption.ReturnDecryptString(userRequestModel.UserName),
                //    DateOfBirth = AESDecryption.ReturnDecryptString(userRequestModel.DateOfBirth),
                //    Gender = AESDecryption.ReturnDecryptString(userRequestModel.Gender),
                //    BloodGroup = AESDecryption.ReturnDecryptString(userRequestModel.BloodGroup),
                //    MaritalStatus = AESDecryption.ReturnDecryptString(userRequestModel.MaritalStatus),
                //    SpouseName = AESDecryption.ReturnDecryptString(userRequestModel.SpouseName),
                //    FatherName = AESDecryption.ReturnDecryptString(userRequestModel.FatherName),
                //    MotherName = AESDecryption.ReturnDecryptString(userRequestModel.MotherName),
                //    GuardianName = AESDecryption.ReturnDecryptString(userRequestModel.GuardianName),
                //    PassportNumber = AESDecryption.ReturnDecryptString(userRequestModel.PassportNumber),
                //    AdhaarCardNumber = AESDecryption.ReturnDecryptString(userRequestModel.AdhaarCardNumber),
                //    PANCardNumber = AESDecryption.ReturnDecryptString(userRequestModel.PANCardNumber),
                //    PrimaryContactNumber = AESDecryption.ReturnDecryptString(userRequestModel.PrimaryContactNumber),
                //    SecondryContactNumber = AESDecryption.ReturnDecryptString(userRequestModel.SecondryContactNumber),
                //    LandlineNumber = AESDecryption.ReturnDecryptString(userRequestModel.LandlineNumber),
                //    EmergencyContactName = AESDecryption.ReturnDecryptString(userRequestModel.EmergencyContactName),
                //    EmergencyContactNumber = AESDecryption.ReturnDecryptString(userRequestModel.EmergencyContactNumber),
                //    EmailId = AESDecryption.ReturnDecryptString(userRequestModel.EmailId),
                //    PermanentAddress = AESDecryption.ReturnDecryptString(userRequestModel.PermanentAddress),
                //    LoginName = AESDecryption.ReturnDecryptString(userRequestModel.LoginName),
                //    Password = AESDecryption.ReturnDecryptString(userRequestModel.Password),
                //    Roles = AESDecryption.ReturnDecryptString(userRequestModel.Roles),
                //    Centres = AESDecryption.ReturnDecryptString(userRequestModel.Centres),
                //    LoggedInUserId = AESDecryption.ReturnDecryptString(userRequestModel.LoggedInUserId),

                //    IPAddress = userRequestModel.IPAddress
                //};
                long userId = 0;
                if (userRequestModel.UserId != Constants.Null)
                {
                    userId = Convert.ToInt64(userRequestModel.UserId);
                }
                userRequestModel.Password = PasswordEncryption.SHA512Encrypt(userRequestModel.Password);
                UserDto userDto = _mapper.Map<UserDto>(userRequestModel);

                return await _unitOfWork.adminUserRepository.SaveUserInfomationAsync(userDto, userId).ConfigureAwait(false);
            }
            catch
            {
                throw;
            }
        }


        public async Task<SuccessResponseModel> DeleteUserByIdAsync(DeleteRecordRequestModel deleteRecordRequestModel)
        {
            try
            {
                //deleteRecordRequestModel = new DeleteRecordRequestModel
                //{
                //    LoggedInUserId = AESDecryption.ReturnDecryptString(deleteRecordRequestModel.LoggedInUserId),
                //    DeleteRecordId = AESDecryption.ReturnDecryptString(deleteRecordRequestModel.DeleteRecordId),
                //};
                return await _unitOfWork.adminUserRepository.DeleteUserByIdAsync(deleteRecordRequestModel).ConfigureAwait(false);
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}
