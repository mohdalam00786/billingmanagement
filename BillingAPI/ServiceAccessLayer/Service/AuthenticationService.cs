﻿using DataAccessLayer.UnitOfWork;
using ProjectBusinessLayer.IService;
using ProjectModels.RequestModels;
using ProjectModels.ResponseModels;
using System.Collections.Generic;
using System.Threading.Tasks;
using UtilityLayer.SharedEnums;
using UtilityLayer.SharedFunctions;

namespace ProjectBusinessLayer.Service
{
    public class AuthenticationService : IAuthenticationService
    {

        private IUnitOfWork _unitOfWork;

        public AuthenticationService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<AuthenticationResponseModel> AuthenticationAsync(AuthenticationRequestModel authenticateRequestModel)
        {
            try
            {
                authenticateRequestModel = new AuthenticationRequestModel
                {
                    Centre = AESDecryption.ReturnDecryptString(authenticateRequestModel.Centre),
                    LoginName = AESDecryption.ReturnDecryptString(authenticateRequestModel.LoginName),
                    Password = AESDecryption.ReturnDecryptString(authenticateRequestModel.Password)
                };
                authenticateRequestModel.Password = PasswordEncryption.SHA512Encrypt(authenticateRequestModel.Password);
                var result = await _unitOfWork.authenticationRepository.AuthenticationAsync(authenticateRequestModel).ConfigureAwait(false);
                if (result.ResponseStatus == (int)Status.LoginSuccess)
                {
                    result.JsonWebToken = SharedFunctions.GenerateJSONWebToken(result.UserId, result.UserName);
                }
                return result;
            }
            catch
            {
                throw;
            }
        }
        public async Task<List<CentreResponseModel>> GetAllCentreForLoginAsync()
        {
            try
            {
                return await _unitOfWork.authenticationRepository
                    .GetAllCentreForLoginAsync().ConfigureAwait(false);
            }
            catch
            {
                throw;
            }
        }
    }
}
