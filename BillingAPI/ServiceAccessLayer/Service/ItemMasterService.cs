﻿using AutoMapper;
using DataAccessLayer.UnitOfWork;
using ProjectBusinessLayer.IService;
using ProjectModels.Dto;
using ProjectModels.RequestModels;
using ProjectModels.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityLayer.SharedFunctions;

namespace ProjectBusinessLayer.Service
{
    public class ItemMasterService : IItemMasterService
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;
        public ItemMasterService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
    }
}
        

