﻿using AutoMapper;
using DataAccessLayer.UnitOfWork;
using ProjectBusinessLayer.IService;
using ProjectModels.RequestModels;
using ProjectModels.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectBusinessLayer.Service
{
    public class BillingService : IBillingService
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;
        public BillingService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        #region Billing implementation

        public async Task<GetBillingResponseModel> GetBillingAsync()
        {
            try
            {
                return await _unitOfWork.billingRepository.GetBillingAsync().ConfigureAwait(false);
            }
            catch
            {
                throw;
            }
        }

        public async Task<CustomerDetailsResponseModel> GetCustomerDetailsByMobileNo(string mobileNumber)
        {
            try
            {
                return await _unitOfWork.billingRepository.GetCustomerDetailsByMobileNo(mobileNumber).ConfigureAwait(false);
            }
            catch
            {
                throw;
            }
        }


        public async Task<SuccessResponseModel> SaveBillingInformationAsync(BillingRequestModel BillingRequestModel)
        {
            try
            {

                return await _unitOfWork.billingRepository.SaveBillingInformationAsync(BillingRequestModel).ConfigureAwait(false);
            }
            catch
            {
                throw;
            }
        }

        public async Task<SuccessResponseModel> DeleteBillingByIdAsync(DeleteBillingtRequestModel deleteBillingRequestModel)
        {
            try
            {
                //deleteRecordRequestModel = new DeleteRecordRequestModel
                //{
                //LoggedInUserId = AESDecryption.ReturnDecryptString(deleteRecordRequestModel.LoggedInUserId),
                //DeleteRecordId = AESDecryption.ReturnDecryptString(deleteRecordRequestModel.DeleteRecordId),
                //};

                return await _unitOfWork.billingRepository.DeleteBillingByIdAsync(deleteBillingRequestModel).ConfigureAwait(false);
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}

