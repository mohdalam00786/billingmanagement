﻿using AutoMapper;
using DataAccessLayer.DBContext;
using Microsoft.EntityFrameworkCore;
using ProjectDataAccessLayer.Entities;
using ProjectDataAccessLayer.IRepository;
using ProjectModels.Dto;
using ProjectModels.RequestModels;
using ProjectModels.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityLayer.SharedEnums;

namespace ProjectDataAccessLayer.Repository
{
    public class EmployeeDetailsRepository: IEmployeeDetailsRepository
    {
        private ProjectDBContext _projectDBContext;
        private IMapper _mapper;
        public EmployeeDetailsRepository(ProjectDBContext projectDBContext, IMapper mapper)
        {
            _projectDBContext = projectDBContext;
            _mapper = mapper;

        }

        public async Task<SuccessResponseModel> SaveEmployeeDetails(EmployeeDetailsRequestModel employeeDetailsRequestModel)
        {
            var successResponseModel = new SuccessResponseModel();
            try
            {
                if (employeeDetailsRequestModel.ID <= 0)
                {
                    EmployeeDetails employeeDetails = _mapper.Map<EmployeeDetails>(employeeDetailsRequestModel);
                    await _projectDBContext.AddAsync(employeeDetails).ConfigureAwait(false);
                    await _projectDBContext.SaveChangesAsync().ConfigureAwait(false);
                    successResponseModel.ResponseStatus = (int)CrudImplementation.Create;
                }
                else
                {
                    var getEmployeeDetails = await _projectDBContext.employeeDetails.FirstOrDefaultAsync(x => x.ID == employeeDetailsRequestModel.ID).ConfigureAwait(false);
                    if (getEmployeeDetails != null)
                    {
                        getEmployeeDetails.Name = employeeDetailsRequestModel.Name;
                        getEmployeeDetails.Email = employeeDetailsRequestModel.Email;
                        getEmployeeDetails.Age = employeeDetailsRequestModel.Age;
                        getEmployeeDetails.Salary = employeeDetailsRequestModel.Salary;
                        getEmployeeDetails.Address = employeeDetailsRequestModel.Address;
                        getEmployeeDetails.Gender = employeeDetailsRequestModel.Gender;
                        await _projectDBContext.SaveChangesAsync();
                        successResponseModel.ResponseStatus = (int)CrudImplementation.Update;
                    }
                }
            }
            catch (Exception exception)
            {
                successResponseModel.ResponseStatus = (int)Status.Error;
                throw exception;
            }
            finally
            {
                await _projectDBContext.DisposeAsync();
            }
            return successResponseModel;
        }
    }
}
