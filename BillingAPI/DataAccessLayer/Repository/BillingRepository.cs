﻿using AutoMapper;
using DataAccessLayer.DBContext;
using Microsoft.EntityFrameworkCore;
using ProjectDataAccessLayer.Entities;
using ProjectDataAccessLayer.IRepository;
using ProjectModels.RequestModels;
using ProjectModels.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UtilityLayer.SharedEnums;

namespace ProjectDataAccessLayer.Repository
{
    public class BillingRepository : IBillingRepository
    {
        private ProjectDBContext _projectDBContext;
        private IMapper _mapper;

        public BillingRepository(ProjectDBContext projectDBContext)
        {
            this._projectDBContext = projectDBContext;
        }

        public BillingRepository(ProjectDBContext projectDBContext, IMapper mapper)
        {
            _projectDBContext = projectDBContext;
            _mapper = mapper;

        }

        #region Billing implementation
        public async Task<GetBillingResponseModel> GetBillingAsync()
        {
            var getBillingResponseModel = new GetBillingResponseModel();
            try
            {
                getBillingResponseModel.BillingList = await _projectDBContext.Billing
                                                .Where(param => param.IsActive)
                                                .Select(p => new BillingList()
                                                {
                                                    Id = p.Id,
                                                    CustomerName = p.CustomerName,
                                                    CustomerMobileNumber = p.CustomerMobileNumber,
                                                    CustomerAddress = p.CustomerAddress,
                                                    GrossAmount = p.GrossAmount,
                                                    Discount = p.Discount,
                                                    DiscountBy = p.DiscountBy,
                                                    DiscountReason = p.DiscountReason,
                                                    NetAmount = p.NetAmount,
                                                    Items = p.Items,
                                                    CentreId = p.CentreId,
                                                    ClientId = p.ClientId,
                                                    EmailId = p.EmailId,
                                                    IsActive = p.IsActive

                                                }).ToListAsync();

                getBillingResponseModel.ResponseStatus = (int)CrudImplementation.Read;
            }
            catch
            {
                getBillingResponseModel.ResponseStatus = (int)Status.Error;

                throw;
            }
            return getBillingResponseModel;
        }



        public async Task<CustomerDetailsResponseModel> GetCustomerDetailsByMobileNo(string mobileNumber)
        {
            CustomerDetailsResponseModel customerDetailsResponseModel = new CustomerDetailsResponseModel();
            try
            {
                var billingDetails = await _projectDBContext.Billing.FirstOrDefaultAsync(x => x.CustomerMobileNumber == mobileNumber && x.IsActive);
                if (billingDetails != null)
                {
                    customerDetailsResponseModel = new CustomerDetailsResponseModel()
                    {
                        CustomerName = billingDetails.CustomerName,
                        CustomerAddress = billingDetails.CustomerAddress,
                        CustomerMobileNumber = mobileNumber,
                        EmailId = billingDetails.EmailId,
                        ResponseStatus= (int)CrudImplementation.Read
                    };
                }
            }
            catch
            {
                customerDetailsResponseModel.ResponseStatus = (int)Status.Error;

                throw;
            }
            return customerDetailsResponseModel;
        }


        public async Task<SuccessResponseModel> SaveBillingInformationAsync(BillingRequestModel billingRequestModel)
        {
            var successResponseModel = new SuccessResponseModel();
            try
            {
                if (billingRequestModel.Id <= 0)
                {
                    Billing billing = _mapper.Map<Billing>(billingRequestModel);
                    billing.CreatedBy = Convert.ToInt32(billingRequestModel.CreatedBy);
                    billing.CreatedDateTime = DateTime.Now;
                    billing.IsActive = true;
                    await _projectDBContext.AddAsync(billing).ConfigureAwait(false);

                    await _projectDBContext.SaveChangesAsync().ConfigureAwait(false);


                    successResponseModel.ResponseStatus = (int)CrudImplementation.Create;
                }
                else
                {
                    var getUserInformation = await _projectDBContext.Billing.FirstOrDefaultAsync(x => x.Id == billingRequestModel.Id && x.IsActive).ConfigureAwait(false);
                    if (getUserInformation != null)
                    {
                        getUserInformation.CustomerName = billingRequestModel.CustomerName;
                        getUserInformation.CustomerMobileNumber = billingRequestModel.CustomerMobileNumber;
                        getUserInformation.CustomerAddress = billingRequestModel.CustomerAddress;
                        getUserInformation.GrossAmount = billingRequestModel.GrossAmount;
                        getUserInformation.Discount = billingRequestModel.Discount;
                        getUserInformation.DiscountBy = billingRequestModel.DiscountBy;
                        getUserInformation.DiscountReason = billingRequestModel.DiscountReason;
                        getUserInformation.NetAmount = billingRequestModel.NetAmount;
                        getUserInformation.Items = billingRequestModel.Items;
                        getUserInformation.CentreId = billingRequestModel.CentreId;
                        getUserInformation.ClientId = billingRequestModel.ClientId;
                        getUserInformation.EmailId = billingRequestModel.EmailId;
                        getUserInformation.CreatedBy = 6;// billingRequestModel.CreatedBy;
                        getUserInformation.CreatedDateTime = DateTime.Now;
                        //getUserInformation.DeletedBy = billingRequestModel.DeletedBy;
                        //getUserInformation.DeletedDateTime = DateTime.Now;
                        getUserInformation.UpdatedBy = Convert.ToInt32(billingRequestModel.UpdatedBy);
                        getUserInformation.UpdatedDateTime = DateTime.Now;
                        //getUserInformation.IsActive = billingRequestModel.IsActive;
                        getUserInformation.IPAddress = billingRequestModel.IPAddress;

                        await _projectDBContext.SaveChangesAsync();
                        successResponseModel.ResponseStatus = (int)CrudImplementation.Update;
                    }
                }
            }
            catch (Exception exception)
            {
                successResponseModel.ResponseStatus = (int)Status.Error;
                throw exception;

            }
            finally
            {
                await _projectDBContext.DisposeAsync();
            }
            return successResponseModel;
        }

        public async Task<SuccessResponseModel> DeleteBillingByIdAsync(DeleteBillingtRequestModel deleteBillingRequestModel)
        {
            var successResponseModel = new SuccessResponseModel();
            try
            {
                var getUserInformation = await _projectDBContext.Billing.FirstOrDefaultAsync(x => x.Id == deleteBillingRequestModel.Id && x.IsActive).ConfigureAwait(false);
                if (getUserInformation != null)


                {
                    getUserInformation.IsActive = false;
                    getUserInformation.DeletedBy = 1;
                    getUserInformation.DeletedDateTime = DateTime.Now;
                    await _projectDBContext.SaveChangesAsync().ConfigureAwait(false);
                    successResponseModel.ResponseStatus = (int)CrudImplementation.Delete;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                await _projectDBContext.DisposeAsync();
            }
            return successResponseModel;
        }



        #endregion



    }
}

