﻿using AutoMapper;
using DataAccessLayer.DBContext;
using Microsoft.EntityFrameworkCore;
using ProjectDataAccessLayer.Entities;
using ProjectDataAccessLayer.IRepository;
using ProjectModels.Dto;
using ProjectModels.RequestModels;
using ProjectModels.ResponseModels;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityLayer.SharedEnums;

namespace ProjectDataAccessLayer.Repository
{
    public class ItemMasterRepository : IItemMasterRepository
    {
        private ProjectDBContext _projectDBContext;
        private IMapper _mapper;
        public ItemMasterRepository(ProjectDBContext projectDBContext)
        {
            this._projectDBContext = projectDBContext;
        }

        public ItemMasterRepository(ProjectDBContext projectDBContext, IMapper mapper)
        {
            _projectDBContext = projectDBContext;
            _mapper = mapper;

        }
        
        

    }
}
