﻿using AutoMapper;
using DataAccessLayer.DBContext;
using Microsoft.EntityFrameworkCore;
using ProjectDataAccessLayer.Entities;
using ProjectDataAccessLayer.IRepository;
using ProjectModels.Dto;
using ProjectModels.RequestModels;
using ProjectModels.ResponseModels;
using System;
using System.Linq;
using System.Threading.Tasks;
using UtilityLayer.SharedConstants;
using UtilityLayer.SharedEnums;

namespace ProjectDataAccessLayer.Repository
{
    public class AdminUserRepository : IAdminUserRepository
    {
        private ProjectDBContext _projectDBContext;
        private IMapper _mapper;
        public AdminUserRepository(ProjectDBContext projectDBContext, IMapper mapper)
        {
            _projectDBContext = projectDBContext;
            _mapper = mapper;

        }
        #region User master implementation
        public async Task<GetAllUserResponseModel> GetAllUsersAsync()
        {
            var getAllUsersResponseModel = new GetAllUserResponseModel();
            try
            {
                getAllUsersResponseModel.UsersList = await _projectDBContext.users
                                                .Where(param => param.IsActive)
                                                .Select(p => new AllUserList()
                                                {
                                                    UserId = p.UserId,
                                                    UserName = p.UserName,
                                                    DateOfBirth = p.DateOfBirth,
                                                    Gender = p.Gender,
                                                    BloodGroup = p.BloodGroup,
                                                    MaritalStatus = p.MaritalStatus,
                                                    SpouseName = p.SpouseName,
                                                    FatherName = p.FatherName,
                                                    MotherName = p.MotherName,
                                                    GuardianName = p.GuardianName,
                                                    PassportNumber = p.PassportNumber,
                                                    AdhaarCardNumber = p.AdhaarCardNumber,
                                                    PANCardNumber = p.PANCardNumber,
                                                    PrimaryContactNumber = p.PrimaryContactNumber,
                                                    SecondryContactNumber = p.SecondryContactNumber,
                                                    LandlineNumber = p.LandlineNumber,
                                                    EmergencyContactName = p.EmergencyContactName,
                                                    EmergencyContactNumber = p.EmergencyContactNumber,
                                                    EmailId = p.EmailId,
                                                    PermanentAddress = p.PermanentAddress,
                                                    Password = p.Password,
                                                    LoginName = p.LoginName,
                                                    Roles = p.Roles,
                                                    Centres = p.Centres
                                                }).ToListAsync();
                getAllUsersResponseModel.ResponseStatus = (int)CrudImplementation.Read; 
            }
            catch
            {
                getAllUsersResponseModel.ResponseStatus = (int)Status.Error;
                throw;
            }
            return getAllUsersResponseModel;
        }
        public async Task<UserDropdownResponseModel> GetUserMasterDataAsync()
        {
            var userDropdownResponseModel = new UserDropdownResponseModel();
            try
            {
                var configDetails = await _projectDBContext.configMaster.Where(x => x.ConfigKey == Constants.Gender || x.ConfigKey == Constants.BloodGroup || x.ConfigKey == Constants.MaritalStatus).ToListAsync();
                userDropdownResponseModel.GenderList = configDetails.Where(x => x.ConfigKey == Constants.Gender).Select(p => new GenderResponseModel()
                {
                    Gender = p.ConfigValue,
                    GenderId = Convert.ToInt32(p.ConfigId),
                    GenderCode = p.ConfigCode
                }).ToList();
                userDropdownResponseModel.BloodGroupList = configDetails.Where(x => x.ConfigKey == Constants.BloodGroup).Select(p => new BloodGroupResponseModel()
                {
                    BloodGroup = p.ConfigValue,
                    BloodGroupId = Convert.ToInt32(p.ConfigId),
                    BloodGroupCode = p.ConfigCode
                }).ToList();
                userDropdownResponseModel.MaritalStatusList = configDetails.Where(x => x.ConfigKey == Constants.MaritalStatus).Select(p => new MaritalStatusResponseModel()
                {
                    MaritalStatus = p.ConfigValue,
                    MaritalStatusId = Convert.ToInt32(p.ConfigId),
                    MaritalStatusCode = p.ConfigCode
                }).ToList();
                userDropdownResponseModel.RoleList = await _projectDBContext.roleMaster.Where(x => x.IsActive).Select(p => new RolesResponseModel()
                {
                    RoleId = p.RoleId,
                    RoleName = p.RoleName,
                    RoleCode = p.RoleCode
                }).ToListAsync().ConfigureAwait(false);
                userDropdownResponseModel.CentreList = await _projectDBContext.centreMaster.Where(x => x.IsActive).Select(p => new CentreResponseModel()
                {
                    CentreId = p.CentreId,
                    CentreName = p.CentreName,
                }).ToListAsync().ConfigureAwait(false);
                userDropdownResponseModel.ResponseStatus = (int)CrudImplementation.Read;


            }
            catch
            {
                userDropdownResponseModel.ResponseStatus = (int)Status.Error;
                throw;
            }
            return userDropdownResponseModel;
        }
        public async Task<SuccessResponseModel> SaveUserInfomationAsync(UserDto userDto, long userId)
        {
            var successResponseModel = new SuccessResponseModel();
            try
            {
                if (userId <= 0)
                {
                    Users users = _mapper.Map<Users>(userDto);
                    users.CreatedBy = Convert.ToInt32(userDto.LoggedInUserId);
                    users.CreatedDateTime = DateTime.Now;
                    await _projectDBContext.AddAsync(users).ConfigureAwait(false);
                    await _projectDBContext.SaveChangesAsync().ConfigureAwait(false);
                    Login login = new Login()
                    {
                        UserId = users.UserId,
                        Password = users.Password,
                        LoginName = users.LoginName,
                        EmailId = users.EmailId,
                        TotalAttemptCount = 0
                    };
                    await _projectDBContext.AddAsync(login).ConfigureAwait(false);
                    await _projectDBContext.SaveChangesAsync().ConfigureAwait(false);

                    successResponseModel.ResponseStatus = (int)CrudImplementation.Create;
                }
                else
                {
                    var getUserInformation = await _projectDBContext.users.FirstOrDefaultAsync(x => x.UserId == userId && x.IsActive).ConfigureAwait(false);
                    if (getUserInformation != null)
                    {
                        getUserInformation.UserName = userDto.UserName;
                        getUserInformation.DateOfBirth = Convert.ToDateTime(userDto.DateOfBirth);
                        getUserInformation.Gender = userDto.Gender;
                        getUserInformation.BloodGroup = userDto.BloodGroup;
                        getUserInformation.MaritalStatus = userDto.MaritalStatus;
                        getUserInformation.SpouseName = userDto.SpouseName;
                        getUserInformation.FatherName = userDto.FatherName;
                        getUserInformation.MotherName = userDto.MotherName;
                        getUserInformation.GuardianName = userDto.GuardianName;
                        getUserInformation.PassportNumber = userDto.PassportNumber;
                        getUserInformation.AdhaarCardNumber = userDto.AdhaarCardNumber;
                        getUserInformation.PANCardNumber = userDto.PANCardNumber;
                        getUserInformation.PrimaryContactNumber = userDto.PrimaryContactNumber;
                        getUserInformation.SecondryContactNumber = userDto.SecondryContactNumber;
                        getUserInformation.EmergencyContactName = userDto.EmergencyContactName;
                        getUserInformation.EmergencyContactNumber = userDto.EmergencyContactNumber;
                        getUserInformation.LandlineNumber = userDto.LandlineNumber;
                        getUserInformation.EmailId = userDto.EmailId;
                        getUserInformation.PermanentAddress = userDto.PermanentAddress;
                        getUserInformation.LoginName = userDto.LoginName;
                        getUserInformation.Password = userDto.Password;
                        getUserInformation.Roles = userDto.Roles;
                        getUserInformation.Centres = userDto.Centres;
                        getUserInformation.UpdatedBy = 60;//Convert.ToInt32(userDto.LoggedInUserId);
                        getUserInformation.UpdatedDateTime = DateTime.Now;
                        getUserInformation.IPAddress = userDto.IPAddress;
                        await _projectDBContext.SaveChangesAsync();
                        successResponseModel.ResponseStatus = (int)CrudImplementation.Update;
                    }
                }
            }
            catch (Exception exception)
            {
                successResponseModel.ResponseStatus = (int)Status.Error;
                throw exception;
            }
            finally
            {
                await _projectDBContext.DisposeAsync();
            }
            return successResponseModel;
        }
        public async Task<SuccessResponseModel> DeleteUserByIdAsync(DeleteRecordRequestModel deleteRecordRequestModel)
        {
            var successResponseModel = new SuccessResponseModel();
            try
            {
                var getUserInformation = await _projectDBContext.users.FirstOrDefaultAsync(x => x.UserId == Convert.ToInt64(deleteRecordRequestModel.DeleteRecordId) && x.IsActive).ConfigureAwait(false);
                if (getUserInformation != null)
                {
                    getUserInformation.IsActive = true;
                    getUserInformation.DeletedBy = 63;// Convert.ToInt64(deleteRecordRequestModel.LoggedInUserId);
                    getUserInformation.DeletedDateTime = DateTime.Now;
                    await _projectDBContext.SaveChangesAsync().ConfigureAwait(false);
                    successResponseModel.ResponseStatus = (int)CrudImplementation.Delete;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                await _projectDBContext.DisposeAsync();
            }
            return successResponseModel;
        }
        #endregion

    }
}
