﻿using DataAccessLayer.DBContext;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System;
using UtilityLayer.SharedEnums;
using ProjectDataAccessLayer.Entities;
using ProjectDataAccessLayer.IRepository;
using ProjectModels.RequestModels;
using ProjectModels.ResponseModels;

namespace ProjectDataAccessLayer.Repository
{
    public class AuthenticationRepository : IAuthenticationRepository
    {
        private ProjectDBContext _projectDBContext;

        public AuthenticationRepository(ProjectDBContext projectDBContext)
        {
            _projectDBContext = projectDBContext;
        }

        public async Task<AuthenticationResponseModel> AuthenticationAsync(AuthenticationRequestModel authenticateRequestModel)
        {
            var authenticationResponseModel = new AuthenticationResponseModel();
            try
            {
                var authenticationResult = await (from x in _projectDBContext.users
                                                  join y in _projectDBContext.login on x.UserId equals y.UserId
                                                  select new
                                                  {
                                                      y.LoginName,
                                                      y.EmailId,
                                                      y.UserId,
                                                      x.Roles,
                                                      x.Centres,
                                                      y.Password
                                                  }).Where(x => x.LoginName == authenticateRequestModel.LoginName && x.Password == authenticateRequestModel.Password).FirstOrDefaultAsync();
                if (authenticationResult != null)
                {
                    Users users = await _projectDBContext.users.FirstOrDefaultAsync(x => x.LoginName == authenticateRequestModel.LoginName && x.IsActive).ConfigureAwait(false);
                    string centres = users.Centres;
                    List<long> userCentreAccessList = centres.Split(',').Select(long.Parse).ToList();
                    if (userCentreAccessList.Contains(Convert.ToInt64(authenticateRequestModel.Centre)))
                    {
                        string roles = users.Roles;
                        List<string> userRoleAccessList = roles.Split(',').ToList();

                        var roleDetailsById = await _projectDBContext.roleMaster.Where(x => userRoleAccessList.Contains(x.RoleCode)).Select(p => new
                        RoleMaster()
                        {
                            RoleCode = p.RoleCode,
                            RoleName = p.RoleName
                        }).ToListAsync();
                        authenticationResponseModel.RoleList = new List<RolesResponseModel>();
                        foreach (var role in roleDetailsById)
                        {
                            authenticationResponseModel.RoleList.Add(new RolesResponseModel { RoleCode = role.RoleCode, RoleName = role.RoleName });
                        }
                        authenticationResponseModel.UserId = users.UserId;
                        authenticationResponseModel.UserName = users.UserName;
                        authenticationResponseModel.CentreName = _projectDBContext.centreMaster.SingleOrDefault(x => x.CentreId == Convert.ToInt64(authenticateRequestModel.Centre)).CentreName;
                        authenticationResponseModel.ResponseStatus = (int)Status.LoginSuccess;
                    }
                    else
                    {
                        authenticationResponseModel.ResponseStatus = (int)Status.LoginFail;
                    }
                }
                else
                {
                    authenticationResponseModel.ResponseStatus = (int)Status.LoginFail;
                }
            }
            catch
            {
                authenticationResponseModel.ResponseStatus = (int)Status.Error;
                throw;
            }
            finally
            {
                await _projectDBContext.DisposeAsync();
            }
            return authenticationResponseModel;
        }

        public async Task<List<CentreResponseModel>> GetAllCentreForLoginAsync()
        {
            try
            {

                return await _projectDBContext.centreMaster.Where(x => x.IsActive).Select(p => new CentreResponseModel()
                {
                    CentreId = p.CentreId,
                    CentreName = p.CentreName,
                }).ToListAsync().ConfigureAwait(false);

            }
            catch
            {
                throw;
            }
        }
    }
}