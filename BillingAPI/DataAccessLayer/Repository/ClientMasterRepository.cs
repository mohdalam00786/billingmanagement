﻿using AutoMapper;
using DataAccessLayer.DBContext;
using Microsoft.EntityFrameworkCore;
using ProjectDataAccessLayer.Entities;
using ProjectDataAccessLayer.IRepository;
using ProjectModels.RequestModels;
using ProjectModels.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityLayer.SharedEnums;

namespace ProjectDataAccessLayer.Repository
{
    public class ClientMasterRepository :IClientMasterRepository
    {
        private ProjectDBContext _projectDBContext;
        private IMapper _mapper;
        public ClientMasterRepository(ProjectDBContext projectDBContext)
        {
            this._projectDBContext = projectDBContext;
        }

        public ClientMasterRepository(ProjectDBContext projectDBContext, IMapper mapper)
        {
            _projectDBContext = projectDBContext;
            _mapper = mapper;

        }

        


    }
}
