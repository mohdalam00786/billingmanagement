﻿using AutoMapper;
using DataAccessLayer.DBContext;
using Microsoft.EntityFrameworkCore;
using ProjectDataAccessLayer.Entities;
using ProjectDataAccessLayer.IRepository;
using ProjectModels.Dto;
using ProjectModels.RequestModels;
using ProjectModels.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UtilityLayer.SharedEnums;

namespace ProjectDataAccessLayer.Repository
{
    public class AdminMasterRepository : IAdminMasterRepository
    {
        private ProjectDBContext _projectDBContext;
        private IMapper _mapper;
        public AdminMasterRepository(ProjectDBContext projectDBContext, IMapper mapper)
        {
            _projectDBContext = projectDBContext;
            _mapper = mapper;
        }

        #region Centre master implementation
        public async Task<List<GetAllCentreResponseModel>> GetAllCentresAsync()
        {
            var getAllCentreResponseModel = new List<GetAllCentreResponseModel>();
            try
            {

                getAllCentreResponseModel = await _projectDBContext.centreMaster.Where(param => param.IsActive)
                                                                .Select(p => new GetAllCentreResponseModel()
                                                                {
                                                                    CentreId = p.CentreId,
                                                                    CentreName = p.CentreName,
                                                                    CentreCode = p.CentreCode,
                                                                    Website = p.Website,
                                                                    Address = p.Address,
                                                                    MobileNumber = p.MobileNumber,
                                                                    LandlineNumber = p.LandlineNumber,
                                                                    EmailId = p.EmailId,
                                                                    ResponseStatus = (int)CrudImplementation.Read
                                                                }).ToListAsync().ConfigureAwait(false);


            }
            catch
            {
                getAllCentreResponseModel[0].ResponseStatus = (int)Status.Error;
                throw;
            }
            return getAllCentreResponseModel;
        }
        public async Task<SuccessResponseModel> SaveCentreInfomationAsync(CentreMasterDto centreMasterDto, long centreId)
        {
            var successResponseModel = new SuccessResponseModel();
            try
            {

                if (centreId <= 0)
                {
                    CentreMaster centreMaster = _mapper.Map<CentreMaster>(centreMasterDto);
                    centreMaster.CreatedBy = Convert.ToInt32(centreMasterDto.LoggedInUserId);
                    centreMaster.CreatedDateTime = DateTime.Now;
                    await _projectDBContext.AddAsync(centreMaster);
                    await _projectDBContext.SaveChangesAsync();
                    successResponseModel.ResponseStatus = (int)CrudImplementation.Create;
                }
                else
                {
                    var getCentreInformation = await _projectDBContext.centreMaster.SingleOrDefaultAsync(x => x.CentreId == centreId && x.IsActive).ConfigureAwait(false);
                    getCentreInformation.CentreName = centreMasterDto.CentreName;
                    getCentreInformation.CentreCode = centreMasterDto.CentreCode;
                    getCentreInformation.Website = centreMasterDto.Website;
                    getCentreInformation.Address = centreMasterDto.Address;
                    getCentreInformation.MobileNumber = centreMasterDto.MobileNumber;
                    getCentreInformation.LandlineNumber = centreMasterDto.LandlineNumber;
                    getCentreInformation.EmailId = centreMasterDto.EmailId;
                    getCentreInformation.UpdatedBy = Convert.ToInt32(centreMasterDto.LoggedInUserId);
                    getCentreInformation.UpdatedDateTime = DateTime.Now;
                    getCentreInformation.IPAddress = centreMasterDto.IPAddress;
                    await _projectDBContext.SaveChangesAsync();
                    successResponseModel.ResponseStatus = (int)CrudImplementation.Update;
                }

            }
            catch
            {
                successResponseModel.ResponseStatus = (int)Status.Error;

                throw;
            }
            finally
            {
                await _projectDBContext.DisposeAsync();
            }
            return successResponseModel;
        }
        public async Task<SuccessResponseModel> DeleteCentreByIdAsync(DeleteRecordRequestModel deleteRecordRequestModel)
        {
            var responseModel = new SuccessResponseModel();
            try
            {
                var getCentreInformation = await _projectDBContext.centreMaster.SingleOrDefaultAsync(x => x.CentreId == Convert.ToInt64(deleteRecordRequestModel.DeleteRecordId) && x.IsActive).ConfigureAwait(false);
                if (getCentreInformation != null)
                {
                    getCentreInformation.IsActive = true;
                    getCentreInformation.DeletedBy = Convert.ToInt64(deleteRecordRequestModel.LoggedInUserId);
                    getCentreInformation.DeletedDateTime = DateTime.Now;
                    await _projectDBContext.SaveChangesAsync();
                    responseModel.ResponseStatus = (int)CrudImplementation.Delete;

                }
            }
            catch
            {
                responseModel.ResponseStatus = (int)Status.Error;
                throw;
            }
            finally
            {
                await _projectDBContext.DisposeAsync();
            }
            return responseModel;
        }
        #endregion


        #region Item master implementation
        public async Task<GetItemMasterResponseModel> GetItemMasterAsync()
        {
            var getItemMasterResponseModel = new GetItemMasterResponseModel();
            try
            {
                getItemMasterResponseModel.ItemMasterList = await _projectDBContext.ItemMaster
                                                .Where(param => param.IsActive)
                                                .Select(p => new ItemMasterList()
                                                {
                                                    Id = p.Id,
                                                    ItemName = p.ItemName,
                                                    ItemPrice = p.ItemPrice,
                                                    ItemCompany = p.ItemCompany,
                                                    ItemMFGDate = p.ItemMFGDate,
                                                    ItemExpiryDate = p.ItemExpiryDate,
                                                    IsActive = p.IsActive

                                                }).ToListAsync();

                getItemMasterResponseModel.ResponseStatus = (int)CrudImplementation.Read;
            }
            catch
            {
                getItemMasterResponseModel.ResponseStatus = (int)Status.Error;

                throw;
            }
            return getItemMasterResponseModel;
        }


        public async Task<SuccessResponseModel> SaveItemMasterInformationAsync(ItemMasterRequestModel itemMasterRequestModel)
        {
            var successResponseModel = new SuccessResponseModel();
            try
            {
                if (itemMasterRequestModel.Id <= 0)
                {
                    ItemMaster itemMaster = _mapper.Map<ItemMaster>(itemMasterRequestModel);
                    itemMaster.CreatedBy = Convert.ToInt32(itemMasterRequestModel.CreatedBy);
                    itemMaster.CreatedDateTime = DateTime.Now;
                    itemMaster.IsActive = true;
                    await _projectDBContext.AddAsync(itemMaster).ConfigureAwait(false);

                    await _projectDBContext.SaveChangesAsync().ConfigureAwait(false);


                    successResponseModel.ResponseStatus = (int)CrudImplementation.Create;
                }
                else
                {
                    var getUserInformation = await _projectDBContext.ItemMaster.FirstOrDefaultAsync(x => x.Id == itemMasterRequestModel.Id && x.IsActive).ConfigureAwait(false);
                    if (getUserInformation != null)
                    {
                        getUserInformation.ItemName = itemMasterRequestModel.ItemName;
                        getUserInformation.ItemPrice = itemMasterRequestModel.ItemPrice;
                        getUserInformation.ItemCompany = itemMasterRequestModel.ItemCompany;
                        getUserInformation.ItemMFGDate = DateTime.Now;
                        getUserInformation.ItemExpiryDate = DateTime.Now;
                        getUserInformation.CreatedBy = 6;// itemMasterRequestModel.CreatedBy;
                        getUserInformation.CreatedDateTime = DateTime.Now;
                        //getUserInformation.DeletedBy = itemMasterRequestModel.DeletedBy;
                        //getUserInformation.IsActive = itemMasterRequestModel.IsActive;
                        getUserInformation.UpdatedBy = Convert.ToInt32(itemMasterRequestModel.UpdatedBy);
                        getUserInformation.UpdatedDateTime = DateTime.Now;
                        getUserInformation.IPAddress = itemMasterRequestModel.IPAddress;

                        await _projectDBContext.SaveChangesAsync();
                        successResponseModel.ResponseStatus = (int)CrudImplementation.Update;
                    }
                }
            }
            catch (Exception exception)
            {
                successResponseModel.ResponseStatus = (int)Status.Error;
                throw exception;

            }
            finally
            {
                await _projectDBContext.DisposeAsync();
            }
            return successResponseModel;
        }

        public async Task<SuccessResponseModel> DeleteItemMasterByIdAsync(DeleteItemMasterRequestModel deleteItemMasterRequestModel)
        {
            var successResponseModel = new SuccessResponseModel();
            try
            {
                var getUserInformation = await _projectDBContext.ItemMaster.FirstOrDefaultAsync(x => x.Id == deleteItemMasterRequestModel.Id && x.IsActive).ConfigureAwait(false);
                if (getUserInformation != null)


                {
                    getUserInformation.IsActive = false;
                    getUserInformation.DeletedBy = 1;
                    getUserInformation.DeletedDateTime = DateTime.Now;
                    await _projectDBContext.SaveChangesAsync().ConfigureAwait(false);
                    successResponseModel.ResponseStatus = (int)CrudImplementation.Delete;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                await _projectDBContext.DisposeAsync();
            }
            return successResponseModel;
        }
        #endregion

        #region Client master implementation
        public async Task<GetClientMasterResponseModel> GetClientMasterAsync()
        {
            var getClientMasterResponseModel = new GetClientMasterResponseModel();
            try
            {
                getClientMasterResponseModel.ClientMasterList = await _projectDBContext.ClientMaster
                                                .Where(param => param.IsActive)
                                                .Select(p => new ClientMasterList()
                                                {
                                                    Id = p.Id,
                                                    ClientName = p.ClientName,
                                                    ClientAddress = p.ClientAddress,
                                                    OwnerName = p.OwnerName,
                                                    MobileNumber1 = p.MobileNumber1,
                                                    MobileNumber2 = p.MobileNumber2,
                                                    EmailId = p.EmailId,
                                                    IsActive = p.IsActive

                                                }).ToListAsync();

                getClientMasterResponseModel.ResponseStatus = (int)CrudImplementation.Read;
            }
            catch
            {
                getClientMasterResponseModel.ResponseStatus = (int)Status.Error;

                throw;
            }
            return getClientMasterResponseModel;
        }


        public async Task<SuccessResponseModel> SaveClientMasterInformationAsync(ClientMasterRequestModel clientMasterRequestModel)
        {
            var successResponseModel = new SuccessResponseModel();
            try
            {
                if (clientMasterRequestModel.Id <= 0)
                {
                    ClientMaster clientMaster = _mapper.Map<ClientMaster>(clientMasterRequestModel);
                    clientMaster.CreatedBy = Convert.ToInt32(clientMasterRequestModel.CreatedBy);
                    clientMaster.CreatedDateTime = DateTime.Now;
                    clientMaster.IsActive = true;
                    await _projectDBContext.AddAsync(clientMaster).ConfigureAwait(false);

                    await _projectDBContext.SaveChangesAsync().ConfigureAwait(false);


                    successResponseModel.ResponseStatus = (int)CrudImplementation.Create;
                }
                else
                {
                    var getUserInformation = await _projectDBContext.ClientMaster.FirstOrDefaultAsync(x => x.Id == clientMasterRequestModel.Id && x.IsActive).ConfigureAwait(false);
                    if (getUserInformation != null)
                    {
                        getUserInformation.ClientName = clientMasterRequestModel.ClientName;
                        getUserInformation.ClientAddress = clientMasterRequestModel.ClientAddress;
                        getUserInformation.OwnerName = clientMasterRequestModel.OwnerName;
                        getUserInformation.MobileNumber1 = clientMasterRequestModel.MobileNumber1;
                        getUserInformation.MobileNumber2 = clientMasterRequestModel.MobileNumber2;
                        getUserInformation.EmailId = clientMasterRequestModel.EmailId;
                        getUserInformation.CreatedBy = 6;// clientMasterRequestModel.CreatedBy;
                        getUserInformation.CreatedDateTime = DateTime.Now;
                        //getUserInformation.DeletedBy = clientMasterRequestModel.DeletedBy;
                        //getUserInformation.IsActive = clientMasterRequestModel.IsActive;
                        getUserInformation.UpdatedBy = Convert.ToInt32(clientMasterRequestModel.UpdatedBy);
                        getUserInformation.UpdatedDateTime = DateTime.Now;
                        getUserInformation.IPAddress = clientMasterRequestModel.IPAddress;

                        await _projectDBContext.SaveChangesAsync();
                        successResponseModel.ResponseStatus = (int)CrudImplementation.Update;
                    }
                }
            }
            catch (Exception exception)
            {
                successResponseModel.ResponseStatus = (int)Status.Error;
                throw exception;

            }
            finally
            {
                await _projectDBContext.DisposeAsync();
            }
            return successResponseModel;
        }

        public async Task<SuccessResponseModel> DeleteClientMasterByIdAsync(DeleteClientMasterRequestModel deleteClientMasterRequestModel)
        {
            var successResponseModel = new SuccessResponseModel();
            try
            {
                var getUserInformation = await _projectDBContext.ClientMaster.FirstOrDefaultAsync(x => x.Id == deleteClientMasterRequestModel.Id && x.IsActive).ConfigureAwait(false);
                if (getUserInformation != null)


                {
                    getUserInformation.IsActive = false;
                    getUserInformation.DeletedBy = 1;
                    getUserInformation.DeletedDateTime = DateTime.Now;
                    await _projectDBContext.SaveChangesAsync().ConfigureAwait(false);
                    successResponseModel.ResponseStatus = (int)CrudImplementation.Delete;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                await _projectDBContext.DisposeAsync();
            }
            return successResponseModel;
        }



        #endregion


        #region Role master implementation
        public async Task<List<RolesResponseModel>> GetAllRolesAsync()
        {
            var rolesResponseModel = new List<RolesResponseModel>();
            try
            {
                rolesResponseModel = await _projectDBContext.roleMaster
                                                .Where(param => param.IsActive)
                                                .Select(p => new RolesResponseModel()
                                                {
                                                    RoleId = p.RoleId,
                                                    RoleName = p.RoleName,
                                                    RoleDescription = p.RoleDescription,
                                                    ResponseStatus = (int)CrudImplementation.Read
                                                }).ToListAsync().ConfigureAwait(false);
            }
            catch
            {
                rolesResponseModel[0].ResponseStatus = (int)Status.Error;
                throw;
            }
            return rolesResponseModel;
        }
        public async Task<SuccessResponseModel> SaveRolesInfomationAsync(RoleMasterDto roleMasterDto, long roleId)
        {
            var successResponseModel = new SuccessResponseModel();
            try
            {

                if (roleId <= 0)
                {
                    roleMasterDto.RoleCode = roleMasterDto.RoleName.Substring(0, 3);
                    RoleMaster roleMaster = _mapper.Map<RoleMaster>(roleMasterDto);
                    roleMaster.CreatedBy = Convert.ToInt32(roleMaster.LoggedInUserId);
                    roleMaster.CreatedDateTime = DateTime.Now;
                    await _projectDBContext.AddAsync(roleMaster);
                    await _projectDBContext.SaveChangesAsync();
                    successResponseModel.ResponseStatus = (int)CrudImplementation.Create;
                }
                else
                {
                    var getRoleInformation = await _projectDBContext.roleMaster.SingleOrDefaultAsync(x => x.RoleId == roleId && x.IsActive).ConfigureAwait(false);
                    getRoleInformation.RoleName = roleMasterDto.RoleName;
                    getRoleInformation.RoleDescription = roleMasterDto.RoleDescription;
                    getRoleInformation.UpdatedBy = Convert.ToInt32(roleMasterDto.LoggedInUserId);
                    getRoleInformation.UpdatedDateTime = DateTime.Now;
                    getRoleInformation.IPAddress = roleMasterDto.IPAddress;
                    await _projectDBContext.SaveChangesAsync();
                    successResponseModel.ResponseStatus = (int)CrudImplementation.Update;
                }

            }
            catch
            {
                successResponseModel.ResponseStatus = (int)Status.Error;
                throw;
            }
            finally
            {
                await _projectDBContext.DisposeAsync();
            }
            return successResponseModel;
        }
        public async Task<SuccessResponseModel> DeleteRoleByIdAsync(DeleteRecordRequestModel deleteRecordRequestModel)
        {
            var responseModel = new SuccessResponseModel();
            try
            {
                var getRoleInformation = await _projectDBContext.roleMaster.SingleOrDefaultAsync(x => x.RoleId == Convert.ToInt64(deleteRecordRequestModel.DeleteRecordId) && x.IsActive).ConfigureAwait(false);
                if (getRoleInformation != null)
                {
                    getRoleInformation.IsActive = true;
                    getRoleInformation.DeletedBy = Convert.ToInt64(deleteRecordRequestModel.LoggedInUserId);
                    getRoleInformation.DeletedDateTime = DateTime.Now;
                    await _projectDBContext.SaveChangesAsync();
                    responseModel.ResponseStatus = (int)CrudImplementation.Delete;
                }
            }
            catch
            {
                responseModel.ResponseStatus = (int)Status.Error;
                throw;
            }
            finally
            {
                await _projectDBContext.DisposeAsync();
            }
            return responseModel;
        }
        #endregion

        #region Config master implementation
        public async Task<List<ConfigMasterResponseModel>> GetAllConfigAsync()
        {
            var configResponseModel = new List<ConfigMasterResponseModel>();
            try
            {
                configResponseModel = await _projectDBContext.configMaster
                                                .Where(param => param.IsActive)
                                                .Select(p => new ConfigMasterResponseModel()
                                                {
                                                    ConfigId = p.ConfigId,
                                                    ConfigKey = p.ConfigKey,
                                                    ConfigValue = p.ConfigValue,
                                                    ResponseStatus = (int)CrudImplementation.Read
                                                }).ToListAsync().ConfigureAwait(false);
            }
            catch
            {
                configResponseModel[0].ResponseStatus = (int)Status.Error;
                throw;
            }
            return configResponseModel;
        }
        public async Task<SuccessResponseModel> SaveConfigInfomationAsync(ConfigMasterDto configMasterDto, long configId)
        {
            var successResponseModel = new SuccessResponseModel();
            try
            {

                if (configId <= 0)
                {
                    var getConfigInformation = await _projectDBContext.configMaster.SingleOrDefaultAsync(x => x.ConfigKey.ToLower() == configMasterDto.ConfigKey.ToLower()
                    && x.ConfigValue.ToLower() == configMasterDto.ConfigValue.ToLower() && x.IsActive).ConfigureAwait(false);
                    if (getConfigInformation == null)
                    {
                        configMasterDto.ConfigKey = configMasterDto.ConfigKey?.First().ToString().ToUpper() + configMasterDto.ConfigKey?.Substring(1).ToLower();
                        configMasterDto.ConfigValue = configMasterDto.ConfigValue?.First().ToString().ToUpper() + configMasterDto.ConfigValue?.Substring(1).ToLower();
                        ConfigMaster configMaster = _mapper.Map<ConfigMaster>(configMasterDto);
                        configMaster.CreatedBy = Convert.ToInt32(configMasterDto.LoggedInUserId);
                        configMaster.CreatedDateTime = DateTime.Now;
                        await _projectDBContext.AddAsync(configMaster);
                        await _projectDBContext.SaveChangesAsync();
                        successResponseModel.ResponseStatus = (int)CrudImplementation.Create;
                    }
                    else
                    {
                        successResponseModel.ResponseStatus = (int)Status.AlreadyExist;
                    }

                }
                else
                {
                    var getConfigInformation = await _projectDBContext.configMaster.SingleOrDefaultAsync(x => x.ConfigId == configId && x.ConfigKey.ToLower() == configMasterDto.ConfigKey.ToLower() && x.IsActive).ConfigureAwait(false);
                    getConfigInformation.ConfigValue = configMasterDto.ConfigValue?.First().ToString().ToUpper() + configMasterDto.ConfigValue?.Substring(1).ToLower();
                    getConfigInformation.UpdatedBy = Convert.ToInt32(configMasterDto.LoggedInUserId);
                    getConfigInformation.UpdatedDateTime = DateTime.Now;
                    getConfigInformation.IPAddress = configMasterDto.IPAddress;
                    await _projectDBContext.SaveChangesAsync();
                    successResponseModel.ResponseStatus = (int)CrudImplementation.Update;
                }

            }
            catch
            {
                successResponseModel.ResponseStatus = (int)Status.Error;
                throw;
            }
            finally
            {
                await _projectDBContext.DisposeAsync();
            }
            return successResponseModel;
        }
        public async Task<SuccessResponseModel> DeleteConfigByIdAsync(DeleteRecordRequestModel deleteRecordRequestModel)
        {
            var responseModel = new SuccessResponseModel();
            try
            {
                var getConfigInformation = await _projectDBContext.configMaster.SingleOrDefaultAsync(x => x.ConfigId == Convert.ToInt64(deleteRecordRequestModel.DeleteRecordId) && x.IsActive).ConfigureAwait(false);
                if (getConfigInformation != null)
                {
                    getConfigInformation.IsActive = true;
                    getConfigInformation.DeletedBy = Convert.ToInt64(deleteRecordRequestModel.LoggedInUserId);
                    getConfigInformation.DeletedDateTime = DateTime.Now;
                    await _projectDBContext.SaveChangesAsync();
                    responseModel.ResponseStatus = (int)CrudImplementation.Delete;
                }
            }
            catch
            {
                responseModel.ResponseStatus = (int)Status.Error;
                throw;
            }
            finally
            {
                await _projectDBContext.DisposeAsync();
            }
            return responseModel;
        }

        
        #endregion
    }
}