﻿using Microsoft.EntityFrameworkCore;
using ProjectDataAccessLayer.Entities;

namespace DataAccessLayer.DBContext
{
    public class ProjectDBContext : DbContext
    {

        public ProjectDBContext()
        {

        }
        public ProjectDBContext(DbContextOptions<ProjectDBContext> dbContextOptions) : base(dbContextOptions)
        {

        }

        #region Admin module
        public DbSet<ConfigMaster> configMaster { get; set; }
        public DbSet<Users> users { get; set; }
        public DbSet<CentreMaster> centreMaster { get; set; }
        public DbSet<RoleMaster> roleMaster { get; set; }
        #endregion

        #region Shared module
        public DbSet<Login> login { get; set; }
       
        public DbSet<EmployeeDetails> employeeDetails { get; set; }

       #endregion


        #region ItemMaster module
        public DbSet<ItemMaster> ItemMaster { get; set; }

        #endregion


        #region ClientMaster module
        public DbSet<ClientMaster> ClientMaster { get; set; }

        #endregion

        #region Billing module
        public DbSet<Billing> Billing { get; set; }

        #endregion


    }
}
