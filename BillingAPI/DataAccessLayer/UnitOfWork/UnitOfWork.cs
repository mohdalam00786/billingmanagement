﻿
using AutoMapper;
using DataAccessLayer.DBContext;
using ProjectDataAccessLayer.IRepository;
using ProjectDataAccessLayer.Repository;
using System;

namespace DataAccessLayer.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private ProjectDBContext _projectDBContext;
        private IMapper _mapper;

        #region Admin Module
        private IAdminMasterRepository _adminMasterRepository;
        private IAdminUserRepository _adminUserRepository;
        #endregion

        #region Shared Module
        private IAuthenticationRepository _authenticationRepository;
        #endregion
        private IEmployeeDetailsRepository _employeeDetailsRepository;


        #region ItemMaster Module
        private IItemMasterRepository _itemMasterRepository;
        #endregion

        #region ClientMaster Module
        private IClientMasterRepository _clientMasterRepository;
        #endregion

        #region Billing Module
        private IBillingRepository _billingRepository;
        #endregion



        public UnitOfWork(ProjectDBContext projectDBContext, IMapper mapper)
        {
            _projectDBContext = projectDBContext;
            _mapper = mapper;
        }

        #region Admin Module
        public IAdminMasterRepository adminMasterRepository
        {
            get
            {
                return _adminMasterRepository = _adminMasterRepository ?? new AdminMasterRepository(_projectDBContext, _mapper);
            }
        }
        public IAdminUserRepository adminUserRepository
        {
            get
            {
                return _adminUserRepository = _adminUserRepository ?? new AdminUserRepository(_projectDBContext, _mapper);
            }
        }
        #endregion

        #region Shared Module
        public IAuthenticationRepository authenticationRepository
        {
            get
            {
                return _authenticationRepository = _authenticationRepository ?? new AuthenticationRepository(_projectDBContext);
            }
        }
        #endregion

        #region ItemMaster Module
        public IItemMasterRepository itemMasterRepository
        {

            get
            {
                return _itemMasterRepository = _itemMasterRepository ?? new ItemMasterRepository(_projectDBContext, _mapper);
            }

        }
        #endregion

        #region ClientMaster Module
        public IClientMasterRepository clientMasterRepository
        {

            get
            {
                return _clientMasterRepository =  _clientMasterRepository ?? new ClientMasterRepository(_projectDBContext, _mapper);
            }

        }
        #endregion

        #region Billing Module
        public IBillingRepository billingRepository
        {

            get
            {
                return _billingRepository = _billingRepository ?? new BillingRepository(_projectDBContext, _mapper);
            }

        }
        #endregion

        #region employeedetailsMaster Module
        public IEmployeeDetailsRepository employeeDetailsRepository
        {
            get
            {
                return _employeeDetailsRepository = _employeeDetailsRepository ?? new EmployeeDetailsRepository(_projectDBContext, _mapper);
            }
        }
        #endregion
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _projectDBContext.Dispose();
            }
        }
    }
}
