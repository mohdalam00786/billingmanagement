﻿using ProjectDataAccessLayer.IRepository;
using System;

namespace DataAccessLayer.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        #region Admin module
        IAdminMasterRepository adminMasterRepository { get; }
        IAdminUserRepository adminUserRepository { get; }
        #endregion

        #region Shared Module
        IAuthenticationRepository authenticationRepository { get; }
        #endregion

        #region employee Module
        IEmployeeDetailsRepository employeeDetailsRepository { get; }
        #endregion



        #region ItemMaster Module
        IItemMasterRepository itemMasterRepository { get; }
        #endregion

        #region ClientMaster Module
        IClientMasterRepository clientMasterRepository { get; }
        #endregion

        #region Billing Module
        IBillingRepository billingRepository { get; }
        #endregion

    }
}

