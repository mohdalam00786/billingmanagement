﻿using ProjectModels.RequestModels;
using ProjectModels.ResponseModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectDataAccessLayer.IRepository
{
    public interface IAuthenticationRepository
    {
        Task<AuthenticationResponseModel> AuthenticationAsync(AuthenticationRequestModel authenticationRequestModel);
        Task<List<CentreResponseModel>> GetAllCentreForLoginAsync();
    }
}
