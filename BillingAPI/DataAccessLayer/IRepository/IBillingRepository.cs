﻿using ProjectModels.RequestModels;
using ProjectModels.ResponseModels;
using System.Threading.Tasks;

namespace ProjectDataAccessLayer.IRepository
{
    public interface IBillingRepository
    {
        #region Billing implementation

        Task<GetBillingResponseModel> GetBillingAsync();
        Task<CustomerDetailsResponseModel> GetCustomerDetailsByMobileNo(string mobileNumber);
        Task<SuccessResponseModel> SaveBillingInformationAsync(BillingRequestModel billingRequestModel);

        Task<SuccessResponseModel> DeleteBillingByIdAsync(DeleteBillingtRequestModel deleteBillingRequestModel);

        #endregion
    }
}

