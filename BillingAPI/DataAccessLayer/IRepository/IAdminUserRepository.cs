﻿using ProjectModels.Dto;
using ProjectModels.RequestModels;
using ProjectModels.ResponseModels;
using System.Threading.Tasks;

namespace ProjectDataAccessLayer.IRepository
{
    public interface IAdminUserRepository
    {
        #region User master implementation
        Task<GetAllUserResponseModel> GetAllUsersAsync();
        Task<UserDropdownResponseModel> GetUserMasterDataAsync();
        Task<SuccessResponseModel> SaveUserInfomationAsync(UserDto userDto, long userId);
        Task<SuccessResponseModel> DeleteUserByIdAsync(DeleteRecordRequestModel deleteRecordRequestModel);
        #endregion
    }
}
