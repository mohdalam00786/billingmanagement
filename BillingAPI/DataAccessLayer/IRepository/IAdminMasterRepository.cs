﻿using ProjectModels.Dto;
using ProjectModels.RequestModels;
using ProjectModels.ResponseModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectDataAccessLayer.IRepository
{
    public interface IAdminMasterRepository
    {
        #region Centre master implementation
        Task<List<GetAllCentreResponseModel>> GetAllCentresAsync();
        Task<SuccessResponseModel> SaveCentreInfomationAsync(CentreMasterDto centreMasterDto, long centreId);
        Task<SuccessResponseModel> DeleteCentreByIdAsync(DeleteRecordRequestModel deleteRecordRequestModel);
        #endregion

        #region Item master implementation

        Task<GetItemMasterResponseModel> GetItemMasterAsync();
        Task<SuccessResponseModel> SaveItemMasterInformationAsync(ItemMasterRequestModel itemMasterRequestModel);

        Task<SuccessResponseModel> DeleteItemMasterByIdAsync(DeleteItemMasterRequestModel deleteItemMasterRequestModel);


        #endregion

        #region Client master implementation

        Task<GetClientMasterResponseModel> GetClientMasterAsync();
        Task<SuccessResponseModel> SaveClientMasterInformationAsync(ClientMasterRequestModel clientMasterRequestModel);

        Task<SuccessResponseModel> DeleteClientMasterByIdAsync(DeleteClientMasterRequestModel deleteClientMasterRequestModel);


        #endregion

        #region Role master implementation
        Task<List<RolesResponseModel>> GetAllRolesAsync();
        Task<SuccessResponseModel> SaveRolesInfomationAsync(RoleMasterDto roleMasterDto, long roleId);
        Task<SuccessResponseModel> DeleteRoleByIdAsync(DeleteRecordRequestModel deleteRecordRequestModel);
        #endregion

        #region Config master implementation
        Task<List<ConfigMasterResponseModel>> GetAllConfigAsync();
        Task<SuccessResponseModel> SaveConfigInfomationAsync(ConfigMasterDto configMasterDto, long configId);
        Task<SuccessResponseModel> DeleteConfigByIdAsync(DeleteRecordRequestModel deleteRecordRequestModel);
        #endregion
    }
}
