﻿using ProjectModels.RequestModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace ProjectDataAccessLayer.Entities
{
    [Table("tblBilling")]
    public class Billing : SharedEntitiesRequestModel
    {
        [Key]
        public long Id { get; set; }
        public string CustomerName { get; set; }
        public string CustomerMobileNumber { get; set; }
        public string CustomerAddress { get; set; }
        public string EmailId { get; set; }
        public double GrossAmount { get; set; }
        public double Discount { get; set; }
        public string DiscountBy { get; set; }
        public string DiscountReason { get; set; }
        public double NetAmount { get; set; }
        public string PaymentMethod { get; set; }
        public string Items { get; set; }
        public long CentreId { get; set; }
        public long ClientId { get; set; }

    }
}