﻿using ProjectModels.RequestModels;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ProjectDataAccessLayer.Entities
{
    [Table("tblrolemaster")]
    public class RoleMaster : SharedEntitiesRequestModel
    {
        [Key]
        public long RoleId { get; set; }
        public string RoleName { get; set; }
        public string RoleCode { get; set; }
        public string RoleDescription { get; set; }


    }
}
