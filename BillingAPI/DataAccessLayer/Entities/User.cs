﻿using ProjectModels.RequestModels;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectDataAccessLayer.Entities
{
    [Table("tblusers")]
    public class Users : SharedEntitiesRequestModel
    {
        [Key]
        public long UserId { get; set; }
        public string UserName { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string SpouseName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string BloodGroup { get; set; }
        public string Roles { get; set; }
        public string Centres { get; set; }
        public string MaritalStatus { get; set; }
        public string Password { get; set; }
        public string PrimaryContactNumber { get; set; }
        public string SecondryContactNumber { get; set; }
        public string EmergencyContactName { get; set; }
        public string EmergencyContactNumber { get; set; }
        public string EmailId { get; set; }
        public string PassportNumber { get; set; }
        public string PANCardNumber { get; set; }
        public string AdhaarCardNumber { get; set; }
        public string GuardianName { get; set; }
        public string LoginName { get; set; }
        public string PermanentAddress { get; set; }
        public string LandlineNumber { get; set; }
    }
}