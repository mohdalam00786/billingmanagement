﻿using System.ComponentModel.DataAnnotations.Schema;


namespace ProjectDataAccessLayer.Entities
{
    [Table("tbllogin")]
    public class Login
    {
        public long LoginId { get; set; }
        public long UserId { get; set; }
        public string Password { get; set; }
        public string EmailId { get; set; }
        public string LoginName { get; set; }
        public byte TotalAttemptCount { get; set; }
    }
}
