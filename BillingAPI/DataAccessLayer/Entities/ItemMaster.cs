﻿using ProjectModels.RequestModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataAccessLayer.Entities
{
    [Table("tblitemmaster")]
    public class ItemMaster : SharedEntitiesRequestModel
    {
        [Key]
        public int Id { get; set; }
        public string ItemName { get; set; }
        public int ItemPrice { get; set; }
        public string ItemCompany { get; set; }
        public DateTime ItemMFGDate { get; set; }
        public DateTime ItemExpiryDate { get; set; }
        
        
    }
}
