﻿using ProjectModels.RequestModels;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectDataAccessLayer.Entities
{
    [Table("tblconfigmaster")]
    public class ConfigMaster : SharedEntitiesRequestModel
    {
        [Key]
        public long ConfigId { get; set; }
        public string ConfigKey { get; set; }
        public string ConfigValue { get; set; }
        public string ConfigCode { get; set; }
    }
}