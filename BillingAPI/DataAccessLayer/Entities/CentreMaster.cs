﻿using ProjectModels.RequestModels;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectDataAccessLayer.Entities
{
    [Table("tblcentremaster")]
    public class CentreMaster : SharedEntitiesRequestModel
    {
        [Key]
        public long CentreId { get; set; }
        public string CentreName { get; set; }
        public string CentreCode { get; set; }
        public string Website { get; set; }
        public string Address { get; set; }
        public string MobileNumber { get; set; }
        public string LandlineNumber { get; set; }
        public string EmailId { get; set; }
    }
}
