﻿
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectDataAccessLayer.Entities
{
    [Table("employeedetails")]
    public class EmployeeDetails
    {
        [Key]
        public long ID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int Age { get; set; }
        public string Salary { get; set; }
        public string Address { get; set; }
        public string Gender { get; set; }
    }
}
