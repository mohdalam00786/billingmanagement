﻿using ProjectModels.RequestModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataAccessLayer.Entities
{
    [Table("tblclientmaster")]
    public class ClientMaster : SharedEntitiesRequestModel
    {
        [Key]
        public long Id { get; set; }
        public string ClientName { get; set; }
        public string ClientAddress { get; set; }
        public string OwnerName { get; set; }
        public string MobileNumber1 { get; set; }
        public string MobileNumber2 { get; set; }
        public string EmailId { get; set; }

    }
}