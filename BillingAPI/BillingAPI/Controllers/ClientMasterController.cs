﻿using AutoMapper;
using BillingAPI.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectBusinessLayer.IService;
using ProjectBusinessLayer.Service;
using ProjectModels.RequestModels;
using System.Threading.Tasks;
using System;
using UtilityLayer.SharedFunctions;
 
namespace ProjectAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientMasterController : BaseController
    {
        private IClientMasterService _ClientMasterService;

        public ClientMasterController(IClientMasterService ClientMasterService, IMapper mapper)
        {
            _ClientMasterService = ClientMasterService; 
            _mapper = mapper;
        }

        



        private string[] GetControllerActionName()
        {
            string controllerName = ControllerContext.ActionDescriptor.ControllerName;
            string actionName = ControllerContext.ActionDescriptor.ActionName;
            string[] controllerActionName = { controllerName, actionName };
            return controllerActionName;
        }
    }
}
