﻿
using AutoMapper;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using ProjectBusinessLayer.IService;
using ProjectModels.RequestModels;
using System;
using System.Threading.Tasks;
using UtilityLayer.SharedFunctions;

namespace BillingAPI.Controllers
{
    // [Authorize]
    [EnableCors("CorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : BaseController
    {
        private IAuthenticationService _IAuthenticationService;



        public AuthenticationController(
            IAuthenticationService iAuthenticationService,
            IMapper mapper
           )
        {
            _IAuthenticationService = iAuthenticationService;
            _mapper = mapper;
        }


        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> Login(AuthenticationRequestModel authenticateRequestModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                else
                {
                    var result = await _IAuthenticationService.AuthenticationAsync(authenticateRequestModel).ConfigureAwait(false);
                    return Ok(result);
                }
            }
            catch (Exception exception)
            {
                string[] controllerActionName = GetControllerActionName();
                LogError(exception, controllerActionName[0], controllerActionName[1], SharedFunctions.GetIPAddress().ToString());
                return null;
            }
        }       
        

        [HttpGet]
        [Route("GetAllCentreForLogin")]
        public async Task<IActionResult> GetAllCentreForLogin()
        {
            try
            {


                var centreMasterResponse = await _IAuthenticationService.GetAllCentreForLoginAsync().ConfigureAwait(false);
                return Ok(centreMasterResponse);

            }
            catch (Exception exception)
            {
                string[] controllerActionName = GetControllerActionName();
                LogError(exception, controllerActionName[0], controllerActionName[1], SharedFunctions.GetIPAddress().ToString());
                return null;
            }
        }
        private string[] GetControllerActionName()
        {
            string controllerName = ControllerContext.ActionDescriptor.ControllerName;
            string actionName = ControllerContext.ActionDescriptor.ActionName;
            string[] controllerActionName = { controllerName, actionName };
            return controllerActionName;
        }
    }
}
