﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectBusinessLayer.IService;
using ProjectBusinessLayer.Service;
using ProjectModels.RequestModels;
using System.Threading.Tasks;
using System;
using UtilityLayer.SharedFunctions;
using BillingAPI.Controllers;

namespace ProjectAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ItemMasterController : BaseController
    {
        private IItemMasterService _ItemMasterService;

        public ItemMasterController(IItemMasterService ItemMasterService, IMapper mapper)
        {
            _ItemMasterService = ItemMasterService;
            _mapper = mapper;
        }

        
        private string[] GetControllerActionName()
        {
            string controllerName = ControllerContext.ActionDescriptor.ControllerName;
            string actionName = ControllerContext.ActionDescriptor.ActionName;
            string[] controllerActionName = { controllerName, actionName };
            return controllerActionName;
        }
    }
}

