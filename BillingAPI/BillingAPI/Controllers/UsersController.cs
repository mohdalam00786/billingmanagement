﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ProjectBusinessLayer.IService;
using ProjectModels.RequestModels;
using System;
using System.Threading.Tasks;
using UtilityLayer.SharedFunctions;

namespace BillingAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : BaseController
    {
        private IAdminUserService _adminUserService;

        public UsersController(IAdminUserService adminUserService , IMapper mapper)
        {
            _adminUserService = adminUserService;
            _mapper = mapper;
        }

        #region User master implementation
        [HttpGet]
        [Route("GetAllUsers")]
        public async Task<IActionResult> GetAllUsers()
        {
            try
            {
                var allUsers = await _adminUserService.GetAllUsersAsync().ConfigureAwait(false);
                return Ok(allUsers);
            }
            catch (Exception exception)
            {
                string[] controllerActionName = GetControllerActionName();
                LogError(exception, controllerActionName[0], controllerActionName[1], SharedFunctions.GetIPAddress().ToString());
                return null;
            }
        }
        [HttpGet]
        [Route("GetUserMasterData")]
        public async Task<IActionResult> GetUserMasterData()
          {
            try
            {
                var allUsers = await _adminUserService.GetUserMasterDataAsync().ConfigureAwait(false); 
                return Ok(allUsers);
            }
            catch (Exception exception)
            {
                string[] controllerActionName = GetControllerActionName();
                LogError(exception, controllerActionName[0], controllerActionName[1], SharedFunctions.GetIPAddress().ToString());
                return null;
            }
        }
        [HttpPost]
        [Route("SaveUserInformation")]
        public async Task<IActionResult> SaveUserInformation(UserRequestModel userRequestModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                else
                {
                    userRequestModel.IPAddress = SharedFunctions.GetIPAddress().ToString();
                    var successResponseModel = await _adminUserService.SaveUserInfomationAsync(userRequestModel).ConfigureAwait(false);
                    return Ok(successResponseModel);
                }
            }
            catch (Exception exception)
            {
                string[] controllerActionName = GetControllerActionName();
                LogError(exception, controllerActionName[0], controllerActionName[1], userRequestModel.IPAddress);
                return null;
            }
        }        

        [HttpPost]
        [Route("DeleteUserById")]
        public async Task<IActionResult> DeleteUserById(DeleteRecordRequestModel deleteRecordRequestModel)
        {
            try
            {
                var userResult = await _adminUserService.DeleteUserByIdAsync(deleteRecordRequestModel).ConfigureAwait(false);
                return Ok(userResult);
            }
            catch (Exception exception)
            {
                string[] controllerActionName = GetControllerActionName();
                LogError(exception, controllerActionName[0], controllerActionName[1], SharedFunctions.GetIPAddress().ToString());
                return null;
            }
        }
        #endregion

        private string[] GetControllerActionName()
        {
            string controllerName = ControllerContext.ActionDescriptor.ControllerName;
            string actionName = ControllerContext.ActionDescriptor.ActionName;
            string[] controllerActionName = { controllerName, actionName };
            return controllerActionName;
        }
    }
}
