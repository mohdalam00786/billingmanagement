﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ProjectBusinessLayer.IService;
using ProjectBusinessLayer.Service;
using ProjectModels.RequestModels;
using ProjectModels.ResponseModels;
using System;
using System.Threading.Tasks;
using UtilityLayer.SharedFunctions;

namespace BillingAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MasterController : BaseController
    {
        private IAdminMasterService _adminMasterService;

        public MasterController(IAdminMasterService adminMasterService, IMapper mapper)
        {
            _adminMasterService = adminMasterService;
            _mapper = mapper;
        }        


        #region Centre master implementation
        [HttpGet]
        [Route("GetAllCentres")]
        public async Task<IActionResult> GetAllCentres()
        {
            try
            {
                var allCentres = await _adminMasterService.GetAllCentresAsync().ConfigureAwait(false);                             
                return Ok(allCentres);
            }
            catch (Exception exception)
            {
                string[] controllerActionName = GetControllerActionName();
                LogError(exception, controllerActionName[0], controllerActionName[1], SharedFunctions.GetIPAddress().ToString());
                return null;
            }
        }

        [HttpPost]
        [Route("SaveCentreInformation")]
        public async Task<IActionResult> SaveCentreInformation(CentreMasterRequestModel centreMasterRequestModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                else
                {
                    centreMasterRequestModel.IPAddress = SharedFunctions.GetIPAddress().ToString();
                    SuccessResponseModel successResponseModel = await _adminMasterService.SaveCentreInfomationAsync(centreMasterRequestModel).ConfigureAwait(false);                  
                    return Ok(successResponseModel);
                }
            }
            catch (Exception exception)
            {
                string[] controllerActionName = GetControllerActionName();
                LogError(exception, controllerActionName[0], controllerActionName[1], centreMasterRequestModel.IPAddress);
                return null;
            }
        }

        [HttpPost]
        [Route("DeleteCentreById")]
        public async Task<IActionResult> DeleteCentreById(DeleteRecordRequestModel deleteRecordRequestModel)
        {
            try
            {              
                var centreResult = await _adminMasterService.DeleteCentreByIdAsync(deleteRecordRequestModel).ConfigureAwait(false);
                return Ok(centreResult);
            }
            catch (Exception exception)
            {
                string[] controllerActionName = GetControllerActionName();
                LogError(exception, controllerActionName[0], controllerActionName[1], SharedFunctions.GetIPAddress().ToString());
                return null;
            }
        }
        #endregion

        #region Item master implementation

        [HttpGet]
        [Route("GetItemMaster")]
        public async Task<IActionResult> GetItemMaster()
        {
            try
            {
                var allUsers = await _adminMasterService.GetItemMasterAsync().ConfigureAwait(false);
                return Ok(allUsers);
            }
            catch (Exception exception)
            {
                string[] controllerActionName = GetControllerActionName();
                LogError(exception, controllerActionName[0], controllerActionName[1], SharedFunctions.GetIPAddress().ToString());
                return null;
            }
        }



        [HttpPost]
        [Route("SaveItemMasterInformation")]

        public async Task<IActionResult> SaveItemMasterInformation(ItemMasterRequestModel ItemMasterRequestModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);

                }
                else
                {
                    ItemMasterRequestModel.IPAddress = SharedFunctions.GetIPAddress().ToString();
                    var successResponseModel = await _adminMasterService.SaveItemMasterInformationAsync(ItemMasterRequestModel).ConfigureAwait(false);
                    return Ok(successResponseModel);
                }
            }
            catch (Exception exception)
            {
                string[] controllerActionName = GetControllerActionName();
                LogError(exception, controllerActionName[0], controllerActionName[1], ItemMasterRequestModel.IPAddress);
                return null;
            }
        }

        [HttpPost]
        [Route("DeleteItemMasterById")]
        public async Task<IActionResult> DeleteItemMasterById(DeleteItemMasterRequestModel deleteItemMasterRequestModel)
        {
            try
            {
                var ItemMasterResult = await _adminMasterService.DeleteItemMasterByIdAsync(deleteItemMasterRequestModel).ConfigureAwait(false);
                return Ok(ItemMasterResult);
            }
            catch (Exception exception)
            {
                string[] controllerActionName = GetControllerActionName();
                LogError(exception, controllerActionName[0], controllerActionName[1], SharedFunctions.GetIPAddress().ToString());
                return null;
            }
        }
        #endregion

        #region Client master implementation

        [HttpGet]
        [Route("GetClientMaster")]
        public async Task<IActionResult> GetClientMaster()
        {
            try
            {
                var allUsers = await _adminMasterService.GetClientMasterAsync().ConfigureAwait(false);
                return Ok(allUsers);
            }
            catch (Exception exception)
            {
                string[] controllerActionName = GetControllerActionName();
                LogError(exception, controllerActionName[0], controllerActionName[1], SharedFunctions.GetIPAddress().ToString());
                return null;
            }
        }



        [HttpPost]
        [Route("SaveClientMasterInformation")]

        public async Task<IActionResult> SaveClientMasterInformation(ClientMasterRequestModel ClientMasterRequestModel)

        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);

                }
                else
                {
                    ClientMasterRequestModel.IPAddress = SharedFunctions.GetIPAddress().ToString();
                    var successResponseModel = await _adminMasterService.SaveClientMasterInformationAsync(ClientMasterRequestModel).ConfigureAwait(false);
                    return Ok(successResponseModel);
                }
            }
            catch (Exception exception)
            {
                string[] controllerActionName = GetControllerActionName();
                LogError(exception, controllerActionName[0], controllerActionName[1], ClientMasterRequestModel.IPAddress);
                return null;
            }
        }

        [HttpPost]
        [Route("DeleteClientMasterById")]
        public async Task<IActionResult> DeleteClientMasterById(DeleteClientMasterRequestModel deleteClientMasterRequestModel)
        {
            try
            {
                var ClientMasterResult = await _adminMasterService.DeleteClientMasterByIdAsync(deleteClientMasterRequestModel).ConfigureAwait(false);
                return Ok(ClientMasterResult);
            }
            catch (Exception exception)
            {
                string[] controllerActionName = GetControllerActionName();
                LogError(exception, controllerActionName[0], controllerActionName[1], SharedFunctions.GetIPAddress().ToString());
                return null;
            }
        }
        #endregion


        #region Role master implementation
        [HttpGet]
        [Route("GetAllRoles")]
        public async Task<IActionResult> GetAllRoles()
        {
            try
            {
                var allRoles = await _adminMasterService.GetAllRolesAsync().ConfigureAwait(false);
                return Ok(allRoles);
            }
            catch (Exception exception)
            {
                string[] controllerActionName = GetControllerActionName();
                LogError(exception, controllerActionName[0], controllerActionName[1], SharedFunctions.GetIPAddress().ToString());
                return null;
            }
        }

        [HttpPost]
        [Route("SaveRoleInformation")]
        public async Task<IActionResult> SaveRoleInformation(RoleMasterRequestModel roleMasterRequestModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                else
                {
                    roleMasterRequestModel.IPAddress = SharedFunctions.GetIPAddress().ToString();
                    SuccessResponseModel successResponseModel = await _adminMasterService.SaveRolesInfomationAsync(roleMasterRequestModel).ConfigureAwait(false);                   
                    return Ok(successResponseModel);
                }
            }
            catch (Exception exception)
            {
                string[] controllerActionName = GetControllerActionName();
                LogError(exception, controllerActionName[0], controllerActionName[1], roleMasterRequestModel.IPAddress);
                return null;
            }
        }

        [HttpPost]
        [Route("DeleteRoleById")]
        public async Task<IActionResult> DeleteRoleById(DeleteRecordRequestModel deleteRecordRequestModel)
        {
            try
            {
                var roleResult = await _adminMasterService.DeleteRoleByIdAsync(deleteRecordRequestModel).ConfigureAwait(false);
                return Ok(roleResult);
            }
            catch (Exception exception)
            {
                string[] controllerActionName = GetControllerActionName();
                LogError(exception, controllerActionName[0], controllerActionName[1], SharedFunctions.GetIPAddress().ToString());
                return null;
            }
        }
        #endregion

        #region Config master implementation
        [HttpGet]
        [Route("GetAllConfig")]
        public async Task<IActionResult> GetAllConfig()
        {
            try
            {
                var allConfig = await _adminMasterService.GetAllConfigAsync().ConfigureAwait(false);
                return Ok(allConfig);
            }
            catch (Exception exception)
            {
                string[] controllerActionName = GetControllerActionName();
                LogError(exception, controllerActionName[0], controllerActionName[1], SharedFunctions.GetIPAddress().ToString());
                return null;
            }
        }

        [HttpPost]
        [Route("SaveConfigInformation")]
        public async Task<IActionResult> SaveConfigInformation(ConfigMasterRequestModel configMasterRequestModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                else
                {
                    configMasterRequestModel.IPAddress = SharedFunctions.GetIPAddress().ToString();
                    SuccessResponseModel successResponseModel = await _adminMasterService.SaveConfigInfomationAsync(configMasterRequestModel).ConfigureAwait(false);
                    return Ok(successResponseModel);
                }
            }
            catch (Exception exception)
            {
                string[] controllerActionName = GetControllerActionName();
                LogError(exception, controllerActionName[0], controllerActionName[1], configMasterRequestModel.IPAddress);
                return null;
            }
        }

        [HttpPost]
        [Route("DeleteConfigById")]
        public async Task<IActionResult> DeleteConfigById(DeleteRecordRequestModel deleteRecordRequestModel)
        {
            try
            {
                var result = await _adminMasterService.DeleteConfigByIdAsync(deleteRecordRequestModel).ConfigureAwait(false);
                return Ok(result);
            }
            catch (Exception exception)
            {
                string[] controllerActionName = GetControllerActionName();
                LogError(exception, controllerActionName[0], controllerActionName[1], SharedFunctions.GetIPAddress().ToString());
                return null;
            }
        }
        #endregion

        




        private string[] GetControllerActionName()
        {
            string controllerName = ControllerContext.ActionDescriptor.ControllerName;
            string actionName = ControllerContext.ActionDescriptor.ActionName;
            string[] controllerActionName = { controllerName, actionName };
            return controllerActionName;
        }
    }
}

