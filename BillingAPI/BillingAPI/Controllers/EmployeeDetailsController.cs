﻿using AutoMapper;
using BillingAPI.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectModels.RequestModels;
using System.Threading.Tasks;
using System;
using UtilityLayer.SharedFunctions;
using ProjectBusinessLayer.IService;

namespace ProjectAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeDetailsController : BaseController
    {
        private IEmployeeDetailsService _ImployeeDetailsService;



        public EmployeeDetailsController(
            IEmployeeDetailsService ImployeeDetailsService,
            IMapper mapper
           )
        {
            _ImployeeDetailsService = ImployeeDetailsService;
            _mapper = mapper;
        }


        [HttpPost]
        [Route("SaveEmployeeDetails")]
        public async Task<IActionResult> SaveEmployeeDetails(EmployeeDetailsRequestModel employeeDetailsRequestModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                else
                {
                    var result = await _ImployeeDetailsService.SaveEmployeeDetails(employeeDetailsRequestModel).ConfigureAwait(false);
                    return Ok(result);
                }
            }
            catch (Exception exception)
            {
                string[] controllerActionName = GetControllerActionName();
                LogError(exception, controllerActionName[0], controllerActionName[1], SharedFunctions.GetIPAddress().ToString());
                return null;
            }
        }
        private string[] GetControllerActionName()
        {
            string controllerName = ControllerContext.ActionDescriptor.ControllerName;
            string actionName = ControllerContext.ActionDescriptor.ActionName;
            string[] controllerActionName = { controllerName, actionName };
            return controllerActionName;
        }

    }
}
        