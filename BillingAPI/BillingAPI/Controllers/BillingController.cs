﻿using AutoMapper;
using BillingAPI.Controllers;
using Microsoft.AspNetCore.Mvc;
using ProjectBusinessLayer.IService;
using ProjectModels.RequestModels;
using System.Threading.Tasks;
using System;
using UtilityLayer.SharedFunctions;
  

namespace ProjectAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BillingController : BaseController
    {
        private IBillingService _billingService; 

        public BillingController(IBillingService BillingService, IMapper mapper)
        {
            _billingService = BillingService; 
            _mapper = mapper;
        }

        #region Billing implementation   
          

        [HttpGet]
        [Route("GetBilling")]
        public async Task<IActionResult> GetBilling()
        {
            try
            {
                var allUsers = await _billingService.GetBillingAsync().ConfigureAwait(false);
                return Ok(allUsers);
            }
            catch (Exception exception)
            {
                string[] controllerActionName = GetControllerActionName();
                LogError(exception, controllerActionName[0], controllerActionName[1], SharedFunctions.GetIPAddress().ToString());
                return null; 
            }
        }

        [HttpGet]
        [Route("GetCustomerDetails")]
        public async Task<IActionResult> GetCustomerDetails(string MobileNumber)
        {
            try
            {
                var allUsers = await _billingService.GetCustomerDetailsByMobileNo(MobileNumber).ConfigureAwait(false);
                return Ok(allUsers);
            }
            catch (Exception exception)
            {
                string[] controllerActionName = GetControllerActionName();
                LogError(exception, controllerActionName[0], controllerActionName[1], SharedFunctions.GetIPAddress().ToString());
                return null;
            }
        }


        [HttpPost]
        [Route("SaveBillingInformation")]

        public async Task<IActionResult> SaveBillingInformation(BillingRequestModel BillingRequestModel)

        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);

                }
                else
                {
                    BillingRequestModel.IPAddress = SharedFunctions.GetIPAddress().ToString();
                    var successResponseModel = await _billingService.SaveBillingInformationAsync(BillingRequestModel).ConfigureAwait(false);
                    return Ok(successResponseModel);
                }
            }
            catch (Exception exception)
            {
                string[] controllerActionName = GetControllerActionName();
                LogError(exception, controllerActionName[0], controllerActionName[1], BillingRequestModel.IPAddress);
                return null;
            }
        }

        [HttpPost]
        [Route("DeleteBillingById")]
        public async Task<IActionResult> DeleteBillingById(DeleteBillingtRequestModel deletebillingRequestModel)
        {
            try
            {
                var BillingResult = await _billingService.DeleteBillingByIdAsync(deletebillingRequestModel).ConfigureAwait(false);
                return Ok(BillingResult);
            }
            catch (Exception exception)
            {
                string[] controllerActionName = GetControllerActionName();
                LogError(exception, controllerActionName[0], controllerActionName[1], SharedFunctions.GetIPAddress().ToString());
                return null;
            }
        }
        #endregion




        private string[] GetControllerActionName()
        {
            string controllerName = ControllerContext.ActionDescriptor.ControllerName;
            string actionName = ControllerContext.ActionDescriptor.ActionName;
            string[] controllerActionName = { controllerName, actionName };
            return controllerActionName;
        }
    }
}
