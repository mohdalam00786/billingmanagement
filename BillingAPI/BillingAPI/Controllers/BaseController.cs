﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System;

namespace BillingAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaseController : Controller
    {
        protected IMapper _mapper;
        protected void LogError(Exception exception, string controllerName, string actionName, string ipAddress)
        {
            string innerMessage = (exception.InnerException != null)
                         ? exception.InnerException.Message
                         : "";
            Log.Fatal(Environment.NewLine +  
                "IP Address: " + ipAddress + Environment.NewLine +
                "Controller Name: " + controllerName + Environment.NewLine + 
                "Action Name: " + actionName + Environment.NewLine +
                "ErrorMessage: " + exception.Message + Environment.NewLine + 
                "InnerException: " + innerMessage + Environment.NewLine + 
                "StackTrace: " + exception.StackTrace + Environment.NewLine+ Environment.NewLine);
        }               
    }
    
}
