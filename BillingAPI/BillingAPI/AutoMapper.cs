﻿
using AutoMapper;
using ProjectDataAccessLayer.Entities;
using ProjectModels.Dto;
using ProjectModels.RequestModels;
using ProjectModels.ResponseModels;

namespace BillingAPI
{
    public class AutoMapper : Profile
    {
        public AutoMapper()
        {
            CreateMap<AuthenticationRequestModel, AuthenticationResponseModel>().ReverseMap();
            CreateMap<CentreMasterDto, CentreMaster>().ReverseMap();
            CreateMap<CentreMasterDto, CentreMasterRequestModel>().ReverseMap();           
            CreateMap<RoleMasterDto, RoleMaster>().ReverseMap();
            CreateMap<RoleMasterDto, RoleMasterRequestModel>().ReverseMap();
            CreateMap<CentreResponseModel, GetAllCentreResponseModel>().ReverseMap();
            CreateMap<UserDto, SuccessResponseModel>().ReverseMap();
            CreateMap<UserDto, Users>().ReverseMap();
            CreateMap<UserDto, UserRequestModel>().ReverseMap();
            CreateMap<ConfigMasterDto, ConfigMaster>().ReverseMap();
            CreateMap<ConfigMasterDto, ConfigMasterRequestModel>().ReverseMap();
            CreateMap<EmployeeDetailsRequestModel, EmployeeDetails>().ReverseMap();
            CreateMap<ItemMasterRequestModel, ItemMaster>().ReverseMap();
            CreateMap<ClientMasterRequestModel, ClientMaster>().ReverseMap();
            CreateMap<BillingRequestModel, Billing>().ReverseMap();

        }
    }
}
