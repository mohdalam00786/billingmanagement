﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectModels.RequestModels
{
    public class SharedEntitiesRequestModel
    {
        public long? CreatedBy { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdatedDateTime { get; set; }
        public long? DeletedBy { get; set; }
        public DateTime? DeletedDateTime { get; set; }
        public bool IsActive { get; set; }
        public string IPAddress { get; set; }
        [NotMapped]
        public string LoggedInUserId { get; set; }
        

    }
}
