﻿using System.ComponentModel.DataAnnotations;


namespace ProjectModels.RequestModels
{
    public class CentreMasterRequestModel : SharedEntitiesRequestModel

    {
        public string CentreId { get; set; }
        [Required]
        public string CentreName { get; set; }
        public string CentreCode { get; set; }
        public string Website { get; set; }
        public string Address { get; set; }
        public string MobileNumber { get; set; }
        public string LandlineNumber { get; set; }
        public string EmailId { get; set; }
    }
}
