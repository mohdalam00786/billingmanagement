﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectModels.RequestModels
{
    public class BillingRequestModel : SharedEntitiesRequestModel

    {
        public long Id { get; set; }
        [Required]
        public string CustomerName { get; set; }
        [Required]
        public string CustomerMobileNumber { get; set; }
        [Required]
        public string CustomerAddress { get; set; }
        [Required]
        public string EmailId { get; set; }
        public double GrossAmount { get; set; }
        [Required]
        public double Discount { get; set; }
        public string DiscountBy { get; set; }
        public string DiscountReason { get; set; }
        public double NetAmount { get; set; }
        [Required]
        public string PaymentMethod { get; set; }
        [Required]
        public string Items { get; set; }
        [Required]
        public long CentreId { get; set; }
        public long ClientId { get; set; }

    }
}