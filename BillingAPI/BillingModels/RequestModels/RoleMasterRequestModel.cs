﻿using System.ComponentModel.DataAnnotations;


namespace ProjectModels.RequestModels
{
    public class RoleMasterRequestModel : SharedEntitiesRequestModel
    {
        public string RoleId { get; set; }
        [Required]
        public string RoleName { get; set; }
        public string RoleDescription { get; set; }

    }
}
