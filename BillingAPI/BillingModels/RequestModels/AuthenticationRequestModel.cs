﻿using System.ComponentModel.DataAnnotations;


namespace ProjectModels.RequestModels
{
    public class AuthenticationRequestModel
    {
        [Required]
        public string LoginName { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string Centre { get; set; }
    }
}
