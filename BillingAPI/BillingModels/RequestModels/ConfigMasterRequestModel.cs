﻿namespace ProjectModels.RequestModels
{
    public class ConfigMasterRequestModel : SharedEntitiesRequestModel
    {
        public string ConfigId { get; set; }
        public string ConfigKey { get; set; }
        public string ConfigValue { get; set; }

    }
}
