﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProjectModels.RequestModels
{
    public class UserRequestModel : SharedEntitiesRequestModel
    {
        public string UserId { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        public string DateOfBirth { get; set; }
        [Required]
        public string Gender { get; set; }
        [Required]
        public string BloodGroup { get; set; }
        [Required]
        public string MaritalStatus { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string SpouseName { get; set; }
        public string GuardianName { get; set; }

        public string PassportNumber { get; set; }
        public string PANCardNumber { get; set; }
        public string AdhaarCardNumber { get; set; }

        [Required]
        public string PrimaryContactNumber { get; set; }
        public string SecondryContactNumber { get; set; }
        public string LandlineNumber { get; set; }
        public string EmergencyContactName { get; set; }
        public string EmergencyContactNumber { get; set; }
        [Required]
        public string EmailId { get; set; }
        public string PermanentAddress { get; set; }
        [Required]
        public string LoginName { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string Centres { get; set; }
        [Required]
        public string Roles { get; set; }
    }


    public class ProjectSharedPropeties
    {
        public long ResponseStatus { get; set; }
        public string IPAddress { get; set; }
        public string LoggedInUserId { get; set; }

    }
}

