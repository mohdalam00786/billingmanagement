﻿namespace ProjectModels.RequestModels
{
    public class DeleteRecordRequestModel
    {
        public string DeleteRecordId { get; set; }
        public string LoggedInUserId { get; set; }


    }
}
