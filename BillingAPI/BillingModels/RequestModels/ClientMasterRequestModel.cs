﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ProjectModels.RequestModels
{
    public class ClientMasterRequestModel : SharedEntitiesRequestModel

    {
        public long Id { get; set; }
        [Required]
        public string ClientName { get; set; }
        [Required]
        public string ClientAddress { get; set; }
        [Required]
        public string OwnerName { get; set; }
        [Required]
        public string MobileNumber1 { get; set; }
        [Required]
        public string MobileNumber2 { get; set; }
        public string EmailId { get; set; }

    }
}
