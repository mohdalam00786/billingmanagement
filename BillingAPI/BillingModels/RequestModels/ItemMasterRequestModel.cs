﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectModels.RequestModels
{
    public class ItemMasterRequestModel : SharedEntitiesRequestModel
    
    {
        public int Id { get; set; }
        public string ItemName { get; set; }
        public int ItemPrice { get; set; }
        public string ItemCompany { get; set; }
        public DateTime ItemMFGDate { get; set; }
        public DateTime ItemExpiryDate { get; set; }
        

    }
}
