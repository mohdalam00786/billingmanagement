﻿

namespace ProjectModels.RequestModels
{
    public class EmployeeDetailsRequestModel
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int Age { get; set; }
        public string Salary { get; set; }
        public string Address { get; set; }
        public string Gender { get; set; }
    }
}
