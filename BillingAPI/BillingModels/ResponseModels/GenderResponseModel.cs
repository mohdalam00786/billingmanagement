﻿namespace ProjectModels.ResponseModels
{
    public class GenderResponseModel
    {
        public int GenderId { get; set; }
        public string Gender { get; set; }
        public string GenderCode { get; set; }
    }
}
