﻿using System.Collections.Generic;

namespace ProjectModels.ResponseModels
{
    public class LoggedInUserResponseModel
    {
        public string UserName { get; set; }
        public List<CentreResponseModel> CentreList { get; set; }
    }
}
