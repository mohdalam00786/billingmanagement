﻿using System.Collections.Generic;

namespace ProjectModels.ResponseModels
{
    public class GetBillingResponseModel : SuccessResponseModel
    {
        public List<BillingList> BillingList { get; set; }

    }

    public class BillingList : CustomerDetailsResponseModel
    {
        public long Id { get; set; }

        public double GrossAmount { get; set; }
        public double Discount { get; set; }
        public string DiscountBy { get; set; }
        public string DiscountReason { get; set; }
        public double NetAmount { get; set; }
        public string PaymentMethod { get; set; }
        public string Items { get; set; }
        public long CentreId { get; set; }
        public long ClientId { get; set; }
        public bool IsActive { get; set; }

    }

    public class CustomerDetailsResponseModel : SuccessResponseModel
    {
        public string CustomerName { get; set; }
        public string CustomerMobileNumber { get; set; }
        public string CustomerAddress { get; set; }
        public string EmailId { get; set; }
    }
}