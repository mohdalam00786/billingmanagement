﻿namespace ProjectModels.ResponseModels
{
    public class BloodGroupResponseModel
    {
        public int BloodGroupId { get; set; }
        public string BloodGroup { get; set; }
        public string BloodGroupCode { get; set; }

    }
}
