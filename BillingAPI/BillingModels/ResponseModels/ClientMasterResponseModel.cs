﻿
using System.Collections.Generic;

namespace ProjectModels.ResponseModels
{
    public class GetClientMasterResponseModel : SuccessResponseModel
    {
        public List<ClientMasterList> ClientMasterList { get; set; }

    }

    public class ClientMasterList
    {
        public long Id { get; set; }
        public string ClientName { get; set; }
        public string ClientAddress { get; set; }
        public string OwnerName { get; set; }
        public string MobileNumber1 { get; set; }
        public string MobileNumber2 { get; set; }
        public string EmailId { get; set; }
        public bool IsActive { get; set; }
    }
}