﻿using System.Collections.Generic;

namespace ProjectModels.ResponseModels
{
    public class UserDropdownResponseModel : SuccessResponseModel
    {
        public List<GenderResponseModel> GenderList { get; set; }
        public List<BloodGroupResponseModel> BloodGroupList { get; set; }
        public List<MaritalStatusResponseModel> MaritalStatusList { get; set; }
        public List<RolesResponseModel> RoleList { get; set; }
        public List<CentreResponseModel> CentreList { get; set; }
    }
}
