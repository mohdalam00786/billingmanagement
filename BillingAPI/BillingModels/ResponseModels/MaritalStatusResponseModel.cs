﻿namespace ProjectModels.ResponseModels
{
    public class MaritalStatusResponseModel
    {
        public int MaritalStatusId { get; set; }
        public string MaritalStatus { get; set; }
        public string MaritalStatusCode { get; set; }

    }
}
