﻿namespace ProjectModels.ResponseModels
{
    public class CentreResponseModel
    {
        public long CentreId { get; set; }
        public string CentreName { get; set; }
    }
}
