﻿

using System.Collections.Generic;

namespace ProjectModels.ResponseModels
{
    public class AuthenticationResponseModel : SuccessResponseModel
    {
        public string JsonWebToken { get; set; }
        public byte TotalAttemptCount { get; set; }
        public List<RolesResponseModel> RoleList { get; set; }
        public long UserId { get; set; }
        public string UserName { get; set; }
        public string CentreName { get; set; }


    }
}
