﻿namespace ProjectModels.ResponseModels
{
    public class ConfigMasterResponseModel : SuccessResponseModel
    {
        public long ConfigId { get; set; }
        public string ConfigKey { get; set; }
        public string ConfigValue { get; set; }
    }
}
