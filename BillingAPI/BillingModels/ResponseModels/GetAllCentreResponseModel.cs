﻿namespace ProjectModels.ResponseModels
{
    public class GetAllCentreResponseModel : SuccessResponseModel
    {
        public long CentreId { get; set; }
        public string CentreName { get; set; }
        public string CentreCode { get; set; }
        public string Website { get; set; }
        public string Address { get; set; }
        public string MobileNumber { get; set; }
        public string LandlineNumber { get; set; }
        public string EmailId { get; set; }
    }
}
