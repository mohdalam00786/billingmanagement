﻿namespace ProjectModels.ResponseModels
{
    public class RolesResponseModel : SuccessResponseModel
    {
        public long RoleId { get; set; }
        public string RoleName { get; set; }
        public string RoleDescription { get; set; }
        public string RoleCode { get; set; }

    }
}
