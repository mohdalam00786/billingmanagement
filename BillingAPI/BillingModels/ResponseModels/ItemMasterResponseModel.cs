﻿using System;
using System.Collections.Generic;


namespace ProjectModels.ResponseModels
{
    public class GetItemMasterResponseModel : SuccessResponseModel
    {
        public List<ItemMasterList> ItemMasterList { get; set; }

    }

    public class ItemMasterList
    {
        
        public int Id { get; set; }
        public string ItemName { get; set; }
        public int ItemPrice { get; set; }
        public string ItemCompany { get; set; }
        public DateTime ItemMFGDate { get; set; }
        public DateTime ItemExpiryDate { get; set; }
        public bool IsActive { get; set; }


    }
}

