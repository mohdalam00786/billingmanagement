﻿using ProjectModels.RequestModels;

namespace ProjectModels.Dto
{
    public class RoleMasterDto : ProjectSharedPropeties
    {
        public string RoleName { get; set; }
        public string RoleCode { get; set; }
        public string RoleDescription { get; set; }
    }
}
