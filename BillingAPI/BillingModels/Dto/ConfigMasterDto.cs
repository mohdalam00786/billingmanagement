﻿using ProjectModels.RequestModels;

namespace ProjectModels.Dto
{
    public class ConfigMasterDto : ProjectSharedPropeties
    {
        public string ConfigKey { get; set; }
        public string ConfigValue { get; set; }
    }
}
