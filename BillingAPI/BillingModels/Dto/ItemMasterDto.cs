﻿using ProjectModels.RequestModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectModels.Dto
{

    public class ItemMasterDto : ProjectSharedPropeties
    {
        public int Id { get; set; }
        public string ItemName { get; set; }
        public int ItemPrice { get; set; }
        public string ItemCompany { get; set; }
        public DateTime ItemMFGDate { get; set; }
        public DateTime ItemExpiryDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedDateTime { get; set; }
        public int DeletedBy { get; set; }
        public string IsActive { get; set; }
        
    }
}
