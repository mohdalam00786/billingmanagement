export class Global {

    public static BaseClientURL = '';
    public static API_ENDPOINT = '';
    public static AUTH_ENDPOINT = '';
    public static EncyptKey = '';
    public static UserDetails = 'UserDetails';
    public static ShowAdmin = 'ShowAdmin';



    public static Init(appConfig:any) {
        this.setEnvironment(appConfig);
    }

    static async setEnvironment(appConfig:any) {
        this.API_ENDPOINT = appConfig["API_ENDPOINT"];
        this.AUTH_ENDPOINT = appConfig["AUTH_ENDPOINT"];
    }

}