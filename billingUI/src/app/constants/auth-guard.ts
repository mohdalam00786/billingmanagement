import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { Global } from './global-config';
import { UserService } from '../services/common/user.service';
@Injectable({
    providedIn: 'root'
})
export class AuthGuard  {
    constructor(private router: Router,
        private userService: UserService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        debugger;
        let userDetails=this.userService.GetCurrentUser(Global.UserDetails)
        if (userDetails) {
            return true;
        }
        this.router.navigate(['/Login']);
        return false;
    }
}