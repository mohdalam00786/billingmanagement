export enum CrudImplementation {    
    Create = 1,
    Read = 2,
    Update = 3,
    Delete = 4
}

export enum Gender {
    Male = 1,
    Female = 2,
    Other = 3
}
export enum Status
{
    AlreadyExist=10,
    LoginSuccess = 11,
    LoginFail = 12,
    Error=13
}