export class ApiControllers
{
    public static Authentication = 'Authentication';
    public static Master="Master";
    public static Users="Users";
}

export class APIActions
{
    public static Login = 'Login';
    public static GetAllCentreForLogin="GetAllCentreForLogin";
//Users
public static GetAllUsers="GetAllUsers";
public static GetUserMasterData="GetUserMasterData";
public static SaveUserInformation="SaveUserInformation";   
public static DeleteUserById="DeleteUserById";

//CentreMaster
public static SaveCentreInformation="SaveCentreInformation";
public static GetAllCentres="GetAllCentres";
public static DeleteCentreById="DeleteCentreById";

//RoleMaster
public static SaveRoleInformation="SaveRoleInformation";
public static GetAllRoles="GetAllRoles";
public static DeleteRoleById="DeleteRoleById";

 //ConfigMaster
 public static SaveConfigInformation="SaveConfigInformation";
 public static GetAllConfig="GetAllConfig";
 public static DeleteConfigById="DeleteConfigById";
}