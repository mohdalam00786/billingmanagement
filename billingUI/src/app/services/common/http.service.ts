import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Global } from 'src/app/constants/global-config';


@Injectable({
  providedIn: 'root'
})
export class HttpService {
  httpOptionsLogin = {
    headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' }),
  };
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  httpOptionsWithHeader = {
    headers: new HttpHeaders({ 'responseType': 'arraybuffer' }),
    observe: 'response'
  };
  httpImageFileOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/JSON', responseType: 'blob' as 'json', observe: 'response' as 'body' }),

  };
  httpOptionsDownload = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json', }),
    responseType: 'blob' as 'json',
    contentType: 'arraybuffer'
  };

  constructor(private http: HttpClient) { }

  PostApiReq(controller:any, method:any, data:any): Observable<any> {
    return this.http.post<any>(Global.API_ENDPOINT + controller + '/' + method, data, this.httpOptions);
  }
  GetApiReq(controller:any, method:any, data?:any): Observable<any> {
    let requestUrl: String = '';
    if (data || data == "") {
      requestUrl = "/" + data;
    }
    return this.http.get<any>(Global.API_ENDPOINT + controller + '/' + method + requestUrl);

  }  
}

