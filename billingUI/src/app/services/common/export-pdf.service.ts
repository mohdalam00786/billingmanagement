import { Injectable } from '@angular/core';
import { GenericConstants } from 'src/app/constants/generic-constants';
import jsPDF from "jspdf";
import "jspdf-autotable";

@Injectable({
  providedIn: 'root'
})
export class ExportPDFService {

  constructor() { }
  GeneratePDF(columns: any, value: any, fileName: any) {
    const doc:any = new jsPDF('p', 'pt');
    doc['autoTable'](columns, value);
    doc.save(fileName + GenericConstants.PdfExtenstion);    
  }
}
