import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr'


@Injectable({
  providedIn: 'root'
})
export class ToasterService {

  constructor( private toastr: ToastrService) { }

  SuccessToaster(message:string,title:string){
    this.toastr.success(message, title);
  }  

  ErrorToaster(message:string,title:string)
  {
    this.toastr.error(message,title);
  }
   
  ShowToasterInfo(message:string,title:string)
  {
    this.toastr.info(message,title)
  }
   
  showToasterWarning(){
    this.toastr.warning("This is warning", "This is warning")
  }
}

