import { Injectable } from '@angular/core';
import { GenericConstants } from 'src/app/constants/generic-constants';
import * as ExcelJS from 'exceljs/dist/exceljs.min.js'
import * as saveAs from 'file-saver';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable({
  providedIn: 'root'
})
export class ExportExcelService {
  constructor(private ngxSpinnerService: NgxSpinnerService) { }

  async GenerateExcel(data: any, headerData: any, fileName: any) {
      this.ngxSpinnerService.show();
      let workbook = new ExcelJS.Workbook();
      let worksheet = workbook.addWorksheet('Sheet1');
      let headerRow = worksheet.addRow(headerData);
      headerRow.eachCell((cell: any, number: any) => {
          cell.fill = {
              type: 'pattern',
              pattern: 'solid',
              fgColor: { argb: 'e8b80d' },
              bgColor: { argb: '' }
          };
          cell.font = {
              color: {
                  argb: '000000',
              },
              bold: true,
              size: 12
          }
          cell.border = {
              top: {
                  style: 'thin'
              },
              left: {
                  style: 'thin'
              },
              bottom: {
                  style: 'thin'
              },
              right: {
                  style: 'thin'
              }
          };
      });
      data.forEach((d: any) => {
          let row = worksheet.addRow(d);
          row.fill = {
              type: 'pattern',
              pattern: 'solid',
              fgColor: {
                  argb: 'FFFFFFFF'
              }
          };
          row.font = {
              color: {
                  argb: '00000000',
              },
              bold: false
          }
          row.eachCell({ includeEmpty: true }, (cell: any, number: any) => {
              cell.border = {
                  top: {
                      style: 'thin'
                  },
                  left: {
                      style: 'thin'
                  },
                  bottom: {
                      style: 'thin'
                  },
                  right: {
                      style: 'thin'
                  }
              };
          });
      });

      worksheet.views = [{ showGridLines: false }];
      workbook.xlsx.writeBuffer().then((excelData: any) => {
          let blob = new Blob([excelData], {
              type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
          });
          saveAs(blob, fileName + GenericConstants.ExcelExtension);
          this.ngxSpinnerService.hide();
      });
  }
}
