import { Injectable } from '@angular/core';
import { EncryptionService } from './encryption.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private encryptionService: EncryptionService) { }

SetCurrentUser(key:any, value:any) {
    sessionStorage.setItem(key, this.encryptionService.encryptValue(value));
}

GetCurrentUser(key:any) {
    const userDetails = sessionStorage.getItem(key)
    if (userDetails) {
        const decryptedData = this.encryptionService.decryptValue(userDetails);
        if (decryptedData) {
            return decryptedData;
        }
        return null;
    }
}

GetUserName(key:any) {
    const userDetails = JSON.parse( this.encryptionService.decryptValue(sessionStorage.getItem(key)));
    if (userDetails) {
        return userDetails.userName;
    }

}
GetUserId(key:any) {
    const userDetails = JSON.parse(this.encryptionService.decryptValue(sessionStorage.getItem(key)));
    if (userDetails) {
        return userDetails.userId;
    }
}
RemoveCurrentUser(key:any) {
    sessionStorage.removeItem(key);
}
ClearCurrentUser() {
    sessionStorage.clear();
}

GetToken(key:any) {
    const userDetails = this.encryptionService.decryptValue(sessionStorage.getItem(key));
    if (userDetails) {
        return userDetails.jsonWebToken;
    }
    return null;

}
}

