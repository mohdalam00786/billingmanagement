import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { UserService } from './user.service';
import { EncryptionService } from './encryption.service';
import { Global } from 'src/app/constants/global-config';
import { APIActions, ApiControllers } from 'src/app/constants/api-configuration';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private userSubject: BehaviorSubject<any>;

  constructor(
    private httpService: HttpService,
    private encryptionService: EncryptionService,
    private userService: UserService
  ) {
    if (this.userService.GetCurrentUser(Global.UserDetails)) {
      this.userSubject = new BehaviorSubject<any>(this.userService.GetCurrentUser(Global.UserDetails));

    } else {
      this.userSubject = new BehaviorSubject<any>(null);
    }
  }

  Login(loginModel:any) {

    return this.httpService.PostApiReq(ApiControllers.Authentication, APIActions.Login, this.encryptionService.EncryptObject(loginModel));
  }
  GetAllCentreForLogin() {
    return this.httpService.GetApiReq(ApiControllers.Authentication, APIActions.GetAllCentreForLogin);
  }
  public SendUserDetails(userDetails: any): void {
    this.userSubject.next(userDetails);
  }
  public GetUserDetails(): Observable<any> {
    return this.userSubject.asObservable();
  }
  

}

