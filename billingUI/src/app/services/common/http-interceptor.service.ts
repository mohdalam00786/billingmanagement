import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { tap, finalize } from 'rxjs/operators';
import { LoaderService } from './loader.service';
import { UserService } from './user.service';
import { Global } from 'src/app/constants/global-config';

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {

  constructor(private userService: UserService,
    public loaderService: LoaderService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = this.userService.GetCurrentUser(Global.UserDetails);
    this.loaderService.show();
    if (token) {
      req = req.clone({
        setHeaders: {
          Authorization: `Bearer ${token.jsonWebToken}`
        }
      });
    }
    return next.handle(req).pipe(
      tap(
        event => {

        },
        error => {
          throwError(error)
        }
      ),
      finalize(() => this.loaderService.hide())
    )
  }
}
