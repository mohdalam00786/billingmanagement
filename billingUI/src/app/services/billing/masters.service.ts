import { Injectable } from '@angular/core';
import { HttpService } from '../common/http.service';
import { EncryptionService } from '../common/encryption.service';
import { APIActions, ApiControllers } from 'src/app/constants/api-configuration';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MastersService {

  constructor(private httpService: HttpService,
    private encryptionService: EncryptionService) { }

  //Centre master
  GetAllCentres(): Observable<any> {
    return this.httpService.GetApiReq(ApiControllers.Master, APIActions.GetAllCentres);
  }

  SaveCentreInformation(centreInformation:any): Observable<any> {
    return this.httpService.PostApiReq(ApiControllers.Master, APIActions.SaveCentreInformation,
      this.encryptionService.EncryptObject(centreInformation));
  }
  DeleteCentreById(centreDetails:any): Observable<any> {
    return this.httpService.PostApiReq(ApiControllers.Master, APIActions.DeleteCentreById, this.encryptionService.EncryptObject(centreDetails));
  }
  // this.httpService.GetApiReq(ApiControllers.Master, APIActions.DeleteCentreById + "/" + centreId);
  //Role master
  GetAllRoles(): Observable<any> {
    return this.httpService.GetApiReq(ApiControllers.Master, APIActions.GetAllRoles);
  }

  SaveRoleInformation(roleInformation:any): Observable<any> {
    return this.httpService.PostApiReq(ApiControllers.Master, APIActions.SaveRoleInformation,
      this.encryptionService.EncryptObject(roleInformation));
  }
  DeleteRoleById(roleDetails:any): Observable<any> {
    return this.httpService.PostApiReq(ApiControllers.Master, APIActions.DeleteRoleById, this.encryptionService.EncryptObject(roleDetails));    
  }

  //Role master
  GetAllConfig(): Observable<any> {
    return this.httpService.GetApiReq(ApiControllers.Master, APIActions.GetAllConfig);
  }

  SaveConfigInformation(configInformation:any): Observable<any> {
    return this.httpService.PostApiReq(ApiControllers.Master, APIActions.SaveConfigInformation,
      this.encryptionService.EncryptObject(configInformation));
  }
  DeleteConfigById(configDetails:any): Observable<any> {
    return this.httpService.PostApiReq(ApiControllers.Master, APIActions.DeleteConfigById, this.encryptionService.EncryptObject(configDetails));    
  }

}


