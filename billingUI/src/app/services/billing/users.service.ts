import { Injectable } from '@angular/core';
import { HttpService } from '../common/http.service';
import { EncryptionService } from '../common/encryption.service';
import { Observable } from 'rxjs';
import { APIActions, ApiControllers } from 'src/app/constants/api-configuration';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private httpService: HttpService,
    private encryptionService: EncryptionService) { }

  GetAllUsers(): Observable<any> {
    return this.httpService.GetApiReq(ApiControllers.Users, APIActions.GetAllUsers);
  }
  GetUserMasterData(): Observable<any> {
    return this.httpService.GetApiReq(ApiControllers.Users, APIActions.GetUserMasterData);
  }
  
  SaveUserInformation(userPersonalInfo:any): Observable<any> {
    return this.httpService.PostApiReq(ApiControllers.Users, APIActions.SaveUserInformation,
      this.encryptionService.EncryptObject(userPersonalInfo));
  } 
  

  DeleteUserById(userDetails:any): Observable<any> {
    return this.httpService.PostApiReq(ApiControllers.Users, APIActions.DeleteUserById, this.encryptionService.EncryptObject(userDetails));
  }
}

