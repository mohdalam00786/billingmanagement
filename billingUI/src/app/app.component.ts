import { Component } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Global } from './constants/global-config';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'billingUI';


  constructor(
    //public translate: TranslateService
    ) {
    // translate.addLangs(['en', 'ar']);
    // translate.setDefaultLang('en');
  }
  async ngOnInit() {
    const appConfig = environment.ApiEndpoint;
    Global.Init(appConfig);
  }
}


