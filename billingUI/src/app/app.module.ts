import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ToastrModule } from 'ngx-toastr';
import { HeaderComponent } from './components/common/header/header.component';
import { LoaderComponent } from './components/common/loader/loader.component';
import { LoginComponent } from './components/common/login/login.component';
import { PrimeNgMessagesComponent } from './components/common/prime-ng-messages/prime-ng-messages.component';
import { CentreMasterComponent } from './components/billing/centre-master/centre-master.component';
import { ConfigMasterComponent } from './components/billing/config-master/config-master.component';
import { RoleMasterComponent } from './components/billing/role-master/role-master.component';
import { UserMasterComponent } from './components/billing/user-master/user-master.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoaderComponent,
    LoginComponent,
    PrimeNgMessagesComponent,
    CentreMasterComponent,
    ConfigMasterComponent,
    RoleMasterComponent,
    UserMasterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    NgxSpinnerModule.forRoot({ type: 'ball-scale-multiple' })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
