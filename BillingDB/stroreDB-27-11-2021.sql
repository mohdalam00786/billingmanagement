/*
SQLyog Community v13.1.7 (64 bit)
MySQL - 8.0.21 : Database - storemanagement
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`storemanagement` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `storemanagement`;

/*Table structure for table `tblcentremaster` */

DROP TABLE IF EXISTS `tblcentremaster`;

CREATE TABLE `tblcentremaster` (
  `CentreId` bigint NOT NULL AUTO_INCREMENT,
  `CentreName` varchar(100) NOT NULL,
  `CentreCode` varchar(10) DEFAULT NULL,
  `Website` varchar(100) DEFAULT NULL,
  `Address` varchar(100) DEFAULT NULL,
  `MobileNumber` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `LandlineNumber` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `EmailId` varchar(100) DEFAULT NULL,
  `CreatedBy` bigint NOT NULL,
  `CreatedDateTime` datetime NOT NULL,
  `UpdatedBy` bigint DEFAULT NULL,
  `UpdatedDateTime` datetime DEFAULT NULL,
  `DeletedBy` bigint DEFAULT NULL,
  `DeletedDateTime` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) DEFAULT NULL,
  `IPAddress` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`CentreId`),
  KEY `UpdatedBy` (`UpdatedBy`),
  KEY `DeletedBy` (`DeletedBy`),
  CONSTRAINT `tblcentremaster_ibfk_1` FOREIGN KEY (`UpdatedBy`) REFERENCES `tblusers` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tblcentremaster_ibfk_2` FOREIGN KEY (`DeletedBy`) REFERENCES `tblusers` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `tblcentremaster` */

insert  into `tblcentremaster`(`CentreId`,`CentreName`,`CentreCode`,`Website`,`Address`,`MobileNumber`,`LandlineNumber`,`EmailId`,`CreatedBy`,`CreatedDateTime`,`UpdatedBy`,`UpdatedDateTime`,`DeletedBy`,`DeletedDateTime`,`IsDeleted`,`IPAddress`) values 
(1,'Indiana Pvt Ltd','IND001','www.indiana.com',NULL,'9999999999',NULL,'indiana@gmail.com',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL),
(2,'centre 2','002','centre.com','centre addresss new ','9090909090','1234567','centre@gmail.com',4,'2021-11-16 21:30:25',4,'2021-11-20 16:49:55',NULL,NULL,0,'192.168.45.28'),
(3,'centre 3','003','nncjdn','new cjfjnjn jdncjdncj','ndcjdnjcndj','scnn','dcndn',4,'2021-11-17 16:40:50',NULL,NULL,NULL,NULL,0,'192.168.38.28'),
(4,'centre 4','004','dcdcd','bjdjbcu jcd bjd','bjbjbjb','jdbjdj','cdcdjbb',4,'2021-11-17 16:41:24',NULL,NULL,NULL,NULL,0,'192.168.38.28'),
(5,'centre 4','004','dcdcd','bjdjbcu jcd bjd','bjbjbjb','jdbjdj','cdcdjbb',4,'2021-11-17 16:41:34',NULL,NULL,4,'2021-11-18 22:56:02',1,'192.168.38.28'),
(6,'centre 5','004','dcdcd','bjdjbcu jcd bjd','bjbjbjb','jdbjdj','cdcdjbb',4,'2021-11-17 16:41:54',NULL,NULL,4,'2021-11-18 18:58:05',1,'192.168.38.28'),
(7,'centre 5','004','dcdcd','bjdjbcu jcd bjd','bjbjbjb','jdbjdj','cdcdjbb',4,'2021-11-17 16:44:18',NULL,NULL,4,'2021-11-17 22:23:04',1,'192.168.38.28'),
(8,'kdkdkh','sckdhk','sdjdj.com','vndknvkdn','djvbjdbvjd','nckdnfkdn','scknk@gmail.com',4,'2021-11-17 17:23:01',NULL,NULL,NULL,NULL,1,'192.168.38.28'),
(9,'centre 5','005','web.com','dfdd dnfdn dvjd djd djd dfdj ','3555353353','dgdgdgdgd','gmail.com',4,'2021-11-20 00:09:43',NULL,NULL,NULL,NULL,0,'192.168.45.28'),
(10,'centre 10','10','nckn','cnkn','kncknd','knkn','nckdnk',4,'2021-11-20 00:10:24',NULL,NULL,NULL,NULL,0,'192.168.45.28'),
(11,'centr11','knvkdn','nvdnv','vldnvldnvldnlv','vndnvkdn','dlvdlnv','dvndnvk',4,'2021-11-20 00:10:46',NULL,NULL,NULL,NULL,0,'192.168.45.28'),
(12,'centre11','212','sfdf.com','sknfknkf','9999999999','(91)123456','mcmskm$.com',4,'2021-11-20 16:53:21',4,'2021-11-20 16:57:24',NULL,NULL,0,'192.168.45.28'),
(13,'dddddw','null','null','null','null','null','dddd123@gmail.co',4,'2021-11-20 17:32:39',NULL,NULL,NULL,NULL,0,'192.168.45.28');

/*Table structure for table `tblconfigmaster` */

DROP TABLE IF EXISTS `tblconfigmaster`;

CREATE TABLE `tblconfigmaster` (
  `ConfigId` bigint NOT NULL AUTO_INCREMENT,
  `ConfigKey` varchar(100) NOT NULL,
  `ConfigValue` varchar(100) NOT NULL,
  `Description` varchar(100) NOT NULL,
  KEY `ConfigId` (`ConfigId`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `tblconfigmaster` */

insert  into `tblconfigmaster`(`ConfigId`,`ConfigKey`,`ConfigValue`,`Description`) values 
(1,'Gender','Male','Male'),
(2,'Gender','Female','Female'),
(3,'Gender','Other','Other'),
(4,'BloodGroup','A Positive','A+'),
(5,'BloodGroup','A Negative','A-'),
(6,'BloodGroup','B Positive','B+'),
(7,'BloodGroup','B Negative','B-'),
(8,'BloodGroup','AB Positive','AB+'),
(9,'BloodGroup','AB Negative','AB-'),
(10,'BloodGroup','O Positive','O+'),
(11,'BloodGroup','O Negative','O-'),
(12,'MaritalStatus','Single','Single'),
(13,'MaritalStatus','Married','Married'),
(14,'MaritalStatus','Common in Law','CommonLaw'),
(15,'MaritalStatus','Separated','Separated'),
(16,'MaritalStatus','Widow','Widow'),
(17,'MaritalStatus','Divorced','Divorced');

/*Table structure for table `tbllogin` */

DROP TABLE IF EXISTS `tbllogin`;

CREATE TABLE `tbllogin` (
  `LoginId` bigint NOT NULL AUTO_INCREMENT,
  `UserId` bigint NOT NULL,
  `Password` varchar(300) NOT NULL,
  `TotalAttemptCount` tinyint DEFAULT NULL,
  `EmailId` varchar(100) NOT NULL,
  `LoginName` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`LoginId`,`UserId`),
  KEY `UserId` (`UserId`),
  CONSTRAINT `tbllogin_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `tblusers` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `tbllogin` */

insert  into `tbllogin`(`LoginId`,`UserId`,`Password`,`TotalAttemptCount`,`EmailId`,`LoginName`) values 
(1,18,'alam@1234567',0,'',''),
(2,19,'alam@123',0,'mohd.alam@gmail.com','Imran Ahmad'),
(3,35,'swt4VwA6PbFmkeEn+SsaFwlYMXQZjGNGtA==',0,'friendlyalam@gmail.com','mohdalam');

/*Table structure for table `tblrolemaster` */

DROP TABLE IF EXISTS `tblrolemaster`;

CREATE TABLE `tblrolemaster` (
  `RoleId` bigint NOT NULL AUTO_INCREMENT,
  `RoleName` varchar(100) DEFAULT NULL,
  `CreatedBy` bigint NOT NULL,
  `CreatedDateTime` datetime NOT NULL,
  `UpdatedBy` bigint DEFAULT NULL,
  `UpdatedDateTime` datetime DEFAULT NULL,
  `DeletedBy` bigint DEFAULT NULL,
  `DeletedDateTime` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) DEFAULT NULL,
  `IPAddress` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`RoleId`),
  KEY `tblrolemaster_ibfk_1` (`CreatedBy`),
  KEY `tblrolemaster_ibfk_2` (`UpdatedBy`),
  KEY `tblrolemaster_ibfk_3` (`DeletedBy`),
  CONSTRAINT `tblrolemaster_ibfk_1` FOREIGN KEY (`CreatedBy`) REFERENCES `tblusers` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tblrolemaster_ibfk_2` FOREIGN KEY (`UpdatedBy`) REFERENCES `tblusers` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tblrolemaster_ibfk_3` FOREIGN KEY (`DeletedBy`) REFERENCES `tblusers` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `tblrolemaster` */

insert  into `tblrolemaster`(`RoleId`,`RoleName`,`CreatedBy`,`CreatedDateTime`,`UpdatedBy`,`UpdatedDateTime`,`DeletedBy`,`DeletedDateTime`,`IsDeleted`,`IPAddress`) values 
(3,'Administrator',4,'2021-11-24 22:43:50',NULL,NULL,NULL,NULL,0,NULL),
(4,'Billing',4,'2021-11-24 22:44:29',NULL,NULL,NULL,NULL,0,NULL),
(5,'Store',4,'2021-11-24 22:44:44',NULL,NULL,NULL,NULL,0,NULL);

/*Table structure for table `tblusers` */

DROP TABLE IF EXISTS `tblusers`;

CREATE TABLE `tblusers` (
  `UserId` bigint NOT NULL AUTO_INCREMENT,
  `UserName` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `FatherName` varchar(100) DEFAULT NULL,
  `MotherName` varchar(100) DEFAULT NULL,
  `SpouseName` varchar(100) DEFAULT NULL,
  `DateOfBirth` date DEFAULT NULL,
  `Gender` bigint DEFAULT NULL,
  `BloodGroup` bigint DEFAULT NULL,
  `Roles` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `MaritalStatus` bigint DEFAULT NULL,
  `Password` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `PrimaryContactNumber` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `SecondryContactNumber` varchar(20) DEFAULT NULL,
  `EmergencyContactName` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `EmergencyContactNumber` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `HighestQualification` varchar(30) DEFAULT NULL,
  `EmailId` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `PassportNumber` varchar(20) DEFAULT NULL,
  `PANCardNumber` varchar(20) DEFAULT NULL,
  `AdhaarCardNumber` varchar(20) DEFAULT NULL,
  `MailingHouseNumber` varchar(20) DEFAULT NULL,
  `MailingStreetName` varchar(50) DEFAULT NULL,
  `MailingLocality` varchar(50) DEFAULT NULL,
  `MailingCity` varchar(50) DEFAULT NULL,
  `MailingState` int DEFAULT NULL,
  `MailingCountry` int DEFAULT NULL,
  `MailingPincode` varchar(10) DEFAULT NULL,
  `PermanentHouseNumber` varchar(20) DEFAULT NULL,
  `PermanentStreetName` varchar(50) DEFAULT NULL,
  `PermanentLocality` varchar(50) DEFAULT NULL,
  `PermanentCity` varchar(50) DEFAULT NULL,
  `PermanentState` int DEFAULT NULL,
  `PermanentCountry` int DEFAULT NULL,
  `PermanentPincode` varchar(10) DEFAULT NULL,
  `CreatedBy` bigint NOT NULL,
  `CreatedDateTime` datetime NOT NULL,
  `UpdatedBy` bigint DEFAULT NULL,
  `UpdatedDateTime` datetime DEFAULT NULL,
  `DeletedBy` bigint DEFAULT NULL,
  `DeletedDateTime` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) DEFAULT NULL,
  `IPAddress` varchar(100) DEFAULT NULL,
  `Centres` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `CorrespondenceAddress` varchar(500) DEFAULT NULL,
  `PermanentAddress` varchar(500) DEFAULT NULL,
  `LandlineNumber` varchar(20) DEFAULT NULL,
  `GuardianName` varchar(100) DEFAULT NULL,
  `LoginName` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`UserId`),
  KEY `CreatedBy` (`CreatedBy`),
  KEY `Gender` (`Gender`),
  KEY `BloodGroup` (`BloodGroup`),
  KEY `MaritalStatus` (`MaritalStatus`),
  KEY `tblusers_ibfk_2` (`UpdatedBy`),
  KEY `tblusers_ibfk_3` (`DeletedBy`),
  KEY `Designation` (`Roles`),
  KEY `CentreId` (`Centres`),
  CONSTRAINT `tblusers_ibfk_2` FOREIGN KEY (`UpdatedBy`) REFERENCES `tblusers` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tblusers_ibfk_3` FOREIGN KEY (`DeletedBy`) REFERENCES `tblusers` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tblusers_ibfk_4` FOREIGN KEY (`Gender`) REFERENCES `tblconfigmaster` (`ConfigId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tblusers_ibfk_5` FOREIGN KEY (`BloodGroup`) REFERENCES `tblconfigmaster` (`ConfigId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tblusers_ibfk_7` FOREIGN KEY (`MaritalStatus`) REFERENCES `tblconfigmaster` (`ConfigId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `tblusers` */

insert  into `tblusers`(`UserId`,`UserName`,`FatherName`,`MotherName`,`SpouseName`,`DateOfBirth`,`Gender`,`BloodGroup`,`Roles`,`MaritalStatus`,`Password`,`PrimaryContactNumber`,`SecondryContactNumber`,`EmergencyContactName`,`EmergencyContactNumber`,`HighestQualification`,`EmailId`,`PassportNumber`,`PANCardNumber`,`AdhaarCardNumber`,`MailingHouseNumber`,`MailingStreetName`,`MailingLocality`,`MailingCity`,`MailingState`,`MailingCountry`,`MailingPincode`,`PermanentHouseNumber`,`PermanentStreetName`,`PermanentLocality`,`PermanentCity`,`PermanentState`,`PermanentCountry`,`PermanentPincode`,`CreatedBy`,`CreatedDateTime`,`UpdatedBy`,`UpdatedDateTime`,`DeletedBy`,`DeletedDateTime`,`IsDeleted`,`IPAddress`,`Centres`,`CorrespondenceAddress`,`PermanentAddress`,`LandlineNumber`,`GuardianName`,`LoginName`) values 
(4,'mohd alam','rakiv ali','sanjeeda khatoon','maahi','2018-03-29',1,1,'1',1,'abcd@123','99997977434',NULL,'alam','9876543210','cs','abcd@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2018-03-29 13:34:00',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,''),
(5,'Mohd Alam','Rakiv Ali','Sanjeeda Khatoon','Maahi','2021-11-01',1,1,'1',1,'alam@123','4344434434',NULL,'alam','343534534','b.tech','mohd.alam@gmail.com',NULL,NULL,NULL,NULL,NULL,'0','0',0,0,'0',NULL,NULL,'0','0',0,0,'0',4,'2021-11-01 16:50:47',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,''),
(6,'Mohd Alam','Rakiv Ali','Sanjeeda Khatoon','Maahi','2021-11-01',1,1,'1',1,'alam@123','4344434434',NULL,'alam','343534534','b.tech','mohd.alam@gmail.com',NULL,NULL,NULL,NULL,NULL,'0','0',0,0,'0',NULL,NULL,'0','0',0,0,'0',4,'2021-11-01 17:19:12',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,''),
(7,'Mohd Alam','Rakiv Ali','Sanjeeda Khatoon','Maahi','2021-11-01',1,1,'1',1,'alam@123','4344434434',NULL,'alam','343534534','b.tech','mohd.alam@gmail.com',NULL,NULL,NULL,NULL,NULL,'0','0',0,0,'0',NULL,NULL,'0','0',0,0,'0',4,'2021-11-01 17:22:38',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,''),
(8,'Mohd Alam','Rakiv Ali','Sanjeeda Khatoon','Maahi','2021-11-01',1,1,'1',1,'alam@123','4344434434',NULL,'alam','343534534','b.tech','mohd.alam@gmail.com',NULL,NULL,NULL,NULL,NULL,'0','0',0,0,'0',NULL,NULL,'0','0',0,0,'0',4,'2021-11-01 17:25:02',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,''),
(9,'Mohd Alam','Rakiv Ali','Sanjeeda Khatoon','Maahi','2021-11-01',1,1,'1',1,'alam@123','4344434434',NULL,'alam','343534534','b.tech','mohd.alam@gmail.com',NULL,NULL,NULL,NULL,NULL,'0','0',0,0,'0',NULL,NULL,'0','0',0,0,'0',4,'2021-11-01 17:25:54',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,''),
(10,'Mohd Alam','Rakiv Ali','Sanjeeda Khatoon','Maahi','2018-03-29',1,2,'1',2,'alam@1234567','4344434434',NULL,'alam','343534534','b.tech','mohd.alam@gmail.com',NULL,NULL,NULL,NULL,NULL,'0','0',0,0,'0',NULL,NULL,'0','0',0,0,'0',4,'2021-11-01 17:27:14',10,'2021-11-03 17:42:54',NULL,NULL,0,'192.168.37.28',NULL,NULL,NULL,NULL,NULL,''),
(11,'Mohd Alam','Rakiv Ali','Sanjeeda Khatoon','Maahi','2021-11-01',1,1,'1',1,'alam@123','4344434434',NULL,'alam','343534534','b.tech','mohd.alam@gmail.com',NULL,NULL,NULL,NULL,NULL,'0','0',0,0,'0',NULL,NULL,'0','0',0,0,'0',4,'2021-11-01 17:33:21',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,''),
(12,'Mohd Alam',NULL,'Sanjeeda Khatoon','Maahi','2018-03-29',1,1,'1',1,'alam@123','4344434434',NULL,'alam','343534534','b.tech','mohd.alam@gmail.com',NULL,NULL,NULL,NULL,NULL,'0','0',0,0,'0',NULL,NULL,'0','0',0,0,'0',4,'2021-11-01 18:35:27',NULL,NULL,NULL,NULL,0,'192.168.37.28',NULL,NULL,NULL,NULL,NULL,''),
(13,'Mohd Alam',NULL,'Sanjeeda Khatoon','Maahi','2018-03-29',1,1,'1',1,'alam@123','4344434434',NULL,'alam','343534534','b.tech','mohd.alam@gmail.com',NULL,NULL,NULL,NULL,NULL,'0','0',0,0,'0',NULL,NULL,'0','0',0,0,'0',4,'2021-11-01 18:50:00',NULL,NULL,4,'2021-11-21 00:18:05',1,'192.168.37.28',NULL,NULL,NULL,NULL,NULL,''),
(14,'Mohd Alam',NULL,'Sanjeeda Khatoon','Maahi','2018-03-29',1,1,'1',1,'alam@12345','4344434434',NULL,'alam','343534534','b.tech','mohd.alam@gmail.com',NULL,NULL,NULL,NULL,NULL,'0','0',0,0,'0',NULL,NULL,'0','0',0,0,'0',4,'2021-11-01 19:08:13',NULL,NULL,NULL,NULL,0,'192.168.37.28',NULL,NULL,NULL,NULL,NULL,''),
(15,'Mohd Alam','Rakiv Ali','Sanjeeda Khatoon','Maahi','2018-03-29',1,1,'1',1,'alam@12345','4344434434',NULL,'alam','343534534','b.tech','mohd.alam@gmail.com',NULL,NULL,NULL,NULL,NULL,'0','0',0,0,'0',NULL,NULL,'0','0',0,0,'0',4,'2021-11-01 19:17:36',NULL,NULL,NULL,NULL,0,'192.168.37.28',NULL,NULL,NULL,NULL,NULL,''),
(16,'Mohd Alam','Rakiv Ali','Sanjeeda Khatoon','Maahi','2018-03-29',1,1,'1',1,'alam@12345','4344434434',NULL,'alam','343534534','b.tech','mohd.alam@gmail.com',NULL,NULL,NULL,NULL,NULL,'0','0',0,0,'0',NULL,NULL,'0','0',0,0,'0',4,'2021-11-01 19:23:36',NULL,NULL,NULL,NULL,0,'192.168.37.28',NULL,NULL,NULL,NULL,NULL,''),
(17,'Mohd Alam','Rakiv Ali','Sanjeeda Khatoon','Maahi','2018-03-29',1,2,'1',2,'alam@1234567','4344434434',NULL,'alam','343534534','b.tech','mohd.alam@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,0,0,NULL,4,'2021-11-03 18:10:21',NULL,NULL,NULL,NULL,0,'192.168.37.28',NULL,NULL,NULL,NULL,NULL,''),
(18,'Mohd Alam','Rakiv Ali','Sanjeeda Khatoon','Maahi','2018-03-29',1,2,'1',2,'alam@1234567','4344434434',NULL,'alam','343534534','b.tech','mohd.alam@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,0,0,NULL,4,'2021-11-03 18:16:43',NULL,NULL,NULL,NULL,0,'192.168.37.28',NULL,NULL,NULL,NULL,NULL,''),
(19,'Imran Ahmad','Rakiv Ali','Sanjeeda Khatoon','Maahi','2018-03-29',1,2,'1',2,'alam@123','4344434434',NULL,'alam','343534534','b.tech','mohd.alam@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,0,0,NULL,4,'2021-11-03 18:53:35',NULL,NULL,NULL,NULL,0,'192.168.37.28',NULL,NULL,NULL,NULL,NULL,''),
(21,'developer alam','father','mother','spouse','1990-12-12',1,7,'0',13,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'8686868868','966999','868686868',NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,0,0,NULL,4,'2021-11-21 18:59:18',NULL,NULL,NULL,NULL,0,'127.0.0.1',NULL,NULL,NULL,NULL,'guardian',NULL),
(22,'developer alam','father','mother','spouse','1990-12-12',1,7,'0',13,NULL,'868886868','6969696','gkgkgg','70707070',NULL,'66fjfjf@gmail.com','8686868868','966999','868686868',NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,0,0,NULL,4,'2021-11-21 19:02:29',10,'2021-11-24 17:02:17',NULL,NULL,0,'127.0.0.1',NULL,'khkkhkh','hkkg gg bkgg kgkg bkk','969696669','guardian',NULL),
(24,'usernamw','gbgkgk','kgkgkgk','668688','2000-10-10',1,4,'0',12,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'kgkgkgk','gkgkgk','kgkgkgk',NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,0,0,NULL,4,'2021-11-21 20:41:49',NULL,NULL,NULL,NULL,0,'127.0.0.1',NULL,NULL,NULL,NULL,'kgkgkgk',NULL),
(26,'mohd alam','nkdndk','nkdd','kcbkbcd','2021-12-12',2,4,'0',12,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'858585','858585','85888',NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,0,0,NULL,4,'2021-11-24 17:16:44',NULL,NULL,NULL,NULL,0,'127.0.0.1',NULL,NULL,NULL,NULL,'dkkdbvbk',NULL),
(27,'mohd alam','nkdndk','nkdd','kcbkbcd','2021-12-12',2,4,'0',12,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'858585','858585','85888',NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,0,0,NULL,4,'2021-11-24 17:46:52',NULL,NULL,NULL,NULL,0,'127.0.0.1',NULL,NULL,NULL,NULL,'dkkdbvbk',NULL),
(28,'indiana','fathername','mothername','spousename','2021-10-22',1,4,'0',12,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'979797979','969696','9696969',NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,0,0,NULL,4,'2021-11-24 18:05:55',NULL,NULL,NULL,NULL,0,'127.0.0.1',NULL,NULL,NULL,NULL,'gaurdianan',NULL),
(30,'kkgkgkgk','kkgk','kgkgk','kkk','1991-02-22',1,5,'0',13,NULL,'6696996','969696969','lhlhlhh','6699996',NULL,'lhlhlh@gmail.com','96969696','96969','699696',NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,0,0,NULL,4,'2021-11-24 18:18:07',10,'2021-11-24 18:18:52',NULL,NULL,0,'127.0.0.1',NULL,'kvddkv dkbkdb mbkdb','vndvld vbkdvkd dvbdbkv dbkdbv ','969696969','kgkk',NULL),
(31,'nllhlh','hhl','lhlh','hhh','2020-10-22',1,5,'0',13,'kfndndk','nvf','lll','dvldvh','lldvlhd',NULL,'vdlvlh','llhlh','lhlhlh','lhlhl',NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,0,0,NULL,4,'2021-11-24 18:39:51',10,'2021-11-24 18:41:02',NULL,NULL,0,'127.0.0.1',NULL,'lhlhlhlhl','lhlhlh','dvdv','hlll','alam'),
(32,'nnhlhll','lhlhlh','lhlhlh','khkkh','2021-02-12',1,5,'0',13,NULL,'669696','696969','kgkgkg','6969696',NULL,'kgkgk','lhlhlh','hllhlh','hllhlh',NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,0,0,NULL,4,'2021-11-24 23:30:32',10,'2021-11-24 23:31:49',NULL,NULL,0,'127.0.0.1',NULL,'gkkgkg','kgkgk','969696','lhlhlh',NULL),
(33,'mohd alam','rakiv ali','sanjeeda','nazma','1994-02-09',1,6,'0',13,'alam@123','9917441044','7827528646','rakiv ali','9917821771',NULL,'friendlyalam@gmail.com','null','ayzpa4878r','504639867219',NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,0,0,NULL,4,'2021-11-26 17:44:15',10,'2021-11-26 17:47:46',NULL,NULL,0,'127.0.0.1',NULL,'null','moradabad','null','null','mohdalam'),
(34,'mod','GKGKG','GKGK','GKGK','1999-02-09',1,5,'3,4,3,4',13,'JRgWhCx1+qJYO40rzStuRAE8V74ZseRSdQ==','KK','KGG','KKG','KGKG',NULL,'KKG','KGKGK','KGKGK','KGKGK',NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,0,0,NULL,4,'2021-11-26 19:10:52',10,'2021-11-26 19:12:27',NULL,NULL,0,'127.0.0.1','2,3,2,3','KGKG','KGK','KKG','GKGK','nkdknv'),
(35,'mohd alam','rakiv ali','sanjeeda','maahi','1994-02-09',1,10,'3,4',13,'swt4VwA6PbFmkeEn+SsaFwlYMXQZjGNGtA==','9917441044','7827528646','9917821771','9917821771',NULL,'friendlyalam@gmail.com','null','AYZPA4878R','504639867219',NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,0,0,NULL,4,'2021-11-26 19:27:21',10,'2021-11-26 19:34:45',NULL,NULL,0,'127.0.0.1','2,3','moradabad','moradabad','null','null','mohdalam');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
