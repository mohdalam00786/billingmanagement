/*
SQLyog Community v13.1.7 (64 bit)
MySQL - 8.0.21 : Database - storemanagement
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`storemanagement` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `storemanagement`;

/*Table structure for table `tblcentremaster` */

DROP TABLE IF EXISTS `tblcentremaster`;

CREATE TABLE `tblcentremaster` (
  `CentreId` bigint NOT NULL AUTO_INCREMENT,
  `CentreName` varchar(100) NOT NULL,
  `CentreCode` varchar(10) DEFAULT NULL,
  `Website` varchar(100) DEFAULT NULL,
  `Address` varchar(100) DEFAULT NULL,
  `MobileNumber` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `LandlineNumber` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `EmailId` varchar(100) DEFAULT NULL,
  `CreatedBy` bigint NOT NULL,
  `CreatedDateTime` datetime NOT NULL,
  `UpdatedBy` bigint DEFAULT NULL,
  `UpdatedDateTime` datetime DEFAULT NULL,
  `DeletedBy` bigint DEFAULT NULL,
  `DeletedDateTime` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) DEFAULT NULL,
  `IPAddress` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`CentreId`),
  KEY `UpdatedBy` (`UpdatedBy`),
  KEY `DeletedBy` (`DeletedBy`),
  CONSTRAINT `tblcentremaster_ibfk_1` FOREIGN KEY (`UpdatedBy`) REFERENCES `tblusers` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tblcentremaster_ibfk_2` FOREIGN KEY (`DeletedBy`) REFERENCES `tblusers` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `tblcentremaster` */

insert  into `tblcentremaster`(`CentreId`,`CentreName`,`CentreCode`,`Website`,`Address`,`MobileNumber`,`LandlineNumber`,`EmailId`,`CreatedBy`,`CreatedDateTime`,`UpdatedBy`,`UpdatedDateTime`,`DeletedBy`,`DeletedDateTime`,`IsDeleted`,`IPAddress`) values 
(1,'Indiana Pvt Ltd','IND001','www.indiana.com',NULL,'9999999999',NULL,'indiana@gmail.com',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL),
(2,'centre 2','002','centre.com','centre addresss new ','9090909090','1234567','centre@gmail.com',4,'2021-11-16 21:30:25',NULL,NULL,NULL,NULL,0,'192.168.38.28');

/*Table structure for table `tblconfigmaster` */

DROP TABLE IF EXISTS `tblconfigmaster`;

CREATE TABLE `tblconfigmaster` (
  `ConfigId` bigint NOT NULL AUTO_INCREMENT,
  `ConfigKey` varchar(100) NOT NULL,
  `ConfigValue` varchar(100) NOT NULL,
  `Description` varchar(100) NOT NULL,
  KEY `ConfigId` (`ConfigId`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `tblconfigmaster` */

insert  into `tblconfigmaster`(`ConfigId`,`ConfigKey`,`ConfigValue`,`Description`) values 
(1,'Gender','Male','Male'),
(2,'Gender','Female','Female'),
(3,'Gender','Other','Other'),
(4,'BloodGroup','A Positive','A+'),
(5,'BloodGroup','A Negative','A-'),
(6,'BloodGroup','B Positive','B+'),
(7,'BloodGroup','B Negative','B-'),
(8,'BloodGroup','AB Positive','AB+'),
(9,'BloodGroup','AB Negative','AB-'),
(10,'BloodGroup','O Positive','O+'),
(11,'BloodGroup','O Negative','O-'),
(12,'MaritalStatus','Single','Single'),
(13,'MaritalStatus','Married','Married'),
(14,'MaritalStatus','Common in Law','CommonLaw'),
(15,'MaritalStatus','Separated','Separated'),
(16,'MaritalStatus','Widow','Widow'),
(17,'MaritalStatus','Divorced','Divorced');

/*Table structure for table `tbllogin` */

DROP TABLE IF EXISTS `tbllogin`;

CREATE TABLE `tbllogin` (
  `LoginId` bigint NOT NULL AUTO_INCREMENT,
  `UserId` bigint NOT NULL,
  `Password` varchar(300) NOT NULL,
  `TotalAttemptCount` tinyint DEFAULT NULL,
  `EmailId` varchar(100) NOT NULL,
  `UserName` varchar(100) NOT NULL,
  PRIMARY KEY (`LoginId`,`UserId`),
  KEY `UserId` (`UserId`),
  CONSTRAINT `tbllogin_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `tblusers` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `tbllogin` */

insert  into `tbllogin`(`LoginId`,`UserId`,`Password`,`TotalAttemptCount`,`EmailId`,`UserName`) values 
(1,18,'alam@1234567',0,'',''),
(2,19,'alam@123',0,'mohd.alam@gmail.com','Imran Ahmad');

/*Table structure for table `tblusers` */

DROP TABLE IF EXISTS `tblusers`;

CREATE TABLE `tblusers` (
  `UserId` bigint NOT NULL AUTO_INCREMENT,
  `UserName` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `FatherName` varchar(100) DEFAULT NULL,
  `MotherName` varchar(100) DEFAULT NULL,
  `SpouseName` varchar(100) DEFAULT NULL,
  `DateOfBirth` date NOT NULL,
  `Gender` bigint NOT NULL,
  `BloodGroup` bigint DEFAULT NULL,
  `Designation` bigint NOT NULL,
  `MaritalStatus` bigint NOT NULL,
  `Password` varchar(300) NOT NULL,
  `ConfirmPassword` varchar(300) NOT NULL,
  `PrimaryContactNumber` varchar(20) NOT NULL,
  `SecondryContactNumber` varchar(20) DEFAULT NULL,
  `EmergencyContactName` varchar(100) NOT NULL,
  `EmergencyContactNumber` varchar(20) NOT NULL,
  `HighestQualification` varchar(30) DEFAULT NULL,
  `EmailId` varchar(100) NOT NULL,
  `PassportNumber` varchar(20) DEFAULT NULL,
  `PANCardNumber` varchar(20) DEFAULT NULL,
  `AdhaarCardNumber` varchar(20) DEFAULT NULL,
  `MailingHouseNumber` varchar(20) DEFAULT NULL,
  `MailingStreetName` varchar(50) DEFAULT NULL,
  `MailingLocality` varchar(50) DEFAULT NULL,
  `MailingCity` varchar(50) DEFAULT NULL,
  `MailingState` int DEFAULT NULL,
  `MailingCountry` int DEFAULT NULL,
  `MailingPincode` varchar(10) DEFAULT NULL,
  `PermanentHouseNumber` varchar(20) DEFAULT NULL,
  `PermanentStreetName` varchar(50) DEFAULT NULL,
  `PermanentLocality` varchar(50) DEFAULT NULL,
  `PermanentCity` varchar(50) DEFAULT NULL,
  `PermanentState` int DEFAULT NULL,
  `PermanentCountry` int DEFAULT NULL,
  `PermanentPincode` varchar(10) DEFAULT NULL,
  `CreatedBy` bigint NOT NULL,
  `CreatedDateTime` datetime NOT NULL,
  `UpdatedBy` bigint DEFAULT NULL,
  `UpdatedDateTime` datetime DEFAULT NULL,
  `DeletedBy` bigint DEFAULT NULL,
  `DeletedDateTime` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) DEFAULT NULL,
  `IPAddress` varchar(100) DEFAULT NULL,
  `CentreId` bigint DEFAULT NULL,
  `CorrespondenceAddress` varchar(500) DEFAULT NULL,
  `PermanentAddress` varchar(500) DEFAULT NULL,
  `LandlineNumber` varchar(20) DEFAULT NULL,
  `GuardianName` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`UserId`),
  KEY `CreatedBy` (`CreatedBy`),
  KEY `Gender` (`Gender`),
  KEY `BloodGroup` (`BloodGroup`),
  KEY `Designation` (`Designation`),
  KEY `MaritalStatus` (`MaritalStatus`),
  KEY `tblusers_ibfk_2` (`UpdatedBy`),
  KEY `tblusers_ibfk_3` (`DeletedBy`),
  KEY `CentreId` (`CentreId`),
  CONSTRAINT `tblusers_ibfk_2` FOREIGN KEY (`UpdatedBy`) REFERENCES `tblusers` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tblusers_ibfk_3` FOREIGN KEY (`DeletedBy`) REFERENCES `tblusers` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tblusers_ibfk_4` FOREIGN KEY (`Gender`) REFERENCES `tblconfigmaster` (`ConfigId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tblusers_ibfk_5` FOREIGN KEY (`BloodGroup`) REFERENCES `tblconfigmaster` (`ConfigId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tblusers_ibfk_6` FOREIGN KEY (`Designation`) REFERENCES `tblconfigmaster` (`ConfigId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tblusers_ibfk_7` FOREIGN KEY (`MaritalStatus`) REFERENCES `tblconfigmaster` (`ConfigId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tblusers_ibfk_8` FOREIGN KEY (`CentreId`) REFERENCES `tblcentremaster` (`CentreId`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `tblusers` */

insert  into `tblusers`(`UserId`,`UserName`,`FatherName`,`MotherName`,`SpouseName`,`DateOfBirth`,`Gender`,`BloodGroup`,`Designation`,`MaritalStatus`,`Password`,`ConfirmPassword`,`PrimaryContactNumber`,`SecondryContactNumber`,`EmergencyContactName`,`EmergencyContactNumber`,`HighestQualification`,`EmailId`,`PassportNumber`,`PANCardNumber`,`AdhaarCardNumber`,`MailingHouseNumber`,`MailingStreetName`,`MailingLocality`,`MailingCity`,`MailingState`,`MailingCountry`,`MailingPincode`,`PermanentHouseNumber`,`PermanentStreetName`,`PermanentLocality`,`PermanentCity`,`PermanentState`,`PermanentCountry`,`PermanentPincode`,`CreatedBy`,`CreatedDateTime`,`UpdatedBy`,`UpdatedDateTime`,`DeletedBy`,`DeletedDateTime`,`IsDeleted`,`IPAddress`,`CentreId`,`CorrespondenceAddress`,`PermanentAddress`,`LandlineNumber`,`GuardianName`) values 
(4,'mohd alam','rakiv ali','sanjeeda khatoon','maahi','2018-03-29',1,1,1,1,'abcd@123','abcd@123','99997977434',NULL,'alam','9876543210','cs','abcd@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2018-03-29 13:34:00',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),
(5,'Mohd Alam','Rakiv Ali','Sanjeeda Khatoon','Maahi','2021-11-01',1,1,1,1,'alam@123','alam@123','4344434434',NULL,'alam','343534534','b.tech','mohd.alam@gmail.com',NULL,NULL,NULL,NULL,NULL,'0','0',0,0,'0',NULL,NULL,'0','0',0,0,'0',4,'2021-11-01 16:50:47',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),
(6,'Mohd Alam','Rakiv Ali','Sanjeeda Khatoon','Maahi','2021-11-01',1,1,1,1,'alam@123','alam@123','4344434434',NULL,'alam','343534534','b.tech','mohd.alam@gmail.com',NULL,NULL,NULL,NULL,NULL,'0','0',0,0,'0',NULL,NULL,'0','0',0,0,'0',4,'2021-11-01 17:19:12',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),
(7,'Mohd Alam','Rakiv Ali','Sanjeeda Khatoon','Maahi','2021-11-01',1,1,1,1,'alam@123','alam@123','4344434434',NULL,'alam','343534534','b.tech','mohd.alam@gmail.com',NULL,NULL,NULL,NULL,NULL,'0','0',0,0,'0',NULL,NULL,'0','0',0,0,'0',4,'2021-11-01 17:22:38',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),
(8,'Mohd Alam','Rakiv Ali','Sanjeeda Khatoon','Maahi','2021-11-01',1,1,1,1,'alam@123','alam@123','4344434434',NULL,'alam','343534534','b.tech','mohd.alam@gmail.com',NULL,NULL,NULL,NULL,NULL,'0','0',0,0,'0',NULL,NULL,'0','0',0,0,'0',4,'2021-11-01 17:25:02',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),
(9,'Mohd Alam','Rakiv Ali','Sanjeeda Khatoon','Maahi','2021-11-01',1,1,1,1,'alam@123','alam@123','4344434434',NULL,'alam','343534534','b.tech','mohd.alam@gmail.com',NULL,NULL,NULL,NULL,NULL,'0','0',0,0,'0',NULL,NULL,'0','0',0,0,'0',4,'2021-11-01 17:25:54',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),
(10,'Mohd Alam','Rakiv Ali','Sanjeeda Khatoon','Maahi','2018-03-29',1,2,1,2,'alam@1234567','alam@1234576','4344434434',NULL,'alam','343534534','b.tech','mohd.alam@gmail.com',NULL,NULL,NULL,NULL,NULL,'0','0',0,0,'0',NULL,NULL,'0','0',0,0,'0',4,'2021-11-01 17:27:14',10,'2021-11-03 17:42:54',NULL,NULL,0,'192.168.37.28',NULL,NULL,NULL,NULL,NULL),
(11,'Mohd Alam','Rakiv Ali','Sanjeeda Khatoon','Maahi','2021-11-01',1,1,1,1,'alam@123','alam@123','4344434434',NULL,'alam','343534534','b.tech','mohd.alam@gmail.com',NULL,NULL,NULL,NULL,NULL,'0','0',0,0,'0',NULL,NULL,'0','0',0,0,'0',4,'2021-11-01 17:33:21',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),
(12,'Mohd Alam',NULL,'Sanjeeda Khatoon','Maahi','2018-03-29',1,1,1,1,'alam@123','alam@123','4344434434',NULL,'alam','343534534','b.tech','mohd.alam@gmail.com',NULL,NULL,NULL,NULL,NULL,'0','0',0,0,'0',NULL,NULL,'0','0',0,0,'0',4,'2021-11-01 18:35:27',NULL,NULL,NULL,NULL,0,'192.168.37.28',NULL,NULL,NULL,NULL,NULL),
(13,'Mohd Alam',NULL,'Sanjeeda Khatoon','Maahi','2018-03-29',1,1,1,1,'alam@123','alam@123','4344434434',NULL,'alam','343534534','b.tech','mohd.alam@gmail.com',NULL,NULL,NULL,NULL,NULL,'0','0',0,0,'0',NULL,NULL,'0','0',0,0,'0',4,'2021-11-01 18:50:00',NULL,NULL,NULL,NULL,0,'192.168.37.28',NULL,NULL,NULL,NULL,NULL),
(14,'Mohd Alam',NULL,'Sanjeeda Khatoon','Maahi','2018-03-29',1,1,1,1,'alam@12345','alam@12345','4344434434',NULL,'alam','343534534','b.tech','mohd.alam@gmail.com',NULL,NULL,NULL,NULL,NULL,'0','0',0,0,'0',NULL,NULL,'0','0',0,0,'0',4,'2021-11-01 19:08:13',NULL,NULL,NULL,NULL,0,'192.168.37.28',NULL,NULL,NULL,NULL,NULL),
(15,'Mohd Alam','Rakiv Ali','Sanjeeda Khatoon','Maahi','2018-03-29',1,1,1,1,'alam@12345','alam@12345','4344434434',NULL,'alam','343534534','b.tech','mohd.alam@gmail.com',NULL,NULL,NULL,NULL,NULL,'0','0',0,0,'0',NULL,NULL,'0','0',0,0,'0',4,'2021-11-01 19:17:36',NULL,NULL,NULL,NULL,0,'192.168.37.28',NULL,NULL,NULL,NULL,NULL),
(16,'Mohd Alam','Rakiv Ali','Sanjeeda Khatoon','Maahi','2018-03-29',1,1,1,1,'alam@12345','alam@12345','4344434434',NULL,'alam','343534534','b.tech','mohd.alam@gmail.com',NULL,NULL,NULL,NULL,NULL,'0','0',0,0,'0',NULL,NULL,'0','0',0,0,'0',4,'2021-11-01 19:23:36',NULL,NULL,NULL,NULL,0,'192.168.37.28',NULL,NULL,NULL,NULL,NULL),
(17,'Mohd Alam','Rakiv Ali','Sanjeeda Khatoon','Maahi','2018-03-29',1,2,1,2,'alam@1234567','alam@1234576','4344434434',NULL,'alam','343534534','b.tech','mohd.alam@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,0,0,NULL,4,'2021-11-03 18:10:21',NULL,NULL,NULL,NULL,0,'192.168.37.28',NULL,NULL,NULL,NULL,NULL),
(18,'Mohd Alam','Rakiv Ali','Sanjeeda Khatoon','Maahi','2018-03-29',1,2,1,2,'alam@1234567','alam@1234576','4344434434',NULL,'alam','343534534','b.tech','mohd.alam@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,0,0,NULL,4,'2021-11-03 18:16:43',NULL,NULL,NULL,NULL,0,'192.168.37.28',NULL,NULL,NULL,NULL,NULL),
(19,'Imran Ahmad','Rakiv Ali','Sanjeeda Khatoon','Maahi','2018-03-29',1,2,1,2,'alam@123','alam@123','4344434434',NULL,'alam','343534534','b.tech','mohd.alam@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,0,0,NULL,4,'2021-11-03 18:53:35',NULL,NULL,NULL,NULL,0,'192.168.37.28',NULL,NULL,NULL,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
