/*
SQLyog Community v13.1.7 (64 bit)
MySQL - 8.0.21 : Database - storemanagement
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`storemanagement` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `storemanagement`;

/*Table structure for table `tblusers` */

DROP TABLE IF EXISTS `tblusers`;

CREATE TABLE `tblusers` (
  `UserId` bigint NOT NULL AUTO_INCREMENT,
  `UserName` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `FatherName` varchar(100) DEFAULT NULL,
  `MotherName` varchar(100) DEFAULT NULL,
  `SpouseName` varchar(100) DEFAULT NULL,
  `DateOfBirth` date DEFAULT NULL,
  `Gender` bigint DEFAULT NULL,
  `BloodGroup` bigint DEFAULT NULL,
  `Roles` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `MaritalStatus` bigint DEFAULT NULL,
  `Password` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `PrimaryContactNumber` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `SecondryContactNumber` varchar(20) DEFAULT NULL,
  `EmergencyContactName` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `EmergencyContactNumber` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `HighestQualification` varchar(30) DEFAULT NULL,
  `EmailId` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `PassportNumber` varchar(20) DEFAULT NULL,
  `PANCardNumber` varchar(20) DEFAULT NULL,
  `AdhaarCardNumber` varchar(20) DEFAULT NULL,
  `CreatedBy` bigint NOT NULL,
  `CreatedDateTime` datetime NOT NULL,
  `UpdatedBy` bigint DEFAULT NULL,
  `UpdatedDateTime` datetime DEFAULT NULL,
  `DeletedBy` bigint DEFAULT NULL,
  `DeletedDateTime` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) DEFAULT NULL,
  `IPAddress` varchar(100) DEFAULT NULL,
  `Centres` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `PermanentAddress` varchar(500) DEFAULT NULL,
  `LandlineNumber` varchar(20) DEFAULT NULL,
  `GuardianName` varchar(100) DEFAULT NULL,
  `LoginName` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`UserId`),
  KEY `CreatedBy` (`CreatedBy`),
  KEY `Gender` (`Gender`),
  KEY `BloodGroup` (`BloodGroup`),
  KEY `MaritalStatus` (`MaritalStatus`),
  KEY `tblusers_ibfk_2` (`UpdatedBy`),
  KEY `tblusers_ibfk_3` (`DeletedBy`),
  KEY `Designation` (`Roles`),
  KEY `CentreId` (`Centres`),
  CONSTRAINT `tblusers_ibfk_2` FOREIGN KEY (`UpdatedBy`) REFERENCES `tblusers` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tblusers_ibfk_3` FOREIGN KEY (`DeletedBy`) REFERENCES `tblusers` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tblusers_ibfk_4` FOREIGN KEY (`Gender`) REFERENCES `tblconfigmaster` (`ConfigId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tblusers_ibfk_5` FOREIGN KEY (`BloodGroup`) REFERENCES `tblconfigmaster` (`ConfigId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tblusers_ibfk_7` FOREIGN KEY (`MaritalStatus`) REFERENCES `tblconfigmaster` (`ConfigId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `tblusers` */

insert  into `tblusers`(`UserId`,`UserName`,`FatherName`,`MotherName`,`SpouseName`,`DateOfBirth`,`Gender`,`BloodGroup`,`Roles`,`MaritalStatus`,`Password`,`PrimaryContactNumber`,`SecondryContactNumber`,`EmergencyContactName`,`EmergencyContactNumber`,`HighestQualification`,`EmailId`,`PassportNumber`,`PANCardNumber`,`AdhaarCardNumber`,`CreatedBy`,`CreatedDateTime`,`UpdatedBy`,`UpdatedDateTime`,`DeletedBy`,`DeletedDateTime`,`IsDeleted`,`IPAddress`,`Centres`,`PermanentAddress`,`LandlineNumber`,`GuardianName`,`LoginName`) values 
(38,'july ausust september','father testig','mother testing','testing spouse','2000-10-10',1,4,'10,11,11',12,'project@123','7979797979','7979797979','alamimran kanpur-test','775575757575',NULL,'kjjkjkj@gmail.com','9797979797','9797979797','797979797979',4,'2022-07-30 13:52:02',38,'2022-07-30 15:49:58',NULL,NULL,0,'192.168.104.28','15,16,16','pakka bagh galshaheed moradabad','979797979797','guardian test','project'),
(39,'july ausust september','father testig','mother testing','testing spouse','2000-10-10',1,4,'3,4',12,'project@123','7979797979','7979797979','alamimran kanpur-test','775575757575',NULL,'kjjkjkj@gmail.com','9797979797','9797979797','797979797979',4,'2022-07-30 13:54:16',NULL,NULL,NULL,NULL,0,'192.168.104.28','9,10','pakka bagh galshaheed moradabad','979797979797','guardian test','project');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
