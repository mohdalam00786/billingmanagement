/*
SQLyog Community v13.1.7 (64 bit)
MySQL - 8.0.21 : Database - storemanagement
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`storemanagement` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `storemanagement`;

/*Table structure for table `tblcentremaster` */

DROP TABLE IF EXISTS `tblcentremaster`;

CREATE TABLE `tblcentremaster` (
  `CentreId` bigint NOT NULL AUTO_INCREMENT,
  `CentreName` varchar(100) NOT NULL,
  `CentreCode` varchar(10) DEFAULT NULL,
  `Website` varchar(100) DEFAULT NULL,
  `Address` varchar(100) DEFAULT NULL,
  `MobileNumber` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `LandlineNumber` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `EmailId` varchar(100) DEFAULT NULL,
  `CreatedBy` bigint NOT NULL,
  `CreatedDateTime` datetime NOT NULL,
  `UpdatedBy` bigint DEFAULT NULL,
  `UpdatedDateTime` datetime DEFAULT NULL,
  `DeletedBy` bigint DEFAULT NULL,
  `DeletedDateTime` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) DEFAULT NULL,
  `IPAddress` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`CentreId`),
  KEY `UpdatedBy` (`UpdatedBy`),
  KEY `DeletedBy` (`DeletedBy`),
  CONSTRAINT `tblcentremaster_ibfk_1` FOREIGN KEY (`UpdatedBy`) REFERENCES `tblusers` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tblcentremaster_ibfk_2` FOREIGN KEY (`DeletedBy`) REFERENCES `tblusers` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `tblcentremaster` */

insert  into `tblcentremaster`(`CentreId`,`CentreName`,`CentreCode`,`Website`,`Address`,`MobileNumber`,`LandlineNumber`,`EmailId`,`CreatedBy`,`CreatedDateTime`,`UpdatedBy`,`UpdatedDateTime`,`DeletedBy`,`DeletedDateTime`,`IsDeleted`,`IPAddress`) values 
(1,'Indiana Pvt Ltd','IND001','www.indiana.com',NULL,'9999999999',NULL,'indiana@gmail.com',0,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL),
(8,'kdkdkh','sckdhk','sdjdj.com','vndknvkdn','djvbjdbvjd','nckdnfkdn','scknk@gmail.com',4,'2021-11-17 17:23:01',NULL,NULL,NULL,NULL,1,'192.168.38.28'),
(9,'centre 5','005','web.com','centre address 5 new ','3555353353','','alam@gmail.com',4,'2021-11-20 00:09:43',38,'2022-08-04 21:57:04',38,'2022-08-04 22:11:03',1,'127.0.0.1'),
(11,'centr11','knvkdn','nvdnv','vldnvldnvldnlv','vndnvkdn','dlvdlnv','dvndnvk',4,'2021-11-20 00:10:46',NULL,NULL,38,'2022-08-04 22:12:00',1,'192.168.45.28'),
(14,'centre11','khhk','khkh','686868','8868686868','868686','kkhkhkk@gmail.com',4,'2021-12-24 15:20:07',NULL,NULL,38,'2022-08-05 21:55:55',1,'127.0.0.1'),
(15,'centre111','hkh','hhlh','null','696969696','69696','alam@gmail.com',4,'2021-12-25 21:33:06',NULL,NULL,38,'2022-08-05 22:11:17',1,'192.168.154.28'),
(16,'alam12','alamnew','alamcom','new adataaddress','9912345678','NA','ff@gmail.com',4,'2022-03-19 14:42:08',60,'2022-08-23 17:05:02',NULL,NULL,0,'192.168.128.28'),
(18,'hathala centre 2','hr02','gg@.com','address data with new centre','9989898989','886868686868','abcd@gmail.com',4,'2022-08-04 20:46:34',NULL,NULL,NULL,NULL,0,'127.0.0.1'),
(19,'mda colony ','mda12','ggg@.com','new addresss mda olony','9797979797','770707070707','kjfkdjfkd@gmail.com',4,'2022-08-04 21:51:19',NULL,NULL,NULL,NULL,0,'127.0.0.1'),
(20,'procurement centre data','cp01','wwwdata@.com','address new data','8787878787','jkjkjkjkjkjk','abcd@gmail.com',4,'2022-08-05 22:12:06',NULL,NULL,NULL,NULL,0,'192.168.65.28'),
(21,'mohd alam new shop','cp01','www.abc.com','address newdata','8898989898','k9979797979','abc@gmail.com',4,'2022-08-05 22:14:42',NULL,NULL,NULL,NULL,0,'192.168.65.28'),
(22,'centre 10','c10','www.abc.com','addressnew','8787878787','landline','ac@gmail.com',4,'2022-08-05 22:18:49',NULL,NULL,NULL,NULL,0,'192.168.65.28'),
(23,'imaan centre','c111','www.xyz.com','address data new record','8688686868','68686868868','abc@gmail.com',4,'2022-08-05 22:19:27',NULL,NULL,NULL,NULL,0,'192.168.65.28'),
(24,'abdulla cetre','c12','www.abc.com','adress new data','7676767676','5454545454','abc@gmail.com',4,'2022-08-06 20:15:39',38,'2022-08-06 20:17:00',38,'2022-08-06 20:17:27',1,'192.168.65.28'),
(25,'new data cetre','kjijij','kjkjkj','address','7878787979','979797979797','abcd@gmail.com',4,'2022-08-21 20:01:01',NULL,NULL,60,'2022-08-23 17:54:55',1,'192.168.34.28'),
(26,'new centre    ','','','new address','7575785857','','alam@gmail.com',60,'2022-08-23 19:53:39',NULL,NULL,60,'2022-08-23 19:56:23',1,'127.0.0.1'),
(27,'alam test','alamcode','www.test.com','yes address','7887875875','','yahoo@gmail.com',60,'2022-10-21 22:37:03',NULL,NULL,60,'2022-10-21 22:38:01',1,'192.168.177.28');

/*Table structure for table `tblconfigmaster` */

DROP TABLE IF EXISTS `tblconfigmaster`;

CREATE TABLE `tblconfigmaster` (
  `ConfigId` bigint NOT NULL AUTO_INCREMENT,
  `ConfigKey` varchar(100) NOT NULL,
  `ConfigValue` varchar(100) NOT NULL,
  `ConfigCode` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `CreatedBy` bigint NOT NULL,
  `CreatedDateTime` datetime NOT NULL,
  `UpdatedBy` bigint DEFAULT NULL,
  `UpdatedDateTime` datetime DEFAULT NULL,
  `DeletedBy` bigint DEFAULT NULL,
  `DeletedDateTime` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) DEFAULT NULL,
  `IPAddress` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ConfigId`,`ConfigCode`),
  KEY `ConfigId` (`ConfigId`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `tblconfigmaster` */

insert  into `tblconfigmaster`(`ConfigId`,`ConfigKey`,`ConfigValue`,`ConfigCode`,`CreatedBy`,`CreatedDateTime`,`UpdatedBy`,`UpdatedDateTime`,`DeletedBy`,`DeletedDateTime`,`IsDeleted`,`IPAddress`) values 
(1,'Gender','Male','M',0,'2022-07-30 14:50:35',38,'2022-08-14 17:31:42',NULL,NULL,0,'192.168.226.28'),
(2,'Gender','Female','F',0,'2022-07-30 14:50:35',NULL,NULL,NULL,NULL,0,NULL),
(3,'Gender','Other','O',0,'2022-07-30 14:50:35',NULL,NULL,NULL,NULL,0,NULL),
(4,'BloodGroup','A Positive','AP',0,'2022-07-30 14:50:35',NULL,NULL,NULL,NULL,0,NULL),
(5,'BloodGroup','A Negative','AN',0,'2022-07-30 14:50:35',NULL,NULL,NULL,NULL,0,NULL),
(6,'BloodGroup','B Positive','BP',0,'2022-07-30 14:50:35',NULL,NULL,NULL,NULL,0,NULL),
(7,'BloodGroup','B Negative','BN',0,'2022-07-30 14:50:35',NULL,NULL,NULL,NULL,0,NULL),
(8,'BloodGroup','AB Positive','ABP',0,'2022-07-30 14:50:35',NULL,NULL,NULL,NULL,0,NULL),
(9,'BloodGroup','AB Negative','ABN',0,'2022-07-30 14:50:35',NULL,NULL,NULL,NULL,0,NULL),
(10,'BloodGroup','O Positive','OP',0,'2022-07-30 14:50:35',NULL,NULL,NULL,NULL,0,NULL),
(11,'BloodGroup','O Negative','ON',0,'2022-07-30 14:50:35',NULL,NULL,NULL,NULL,0,NULL),
(12,'MaritalStatus','Single','Single',0,'2022-07-30 14:50:35',NULL,NULL,NULL,NULL,0,NULL),
(13,'MaritalStatus','Married','Married',0,'2022-07-30 14:50:35',NULL,NULL,NULL,NULL,0,NULL),
(14,'MaritalStatus','CommonLaw','CommonLaw',0,'2022-07-30 14:50:35',38,'2022-08-13 22:10:12',NULL,NULL,0,'127.0.0.1'),
(15,'MaritalStatus','Separated','Separated',0,'2022-07-30 14:50:35',NULL,NULL,NULL,NULL,0,NULL),
(16,'MaritalStatus','Widow','Widow',0,'2022-07-30 14:50:35',NULL,NULL,NULL,NULL,0,NULL),
(17,'MaritalStatus','Divorced','Divorced',0,'2022-07-30 14:50:35',NULL,NULL,NULL,NULL,0,NULL);

/*Table structure for table `tbllogin` */

DROP TABLE IF EXISTS `tbllogin`;

CREATE TABLE `tbllogin` (
  `LoginId` bigint NOT NULL AUTO_INCREMENT,
  `UserId` bigint NOT NULL,
  `Password` varchar(300) NOT NULL,
  `TotalAttemptCount` tinyint DEFAULT NULL,
  `EmailId` varchar(100) NOT NULL,
  `LoginName` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`LoginId`,`UserId`),
  KEY `UserId` (`UserId`),
  CONSTRAINT `tbllogin_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `tblusers` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `tbllogin` */

insert  into `tbllogin`(`LoginId`,`UserId`,`Password`,`TotalAttemptCount`,`EmailId`,`LoginName`) values 
(4,54,'alam@123',0,'null','alam'),
(5,55,'swt4VwA6PbFmkeEn+SsaFwlYMXQZjGNGtA==',0,'null','alam'),
(6,56,'TGFZUnL4fIyOnvwr2Tq8dXrJrndKlRCTdw==',0,'null','alam'),
(7,57,'swt4VwA6PbFmkeEn+SsaFwlYMXQZjGNGtA==',0,'hkkhk','alam'),
(10,60,'swt4VwA6PbFmkeEn+SsaFwlYMXQZjGNGtA==',0,'null','alam'),
(11,61,'b2vAsqHk5ZzaceaUIdasXZyi5MI2ndy0ag==',0,'null','new'),
(12,62,'swt4VwA6PbFmkeEn+SsaFwlYMXQZjGNGtA==',0,'','yesde'),
(13,63,'swt4VwA6PbFmkeEn+SsaFwlYMXQZjGNGtA==',0,'','bjkgkgkj');

/*Table structure for table `tblrolemaster` */

DROP TABLE IF EXISTS `tblrolemaster`;

CREATE TABLE `tblrolemaster` (
  `RoleId` bigint NOT NULL AUTO_INCREMENT,
  `RoleName` varchar(100) DEFAULT NULL,
  `CreatedBy` bigint NOT NULL,
  `CreatedDateTime` datetime NOT NULL,
  `UpdatedBy` bigint DEFAULT NULL,
  `UpdatedDateTime` datetime DEFAULT NULL,
  `DeletedBy` bigint DEFAULT NULL,
  `DeletedDateTime` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) DEFAULT NULL,
  `IPAddress` varchar(100) DEFAULT NULL,
  `RoleCode` varchar(10) DEFAULT NULL,
  `RoleDescription` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`RoleId`),
  KEY `tblrolemaster_ibfk_1` (`CreatedBy`),
  KEY `tblrolemaster_ibfk_2` (`UpdatedBy`),
  KEY `tblrolemaster_ibfk_3` (`DeletedBy`),
  CONSTRAINT `tblrolemaster_ibfk_1` FOREIGN KEY (`CreatedBy`) REFERENCES `tblusers` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tblrolemaster_ibfk_2` FOREIGN KEY (`UpdatedBy`) REFERENCES `tblusers` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tblrolemaster_ibfk_3` FOREIGN KEY (`DeletedBy`) REFERENCES `tblusers` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `tblrolemaster` */

insert  into `tblrolemaster`(`RoleId`,`RoleName`,`CreatedBy`,`CreatedDateTime`,`UpdatedBy`,`UpdatedDateTime`,`DeletedBy`,`DeletedDateTime`,`IsDeleted`,`IPAddress`,`RoleCode`,`RoleDescription`) values 
(9,'Admin',38,'2022-07-30 14:50:35',38,'2022-08-13 17:52:45',NULL,NULL,0,'192.168.226.28','Adm',NULL),
(10,'Procurements',38,'2022-07-30 14:51:32',60,'2022-09-04 12:43:08',NULL,NULL,0,'192.168.184.28','Pro',''),
(11,'Billing',38,'2022-07-30 14:51:49',NULL,NULL,NULL,NULL,0,NULL,'Bil',NULL),
(12,'xyz aa',38,'2022-08-13 17:55:14',38,'2022-08-13 17:56:02',38,'2022-08-13 17:56:16',1,'192.168.226.28','yes',NULL),
(13,'Requestor',38,'2022-08-14 20:06:17',38,'2022-08-14 20:06:39',38,'2022-08-14 20:06:46',1,'192.168.226.28','RE0123',NULL),
(14,'role name',60,'2022-08-23 19:28:39',60,'2022-08-23 19:29:05',60,'2022-08-23 19:29:13',1,'127.0.0.1','',NULL),
(15,'new role',60,'2022-08-23 20:08:09',60,'2022-08-23 20:08:32',60,'2022-08-23 20:08:43',1,'127.0.0.1','',NULL),
(16,'test data',60,'2022-09-03 19:24:23',60,'2022-09-03 19:28:58',60,'2022-09-03 19:33:53',1,'127.0.0.1','te','role descr'),
(17,'test role',60,'2022-09-03 20:16:33',NULL,NULL,60,'2022-09-03 22:01:28',1,'192.168.188.28','tes','roledescri'),
(18,'new role data',60,'2022-09-03 20:24:26',NULL,NULL,60,'2022-09-03 22:01:32',1,'192.168.188.28','new','');

/*Table structure for table `tblusers` */

DROP TABLE IF EXISTS `tblusers`;

CREATE TABLE `tblusers` (
  `UserId` bigint NOT NULL AUTO_INCREMENT,
  `UserName` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `FatherName` varchar(100) DEFAULT NULL,
  `MotherName` varchar(100) DEFAULT NULL,
  `SpouseName` varchar(100) DEFAULT NULL,
  `DateOfBirth` date DEFAULT NULL,
  `Gender` varchar(10) DEFAULT NULL,
  `BloodGroup` varchar(10) DEFAULT NULL,
  `Roles` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `MaritalStatus` varchar(10) DEFAULT NULL,
  `Password` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `PrimaryContactNumber` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `SecondryContactNumber` varchar(20) DEFAULT NULL,
  `EmergencyContactName` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `EmergencyContactNumber` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `HighestQualification` varchar(30) DEFAULT NULL,
  `EmailId` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `PassportNumber` varchar(20) DEFAULT NULL,
  `PANCardNumber` varchar(20) DEFAULT NULL,
  `AdhaarCardNumber` varchar(20) DEFAULT NULL,
  `CreatedBy` bigint NOT NULL,
  `CreatedDateTime` datetime NOT NULL,
  `UpdatedBy` bigint DEFAULT NULL,
  `UpdatedDateTime` datetime DEFAULT NULL,
  `DeletedBy` bigint DEFAULT NULL,
  `DeletedDateTime` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) DEFAULT NULL,
  `IPAddress` varchar(100) DEFAULT NULL,
  `Centres` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `PermanentAddress` varchar(500) DEFAULT NULL,
  `LandlineNumber` varchar(20) DEFAULT NULL,
  `GuardianName` varchar(100) DEFAULT NULL,
  `LoginName` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`UserId`),
  KEY `CreatedBy` (`CreatedBy`),
  KEY `Gender` (`Gender`),
  KEY `BloodGroup` (`BloodGroup`),
  KEY `MaritalStatus` (`MaritalStatus`),
  KEY `tblusers_ibfk_2` (`UpdatedBy`),
  KEY `tblusers_ibfk_3` (`DeletedBy`),
  KEY `Designation` (`Roles`),
  KEY `CentreId` (`Centres`),
  CONSTRAINT `tblusers_ibfk_2` FOREIGN KEY (`UpdatedBy`) REFERENCES `tblusers` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tblusers_ibfk_3` FOREIGN KEY (`DeletedBy`) REFERENCES `tblusers` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `tblusers` */

insert  into `tblusers`(`UserId`,`UserName`,`FatherName`,`MotherName`,`SpouseName`,`DateOfBirth`,`Gender`,`BloodGroup`,`Roles`,`MaritalStatus`,`Password`,`PrimaryContactNumber`,`SecondryContactNumber`,`EmergencyContactName`,`EmergencyContactNumber`,`HighestQualification`,`EmailId`,`PassportNumber`,`PANCardNumber`,`AdhaarCardNumber`,`CreatedBy`,`CreatedDateTime`,`UpdatedBy`,`UpdatedDateTime`,`DeletedBy`,`DeletedDateTime`,`IsDeleted`,`IPAddress`,`Centres`,`PermanentAddress`,`LandlineNumber`,`GuardianName`,`LoginName`) values 
(38,'july ausust september','father testig','mother testing','testing spouse','2000-10-21','M','BP','Adm,Pro,Bil','Single','4KwbEVcalsaPzo3sqMLTLRsrBqE+rKpY1w==','7979797979','7979797979','alamimran kanpur-test','775575757575',NULL,'kjjkjkj@gmail.com','9797979797','','797979797979',4,'2022-07-30 13:52:02',60,'2022-09-03 22:21:38',NULL,NULL,0,'127.0.0.1','18,23','pakka bagh galshaheed moradabad','979797979797','guardian test','yes'),
(39,'july ausust september','father testig','mother testing','testing spouse','2000-10-10','M','BP','Adm,Pro','Single','project@123','7979797979','7979797979','alamimran kanpur-test','775575757575',NULL,'kjjkjkj@gmail.com','9797979797','9797979797','797979797979',4,'2022-07-30 13:54:16',39,'2022-08-13 16:20:57',NULL,NULL,0,'192.168.226.28','18,23,21','pakka bagh galshaheed moradabad','979797979797','guardian test','yes'),
(43,'tester data','father test','mother test','spouse test','2000-10-10','M','BP','Adm,Pro','Single','WPgRqmoqg5tclbQhkoKGtybM+6sK2bJPDQ==','9808080808','6466464646','emergenc yname','757757575757',NULL,'abcd@gmail.com','8686868686','','545454545454',0,'2022-08-06 12:49:59',60,'2022-08-23 19:03:15',60,'2022-08-23 19:05:20',1,'127.0.0.1','18,20,21','permanant address','','guardian nema','yes'),
(44,'tester data','father test','mother test','spouse test','2000-10-10','M','BP','Adm,Pro','Single','alam@123','9808080808','6466464646','emergencyname','757757575757',NULL,'abcd@gmail.com','8686868686','9797979797','545454545454',0,'2022-08-06 12:50:04',NULL,NULL,38,'2022-08-06 13:48:22',1,'127.0.0.1','18,19','permanant address','null','guardian nema','yes'),
(45,'tester data','father test','mother test','spouse test','2000-10-10','M','BP','Adm,Pro','Single','alam@123','9808080808','6466464646','emergencyname','757757575757',NULL,'abcd@gmail.com','8686868686','9797979797','545454545454',0,'2022-08-06 12:50:07',NULL,NULL,NULL,NULL,0,'127.0.0.1','18,19','permanant address','null','guardian nema','yes'),
(46,'tester data','father test','mother test','spouse test','2000-10-10','M','BP','Adm,Pro','Single','alam@123','9808080808','6466464646','emergencyname','757757575757',NULL,'abcd@gmail.com','8686868686','9797979797','545454545454',0,'2022-08-06 12:50:10',NULL,NULL,NULL,NULL,0,'127.0.0.1','18,19','permanant address','null','guardian nema','yes'),
(47,'tester data','father test','mother test','spouse test','2000-10-10','M','BP','Adm,Pro','Single','alam@123','9808080808','6466464646','emergencyname','757757575757',NULL,'abcd@gmail.com','8686868686','9797979797','545454545454',0,'2022-08-06 12:50:12',NULL,NULL,NULL,NULL,0,'127.0.0.1','18,19','permanant address','null','guardian nema','yes'),
(48,'tester data','father test','mother test','spouse test','2000-10-10','M','BP','Adm,Pro','Single','alam@123','9808080808','6466464646','emergencyname','757757575757',NULL,'abcd@gmail.com','8686868686','9797979797','545454545454',0,'2022-08-06 12:50:15',NULL,NULL,NULL,NULL,0,'127.0.0.1','18,19','permanant address','null','guardian nema','yes'),
(49,'tester data','father test','mother test','spouse test','2000-10-10','M','BP','Adm,Pro','Single','alam@123','9808080808','6466464646','emergencyname','757757575757',NULL,'abcd@gmail.com','8686868686','9797979797','545454545454',0,'2022-08-06 12:50:17',NULL,NULL,NULL,NULL,0,'127.0.0.1','18,19','permanant address','null','guardian nema','yes'),
(50,'tester data','father test','mother test','spouse test','2000-10-10','M','BP','Adm,Pro','Single','alam@123','9808080808','6466464646','emergencyname','757757575757',NULL,'abcd@gmail.com','8686868686','9797979797','545454545454',0,'2022-08-06 12:50:19',NULL,NULL,NULL,NULL,0,'127.0.0.1','18,19','permanant address','null','guardian nema','yes'),
(51,'tester data','father test','mother test','spouse test','2000-10-10','M','BP','Adm,Pro','Single','alam@123','9808080808','6466464646','emergencyname','757757575757',NULL,'abcd@gmail.com','8686868686','9797979797','545454545454',0,'2022-08-06 12:50:22',51,'2022-08-15 17:20:19',NULL,NULL,0,'127.0.0.1','23,20','permanant address','null','guardian nema','yes'),
(52,'tester data','father test','mother test','spouse test','2000-10-10','M','BP','Adm,Pro','Single','alam@123','9808080808','6466464646','emergencyname','757757575757',NULL,'abcd@gmail.com','8686868686','9797979797','545454545454',0,'2022-08-06 12:50:25',NULL,NULL,NULL,NULL,0,'127.0.0.1','18,19','permanant address','null','guardian nema','yes'),
(53,'jjdkjksjk','null','null','NA','2022-08-02','M','BP','Adm,Pro','Single','alam@123','7979797997','jbjgjgjj','gkgkgkgkjgkgkj','797979797979',NULL,'null','null','pan','null',0,'2022-08-13 15:03:33',53,'2022-08-15 20:44:44',NULL,NULL,0,'192.168.226.28','16,23','address','','ggg','yes'),
(54,'alam','rakib ali','sanjeeda ','spouse','1994-09-02','M','BP','Adm,Pro','Single','alam@123','9917441044','null','rakib ali','9917821771',NULL,'null','NA','ayzpa4878r','504639867219',0,'2022-08-15 20:51:48',NULL,NULL,38,'2022-08-15 21:01:32',1,'192.168.226.28',',23','addresss data','null','NA','yes'),
(55,'mohd alam','rakib ali','sanjeeda','NA','1994-02-09','M','BP','Adm,Pro','Single','swt4VwA6PbFmkeEn+SsaFwlYMXQZjGNGtA==','9917441044','null','rakib ali','9917821771',NULL,'null','na','ayzpa4878r','504639867219',0,'2022-08-15 21:06:06',NULL,NULL,38,'2022-08-16 15:28:50',1,'192.168.226.28',',,16,21','b-13 himgiri colony harthala moradabad UP-20001','null','guardina','alam'),
(56,'kdskdksj','null','null','null','2022-08-15','M','BP','Adm,Pro','Single','TGFZUnL4fIyOnvwr2Tq8dXrJrndKlRCTdw==','8688686868','686868','868686868686868','686868686868',NULL,'null','null','null','null',0,'2022-08-15 21:14:05',NULL,NULL,NULL,NULL,0,'192.168.226.28','22','cnjncdnjnjv','null','null','yes'),
(57,'hhhkhhkhkh','khkhk','khkhk','khkhkHKk','2022-08-03','M','BP','Adm,Pro','Single','swt4VwA6PbFmkeEn+SsaFwlYMXQZjGNGtA==','khkhkh','khkkh','khkhkh','khkhk',NULL,'hkkhk','khkhkh','khkhkh','khkhkhk',0,'2022-08-15 21:52:10',NULL,NULL,NULL,NULL,0,'192.168.226.28','22','khkhkh bkgkg','khkkhkh','khkhkhk','yes'),
(60,'mohd alam','rakib','null','NA','2001-03-01','M','BP','Adm,Pro','Single','8JwyXTz6Hd9hU+DRR7ePYEqQCq2RwvM0mQ==','9917441044','null','rakib','9917821771',NULL,'null','null','null','null',0,'2022-08-16 15:30:20',60,'2022-08-16 15:36:22',NULL,NULL,0,'192.168.157.28','16,21,19','himgiri colony','null','null','alam'),
(61,'new user','na','null','na','2022-08-09','M','BP','Adm,Pro','Single','das1R1ZFh07597VK87jCVSGb+QH5XKHoJw==','7979797979','null','na','758786986874',NULL,'null','null','null','null',0,'2022-08-20 15:26:45',61,'2022-08-21 17:43:46',NULL,NULL,0,'192.168.34.28','22,23','null','null','null','new'),
(62,'yes sir','','','','2022-09-01','M','BP','Adm,Pro','Single','swt4VwA6PbFmkeEn+SsaFwlYMXQZjGNGtA==','7575875858','','bkjkjvkjgkj','587578578578',NULL,'','','','',60,'2022-09-03 21:57:51',NULL,NULL,NULL,NULL,0,'127.0.0.1','16,18','','','','yesde'),
(63,'alam','','','','2022-09-01','M','BP','Adm,Pro','Single','swt4VwA6PbFmkeEn+SsaFwlYMXQZjGNGtA==','8578585858','','kfjkfkjfkjfkj','857858578585',NULL,'','','','',60,'2022-09-03 22:06:37',NULL,NULL,NULL,NULL,0,'127.0.0.1','16,18','','','','bjkgkgkj');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
