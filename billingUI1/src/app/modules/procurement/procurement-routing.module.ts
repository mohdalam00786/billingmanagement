import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../shared/constants/auth-guard';
import { ProcurementComponent } from './procurement.component';

const routes: Routes = [
  {
    path: '', component: ProcurementComponent,
    children: [
      { path: 'Procurement', component: ProcurementComponent, canActivate: [AuthGuard] },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProcurementRoutingModule { }
