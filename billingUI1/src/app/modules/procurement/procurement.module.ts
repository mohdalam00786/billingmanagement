import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProcurementRoutingModule } from './procurement-routing.module';
import { SharedModule } from '../shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { ProcurementComponent } from './procurement.component';


@NgModule({
  declarations: [ProcurementComponent],
  imports: [
    CommonModule,
    ProcurementRoutingModule,
    SharedModule,
    TranslateModule
  ],
  exports: [TranslateModule],
})
export class ProcurementModule { }
