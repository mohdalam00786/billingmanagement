import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { SelectItem } from 'primeng/api';
import { GenericConstants } from 'src/app/modules/shared/constants/generic-constants';
import { Status } from 'src/app/modules/shared/constants/generic-numbers';
import { AuthService } from '../../services/auth.service';
import { Global } from '../../constants/global-config';
import { Subscription } from 'rxjs';
import { UserService } from '../../services/user.service';
import { environment } from 'src/environments/environment';
import { SharedService } from '../../services/shared.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {

  LoginForm: FormGroup;
  submitted: boolean = false;
  requiredMessage: string = '';
  incorrectCredentials: boolean = false;
  CentreList!: SelectItem[];
  public subscriptions: Subscription[] = [];
  constructor(private formBuilder: FormBuilder,
    private authService: AuthService,
    private userService: UserService,
    public translateService: TranslateService,
    private sharedService: SharedService,
    public router: Router) {     
     }

  ngOnInit() {
    Global.API_ENDPOINT = environment.ApiEndpoint;
    this.LoginForm = this.formBuilder.group(
      {
        Centre: ['', Validators.required],
        LoginName: ['', Validators.required],
        Password: ['', Validators.required],
      }
    );
    this.GetAllCentreForLogin();
    this.GetTranslation();
  }

  get loginControls() {
    return this.LoginForm.controls;
  }
  async GetTranslation() {
    this.requiredMessage = await this.translateService.get('RequiredMessage').toPromise();
  }
  ngOnDestroy(): void {
    this.subscriptions.forEach(element => {
      element.unsubscribe();
    });
  }

  GetAllCentreForLogin() {
    this.subscriptions.push(
      this.authService.GetAllCentreForLogin().subscribe(
        response => {
          this.CentreList = [];
          this.CentreList.push({ 'label': "", value: GenericConstants.DefaultSelect })
          response.forEach((item: { centreId: any; centreName: any; }) => {
            this.CentreList.push({ 'label': item.centreId, value: item.centreName });
          });
        },
        error => {
          console.log('GetCentreError', error)
        }

      )
    )
  } 
  Login() {
    this.incorrectCredentials = false;
    this.submitted = true;
    if (this.LoginForm.invalid) {
      return;
    }
    this.subscriptions.push(
      this.authService.Login(this.LoginForm.value).subscribe(
        response => {
          switch (response.responseStatus) {
            case Status.LoginSuccess:
              this.userService.SetCurrentUser(Global.UserDetails, response);
              this.authService.SendUserDetails(response);
              this.sharedService.RouteNavigation(response.roleList);
              break;
            case Status.LoginFail:
              this.incorrectCredentials = true;
              break;

          }
        },
        error => {
          console.log('loginError', error)
        }

      )
    )
  }  
}
