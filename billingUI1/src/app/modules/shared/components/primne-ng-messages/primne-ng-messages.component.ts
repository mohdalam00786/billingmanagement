import { Component, OnInit } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';

@Component({
  selector: 'app-primne-ng-messages',
  templateUrl: './primne-ng-messages.component.html',
  styleUrls: ['./primne-ng-messages.component.scss']
})
export class PrimneNgMessagesComponent implements OnInit {

  constructor(
    private primengConfig:PrimeNGConfig
  ) { }

  ngOnInit(): void {
    this.primengConfig.ripple=true;
  }

}
