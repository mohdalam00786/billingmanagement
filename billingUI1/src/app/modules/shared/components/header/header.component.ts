import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Global } from '../../constants/global-config';
import { SelectItem } from 'primeng/api';
import { UserService } from '../../services/user.service';
import { AuthService } from '../../services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  subscriptions: Subscription[] = [];
  centreName: string;
  userName: string;
  roleList!: SelectItem[];
  toggle: boolean = true;
  showAdminHeader: boolean = false;
  showProcurementHeader: boolean = false;


  constructor(private router: Router,
    private userService: UserService,
    private authService: AuthService,
    public translateService:TranslateService
  ) {
    translateService.addLangs(['English', 'Arabic']);
    translateService.setDefaultLang('English');
   }

  ngOnInit(): void {
    this.authService.GetUserDetails().subscribe(userDetails => this.GetLoggedInUserDetails(userDetails));
  }

  public GetLoggedInUserDetails(userDetails): any {
    switch (typeof (userDetails)) {
      case 'object':
        if (userDetails != null || userDetails != undefined) {
          this.SetUserDetails(userDetails);
        }
        break;
      case 'string':
        var userDetails = this.userService.GetCurrentUser(Global.UserDetails);
        if (userDetails != null) {
          this.SetUserDetails(JSON.parse(userDetails));
        }
        break;
    }    
  }

  public SetUserDetails(userDetails: any) {
    this.userName = userDetails.userName;
    this.centreName = userDetails.centreName;
    this.roleList = [];
    userDetails.roleList.forEach((item: { roleCode: any; roleName: any; }) => {
      this.roleList.push({ 'label': item.roleCode, value: item.roleName });
    });
    this.RouteNavigation(userDetails.roleList);
  }
  ngOnDestroy(): void {
    this.subscriptions.forEach(element => {
      element.unsubscribe();
    });
  }

  GetCentreMaster() {
    this.router.navigate(['/Admin/CentreMaster'])
  }
  GetConfigMaster() {
    this.router.navigate(['/Admin/ConfigMaster'])
  }
  GetRoleMaster() {
    this.router.navigate(['/Admin/RoleMaster'])
  }
  GetUserMaster() {
    this.router.navigate(['/Admin/UserMaster'])
  }

  ToggleDisplay() {
    this.toggle = !this.toggle;
  }

  LogOut() {
    this.userService.RemoveCurrentUser(Global.UserDetails);
    this.userService.ClearCurrentUser();
    this.router.navigate(['/Login']);
  }
  RouteNavigation(roleList) {
    const selectedRoleList = [];
    roleList.forEach(item => {
      selectedRoleList.push(item.roleCode);
    });
    if (selectedRoleList.includes('Adm')) {
      this.showAdminHeader = true;
      this.showProcurementHeader = false;
      this.router.navigate(['/Admin/CentreMaster']);

    }
    else {
      this.GetRouteByRole(selectedRoleList[0]);
    }
  }
  GetSelectedRole(event) {
    this.GetRouteByRole(event.value);
  }
  GetRouteByRole(role) {
    switch (role) {
      case 'Adm':
        this.showAdminHeader = true;
        this.showProcurementHeader = false;
        this.router.navigate(['/Admin']);
        break;
      case 'Pro':
        this.showAdminHeader = false;
        this.showProcurementHeader = true;
        this.router.navigate(['/Procurement']);
        break;
    }
  }

  switchLang(lang: string) {
    this.translateService.use(lang);
  }
}
