import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { LoaderService } from '../../services/loader.service';
@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {

  constructor(private loaderService: LoaderService,
    private spinnerService: NgxSpinnerService) {
    
}

  ngOnInit(): void {
    this.loaderService.isLoading.subscribe((status) => {
      if (status){
          this.spinnerService.show();
      }
      else{
          this.spinnerService.hide();
      }
  });
  }

}
