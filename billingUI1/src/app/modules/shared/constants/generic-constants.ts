export class GenericConstants
{
    public static EncyptKey="SEB090909CONNECT";
    public static EmailPattern=/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    public static PasswordPattern="(?=\\D*\\d)(?=[^a-z]*[a-z])(?=[^A-Z]*[A-Z]).{8,30}";  
    public static PdfExtenstion=".pdf";  
    public static ExcelExtension=".xlsx";
    public static DefaultSelect="--Select--";
    public static DeleteConfirmationIcon='pi pi-info-circle';
    public static DateFormat="dd/MM/yyyy";
    public static DatePickerFormat='DD/MM/YYYY';
    public static GenderMale='Male';
    public static GenderFemale='Female';
    public static GenderOther='Other';
    public static PagingConfiguration=[25,50,100,500];
    public static RowsPerPage=25;

}