import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { UserService } from '../services/user.service';
import { Global } from './global-config';
@Injectable({
    providedIn: 'root'
})
export class AuthGuard  {
    constructor(private router: Router,
        private userService: UserService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        debugger;
        let userDetails=this.userService.GetCurrentUser(Global.UserDetails)
        if (userDetails) {
            return true;
        }
        this.router.navigate(['/Login']);
        return false;
    }
}