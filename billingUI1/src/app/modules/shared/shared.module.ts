import { ErrorHandler, NgModule } from '@angular/core';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatCardModule } from '@angular/material/card';
import { TableModule } from 'primeng/table';
import { NgxSpinnerModule } from 'ngx-spinner';
import { RippleModule } from 'primeng/ripple';
import { MessageModule } from 'primeng/message';
import { InputTextModule } from 'primeng/inputtext';
import { ToastModule } from 'primeng/toast';
import { CommonModule, DatePipe } from '@angular/common';
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import { CheckboxModule } from 'primeng/checkbox';
import { MessageService } from 'primeng/api';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';
import { CalendarModule } from 'primeng/calendar';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { LoaderService } from './services/loader.service';
import { MastersService } from '../admin/services/masters.service';
import { AuthService } from './services/auth.service';
import { UserService } from './services/user.service';
import { EncryptionService } from './services/encryption.service';
import { HeaderComponent } from './components/header/header.component';



@NgModule({
  declarations: [HeaderComponent],
  imports: [
    CommonModule, MessageModule,
    CardModule,
    ButtonModule,
    RippleModule,
    DropdownModule,
    CheckboxModule,
    ConfirmDialogModule,
    CalendarModule,
    BsDatepickerModule.forRoot(),    
  ],
  exports: [
    HeaderComponent,
    MatCheckboxModule,
    MatCardModule,
    TableModule,
    NgxSpinnerModule,  
    MessageModule,
    InputTextModule,
    ToastModule,
    CardModule,
    ButtonModule,
    RippleModule,
    DropdownModule,
    CheckboxModule,
    ConfirmDialogModule,
    CalendarModule,
    BsDatepickerModule    

  ],
  providers: [
    ConfirmationService,
    MessageService, LoaderService, MastersService, AuthService, UserService,
    EncryptionService, DatePipe,
    { provide: ErrorHandler }
  ]
})
export class SharedModule { }
