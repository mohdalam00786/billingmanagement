import { Injectable } from '@angular/core';
import jsPDF from "jspdf";
import "jspdf-autotable";
import { GenericConstants } from 'src/app/modules/shared/constants/generic-constants';

@Injectable({
  providedIn: 'root'
})
export class ExportToPDFService {

  constructor() { }

  GeneratePDF(columns: any, value: any, fileName: any) {
    const doc = new jsPDF('p', 'pt');
    doc['autoTable'](columns, value);
    doc.save(fileName + GenericConstants.PdfExtenstion);    
  }

}
