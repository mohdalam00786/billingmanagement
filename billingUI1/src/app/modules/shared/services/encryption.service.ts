import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import { GenericConstants } from 'src/app/modules/shared/constants/generic-constants';
import { Global } from '../constants/global-config';

@Injectable({
  providedIn: 'root'
})
export class EncryptionService {

  constructor() { }

  encryptValue(data): any {
    return CryptoJS.AES.encrypt(JSON.stringify(data), Global.EncyptKey).toString();
  }

  decryptValue(data) {
    if(data)
    {
      return CryptoJS.AES.decrypt(data, Global.EncyptKey).toString(CryptoJS.enc.Utf8);
    }
  }
  
  EncryptObject(empDetails:any): any {
    let encryptedCredential = {};    
    var key = CryptoJS.enc.Utf8.parse(GenericConstants.EncyptKey);
    var iv = CryptoJS.enc.Utf8.parse(GenericConstants.EncyptKey);
    Object.keys(empDetails).forEach(keyElement => {
        encryptedCredential[keyElement] =  CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(empDetails[keyElement]), key,
        {
          keySize: 128 / 8,
          iv: iv,
          mode: CryptoJS.mode.CBC,
          padding: CryptoJS.pad.Pkcs7
        }).toString();
    })
    return encryptedCredential;
}

EncryptParameter(Parameter : any){
    var key = CryptoJS.enc.Utf8.parse(GenericConstants.EncyptKey);
    var iv = CryptoJS.enc.Utf8.parse(GenericConstants.EncyptKey);
        Parameter =  CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(Parameter), key,
        {
          keySize: 128 / 8,
          iv: iv,
          mode: CryptoJS.mode.CBC,
          padding: CryptoJS.pad.Pkcs7
        }).toString();

        return encodeURIComponent(Parameter);
}
}
