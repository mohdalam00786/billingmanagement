import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { Global } from '../constants/global-config';
import { SharedApiAction, SharedApiController } from '../constants/shared-api-configuration';
import { EncryptionService } from './encryption.service';
import { HttpService } from './http.service';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private userSubject: BehaviorSubject<any>;

  constructor(
    private httpService: HttpService,
    private encryptionService: EncryptionService,
    private userService: UserService,
    private router: Router
  ) {
    if (this.userService.GetCurrentUser(Global.UserDetails)) {
      this.userSubject = new BehaviorSubject<any>(this.userService.GetCurrentUser(Global.UserDetails));

    } else {
      this.userSubject = new BehaviorSubject<any>(null);
    }
  }

  Login(loginModel) {

    return this.httpService.PostApiReq(SharedApiController.Authentication, SharedApiAction.Login, this.encryptionService.EncryptObject(loginModel));
  }
  GetAllCentreForLogin() {
    return this.httpService.GetApiReq(SharedApiController.Authentication, SharedApiAction.GetAllCentreForLogin);
  }
  public SendUserDetails(userDetails: any): void {
    this.userSubject.next(userDetails);
  }
  public GetUserDetails(): Observable<any> {
    return this.userSubject.asObservable();
  }
  

}
