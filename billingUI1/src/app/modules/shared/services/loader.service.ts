import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  static callCount = 0;
    isLoading = new BehaviorSubject<boolean>(false);
  constructor() { }

  show() {
    LoaderService.callCount++;   
    if (LoaderService.callCount === 1) {
        this.isLoading.next(true);    
    }
}
hide() {
   LoaderService.callCount --;   
    if (LoaderService.callCount < 1) {      
       this.isLoading.next(false);      
    }
  }
}

