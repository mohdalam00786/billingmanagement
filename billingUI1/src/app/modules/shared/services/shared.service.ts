import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  constructor(private router: Router) { }

  RouteNavigation(roleList) {
    const selectedRoleList = [];
    roleList.forEach(item => {
      selectedRoleList.push(item.roleCode);
    });
    if (selectedRoleList.includes('Adm')) {
      this.router.navigate(['/Admin']);

    }
    else {
      this.GetRouteByRole(selectedRoleList[0]);
    }
  }
  GetRouteByRole(role) {
    switch (role) {
      case 'Adm':
        this.router.navigate(['/Admin']);
        break;
      case 'Pro':
        this.router.navigate(['/Procurement']);
        break;
    }
  }



  ConvertNullToBlank(value) {
    if (value == null || value == undefined) {
      value = '';
    }
    else {
      value = value.trim();
    }
    return value;
  }


  //handle space at first place of controls
  HandleFirstPlaceSpace(event) {
    if (event.target.selectionStart === 0 && event.code === 'Space') {
      event.preventDefault();
    }
  }
  // accept only numbers in controls
  NumberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  // deny to accept special characters like !@#$%^&*()
  DenySpecialCharacters(event) {
    if (/^[a-zA-Z0-9\s]*$/.test(event.key)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }

  }
}
