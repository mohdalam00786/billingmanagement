import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { AdminRoutingModule } from './admin-routing.module';
import { CommonModule } from '@angular/common';
import { UserMasterComponent } from './components/user-master/user-master.component';
import { CentreMasterComponent } from './components/centre-master/centre-master.component';
import { RoleMasterComponent } from './components/role-master/role-master.component';
import { ConfigMasterComponent } from './components/config-master/config-master.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    UserMasterComponent,
    CentreMasterComponent,
    AdminComponent,
    RoleMasterComponent,
    ConfigMasterComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    TranslateModule,
    SharedModule,RouterModule,ReactiveFormsModule

  ],
  exports: [AdminComponent],
  providers: []
})
export class AdminModule {
}