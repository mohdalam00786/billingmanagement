import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { CentreMasterComponent } from './components/centre-master/centre-master.component';
import { ConfigMasterComponent } from './components/config-master/config-master.component';
import { RoleMasterComponent } from './components/role-master/role-master.component';
import { UserMasterComponent } from './components/user-master/user-master.component';

const routes: Routes = [
  {
    path: '', component: AdminComponent
  },
  { path: 'UserMaster', component: UserMasterComponent },
  { path: 'CentreMaster', component: CentreMasterComponent },
  { path: 'RoleMaster', component: RoleMasterComponent },
  { path: 'ConfigMaster', component: ConfigMasterComponent }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
