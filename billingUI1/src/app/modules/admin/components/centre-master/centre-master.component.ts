import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Table } from 'primeng/table';
import { TranslateService } from '@ngx-translate/core';
import { ConfirmationService } from 'primeng/api';
import { Subscription } from 'rxjs';
import { GenericConstants } from 'src/app/modules/shared/constants/generic-constants';
import { CrudImplementation, Status } from 'src/app/modules/shared/constants/generic-numbers';
import { MastersService } from '../../services/masters.service';
import { ExportToExcelService } from 'src/app/modules/shared/services/export-to-excel.service';
import { ExportToPDFService } from 'src/app/modules/shared/services/export-to-pdf.service';
import { ToasterService } from 'src/app/modules/shared/services/toaster.service';
import { SharedService } from 'src/app/modules/shared/services/shared.service';
import { UserService } from 'src/app/modules/shared/services/user.service';
import { Global } from 'src/app/modules/shared/constants/global-config';




@Component({
  selector: 'app-centre-master',
  templateUrl: './centre-master.component.html',
  styleUrls: ['./centre-master.component.scss']
})

export class CentreMasterComponent implements OnInit {

  CentreMasterForm: FormGroup;
  centres: any;
  showCentreMasterForm: boolean = false;
  showAllCentres: boolean = true;
  showAddButton: boolean = true;
  showAllCentreButton: boolean = false;
  rows = GenericConstants.RowsPerPage;
  rowsPerPageOptions = GenericConstants.PagingConfiguration;
  showSaveResetButton: boolean = true;
  requiredMessage: string = '';
  cols: any[];
  exportColumns;
  submitted: boolean = false;
  centreTitle: string;
  subscriptions: Subscription[] = [];

  constructor(private formBuilder: FormBuilder,
    private mastersService: MastersService,
    private translateService: TranslateService,
    private excelService: ExportToExcelService,
    private exportToPDFService: ExportToPDFService,
    private toaster: ToasterService,
    private confirmationService: ConfirmationService,
    private sharedService: SharedService,
    private userService: UserService) { }

  ngOnInit(): void {
    this.CentreMasterForm = this.formBuilder.group(
      {
        CentreId: ['',],
        CentreName: ['', [Validators.required, Validators.minLength(5)]],
        CentreCode: ['',],
        Website: ['',],
        EmailId: ['', Validators.compose([Validators.required, Validators.pattern(GenericConstants.EmailPattern)])],
        MobileNumber: ['', Validators.required],
        LandlineNumber: ['',],
        Address: ['',],
        LoggedInUserId: ['',]
      }
    );
    this.GetAllCentres();
    this.SetTableColumns();
    this.GetTranslation();
    this.exportColumns = this.cols.map(col => ({
      title: col.header,
      dataKey: col.field
    }));
  }
  ngOnDestroy(): void {
    this.subscriptions.forEach(element => {
      element.unsubscribe();
    });
  }
  get centreControls() {
    return this.CentreMasterForm.controls;
  }

  async GetTranslation() {
    this.requiredMessage = await this.translateService.get('RequiredMessage').toPromise();
  }
  SetTableColumns() {
    this.cols = [
      { field: 'centreId', header: 'CentreId', display: 'none' },
      { field: 'centreName', header: 'Centre Name' },
      { field: 'centreCode', header: 'Centre Code' },
      { field: 'mobileNumber', header: 'Mobile Number' },
    ];
  }
  GetAllCentres() {
    this.subscriptions.push(
      this.mastersService.GetAllCentres().subscribe(
        response => {
          switch (response[0].responseStatus) {
            case CrudImplementation.Read:
              this.centres = response;
              break;
            case Status.Error:
              this.ErrorToasterMessage();
              break;
          }

        },
        error => {
          this.ErrorToasterMessage();
          console.log('GetCentreError', error)
        }

      )
    )
  }

  SaveCentreInformation() {
    this.submitted = true;
    if (this.CentreMasterForm.invalid) {
      return;
    }

    var centreDetails =
    {
      CentreId: this.CentreMasterForm.value.CentreId,
      CentreName: this.sharedService.ConvertNullToBlank(this.CentreMasterForm.value.CentreName),
      CentreCode: this.sharedService.ConvertNullToBlank(this.CentreMasterForm.value.CentreCode),
      Website: this.sharedService.ConvertNullToBlank(this.CentreMasterForm.value.Website),
      EmailId: this.sharedService.ConvertNullToBlank(this.CentreMasterForm.value.EmailId),
      MobileNumber: this.sharedService.ConvertNullToBlank(this.CentreMasterForm.value.MobileNumber),
      LandlineNumber: this.sharedService.ConvertNullToBlank(this.CentreMasterForm.value.LandlineNumber),
      Address: this.sharedService.ConvertNullToBlank(this.CentreMasterForm.value.Address),
      LoggedInUserId: this.userService.GetUserId(Global.UserDetails)
    }
    this.subscriptions.push(
      this.mastersService.SaveCentreInformation(centreDetails).subscribe(
        response => {
          if (response.responseStatus) {
            switch (response.responseStatus) {
              case CrudImplementation.Create:
                this.SaveToasterMessage();
                this.ResetCentreMasterForm();
                this.GetAllCentres();
                break;
              case CrudImplementation.Update:
                this.GetAllCentres();
                this.GetAllCentresList();
                this.UpdateToasterMessage();
                break;
              case Status.Error:
                this.ErrorToasterMessage();
                break;
            }

          }
        },
        error => {
          this.ErrorToasterMessage();
          console.log('SaveCentreError', error)
        }

      )
    )
  }
  async DeleteCentreById(centreId) {
    var details=
    {
      DeleteRecordId:centreId,
      LoggedInUserId:this.userService.GetUserId(Global.UserDetails)
    }
    this.subscriptions.push(
      this.mastersService.DeleteCentreById(details).subscribe(
        response => {
          switch (response.responseStatus) {
            case CrudImplementation.Delete:
              this.GetAllCentres();
              this.DeleteToasterMessage();
              break;
            case Status.Error:
              this.ErrorToasterMessage();
              break;
          }
        },
        error => {
          this.ErrorToasterMessage();
          console.log('DeleteCentreError', error)
        }
      )
    )
  }
  ResetCentreMasterForm() {
    this.CentreMasterForm.reset();
    this.submitted = false;
  }

  async GetCentreMasterForm() {
    this.ResetCentreMasterForm();
    this.GetCentreForm();
    this.showSaveResetButton = true;
    this.CentreMasterForm.enable();
    this.centreTitle = await this.translateService.get('AddNewCentre').toPromise();
  }
  GetAllCentresList() {
    this.showCentreMasterForm = false;
    this.showAllCentres = true;
    this.showAddButton = true;
    this.showAllCentreButton = false;
  }
  GetCentreForm() {
    this.showCentreMasterForm = true;
    this.showAllCentres = false;
    this.showAddButton = false;
    this.showAllCentreButton = true;
  }
  async ViewCentreMasterForm(centreInfo) {
    this.GetCentreForm();
    this.SetCentreMasterForm(centreInfo);
    this.CentreMasterForm.disable();
    this.showSaveResetButton = false;
    this.centreTitle = await this.translateService.get('ViewRecord').toPromise();

  }
  async EditCentreMasterForm(centreInfo) {
    this.GetCentreForm();
    this.SetCentreMasterForm(centreInfo);
    this.CentreMasterForm.enable();
    this.showSaveResetButton = true;
    this.centreTitle = await this.translateService.get('UpdateRecord').toPromise();
  }
  SetCentreMasterForm(centreInfo) {
    this.CentreMasterForm.patchValue(
      {
        CentreId: centreInfo.centreId,
        CentreName: centreInfo.centreName,
        CentreCode: centreInfo.centreCode,
        Website: centreInfo.website,
        EmailId: centreInfo.emailId,
        MobileNumber: centreInfo.mobileNumber,
        LandlineNumber: centreInfo.landlineNumber,
        Address: centreInfo.address
      }
    );
  }


  async DeleteToasterMessage() {
    let title = await this.translateService.get('ToasterSuccess').toPromise();
    let message = await this.translateService.get('RecordDeleted').toPromise();
    this.toaster.SuccessToaster(message, title);
  }

  async SaveToasterMessage() {
    let title = await this.translateService.get('ToasterSuccess').toPromise();
    let message = await this.translateService.get('RecordSaved').toPromise();
    this.toaster.SuccessToaster(message, title);
  }
  async UpdateToasterMessage() {
    let title = await this.translateService.get('ToasterSuccess').toPromise();
    let message = await this.translateService.get('RecordUpdated').toPromise();
    this.toaster.SuccessToaster(message, title);
  }
  async ErrorToasterMessage() {
    let title = await this.translateService.get('ToasterError').toPromise();
    let message = await this.translateService.get('ToasterErrorMessage').toPromise();
    this.toaster.ErrorToaster(message, title);
  }

  ExportExcelVisibleData(table: Table) {
    let excelData: any = [];
    if (table && !!table.filteredValue) {
      excelData = table.filteredValue;
    }
    else {
      excelData = table._value;
    }
    if (excelData && excelData.length > 0) {
      this.CreateExcel(excelData);
    }
    else {
      this.NoRecordInfo();
    }
  }

  async CreateExcel(excelData: any) {
    let objData: any = [];
    let dataForExcel: any = [];
    excelData.forEach((value: any) => {
      objData.push({
        "Centre Name": value.centreName, "Centre Code": value.centreCode, "Email Id": value.emailId, "Mobile Number": value.mobileNumber,
        "Landline Number": value.landlineNumber, "Website": value.website, "Address": value.address
      })
    })
    objData.forEach((row: any) => {
      dataForExcel.push(Object.values(row));
    })
    let fileName = await this.translateService.get('AllCentres').toPromise();
    this.excelService.GenerateExcel(dataForExcel, Object.keys(objData[0]), fileName);
    let message = await this.translateService.get('ExcelGenerateSuccess').toPromise();
    let title = await this.translateService.get('ToasterSuccess').toPromise();
    this.toaster.SuccessToaster(message, title);
  }
  ExportPDFVisibleData(table: Table) {
    let excelData: any = [];
    if (table && !!table.filteredValue) {
      excelData = table.filteredValue;
    }
    else {
      excelData = table._value;
    }
    if (excelData && excelData.length > 0) {
      this.CreatePDF(excelData);
    }
    else {
      this.NoRecordInfo();
    }
  }
  async CreatePDF(pdfData) {
    let objData: any = [];
    let dataForPDF: any = [];
    pdfData.forEach((value: any) => {
      objData.push({
        "Centre Name": value.centreName, "Centre Code": value.centreCode, "Email Id": value.emailId, "Mobile Number": value.mobileNumber,
        "Landline Number": value.landlineNumber, "Website": value.website, "Address": value.address
      })
    })
    objData.forEach((row: any) => {
      dataForPDF.push(Object.values(row));
    })
    let fileName = await this.translateService.get('AllCentres').toPromise();

    this.exportToPDFService.GeneratePDF(Object.keys(objData[0]), dataForPDF, fileName);
    let message = await this.translateService.get('PdfGenerateSuccess').toPromise();
    let title = await this.translateService.get('ToasterSuccess').toPromise();
    this.toaster.SuccessToaster(message, title);
  }
  async NoRecordInfo() {
    let message = await this.translateService.get('NoRecordFound').toPromise();
    let title = await this.translateService.get('ToasterInfo').toPromise();
    this.toaster.ShowToasterInfo(message, title);
  }


  async DeleteCentreConfirmation(centreId: any) {
    let deleteConfirmHeader = await this.translateService.get('DeleteConfirmHeader').toPromise();
    let deleteConfirmMessage = await this.translateService.get('DeleteConfirmMessage').toPromise();
    this.confirmationService.confirm({
      message: deleteConfirmMessage,
      header: deleteConfirmHeader,
      icon: GenericConstants.DeleteConfirmationIcon,
      accept: () => {
        this.DeleteCentreById(centreId);
      },
      reject: () => {

      }
    });
  }

  HandleFirstPlaceSpace(event) {
    this.sharedService.HandleFirstPlaceSpace(event);
  }
  DenySpecialChar(event) {
    this.sharedService.DenySpecialCharacters(event);
  }
  NumberOnly(event) {
    this.sharedService.NumberOnly(event);
  }
}
