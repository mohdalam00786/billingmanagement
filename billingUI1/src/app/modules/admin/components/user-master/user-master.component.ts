import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { SelectItem } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';
import { Table } from 'primeng/table';
import { Subscription } from 'rxjs';
import { GenericConstants } from 'src/app/modules/shared/constants/generic-constants';
import { CrudImplementation, Status } from 'src/app/modules/shared/constants/generic-numbers';
import { Global } from 'src/app/modules/shared/constants/global-config';
import { ExportToExcelService } from 'src/app/modules/shared/services/export-to-excel.service';
import { ExportToPDFService } from 'src/app/modules/shared/services/export-to-pdf.service';
import { SharedService } from 'src/app/modules/shared/services/shared.service';
import { ToasterService } from 'src/app/modules/shared/services/toaster.service';
import { UserService } from 'src/app/modules/shared/services/user.service';
import { UsersService } from '../../services/users.service';



@Component({
  selector: 'app-user-master',
  templateUrl: './user-master.component.html',
  styleUrls: ['./user-master.component.scss']
})

export class UserMasterComponent implements OnInit {

  UserInformationForm: FormGroup;

  showAddButton: boolean = true;
  showAllUsersButton: boolean = false;
  showUsersMasterForm: boolean = false;
  showAllUsers: boolean = true;
  showSaveResetButton: boolean = true;
  allUsers: any;
  BloodGroupList!: SelectItem[];
  GenderList!: SelectItem[];
  MaritalStatusList!: SelectItem[];
  roleList!: Roles[];
  centreList!: Centres[];
  selectedCentreList: CheckedArray[];
  selectedRoleList: CheckedArray[];

  cols: any[];
  requiredMessage: string = '';
  submitted: boolean = false;
  userTitle: string;
  subscriptions: Subscription[] = [];
  checkedCentreArray: any = [];
  checkedRoleArray: any = [];
  isChecked: boolean;
  minDate: Date;
  maxDate: Date;
  rowsPerPageOptions = GenericConstants.PagingConfiguration;
  rows = GenericConstants.RowsPerPage;
  oldDateOfBirth: any;

  constructor(private formBuilder: FormBuilder,
    private usersService: UsersService,
    private translateService: TranslateService,
    private datePipe: DatePipe,
    private toaster: ToasterService,
    private confirmationService: ConfirmationService,
    private excelService: ExportToExcelService,
    private exportToPDFService: ExportToPDFService,
    private sharedService: SharedService,
    private userService: UserService) {
    this.maxDate = new Date();

  }

  ngOnInit(): void {
    this.UserCompleteInformationForm();
    this.SetTableColumns()
    this.GetAllUsers();
    this.GetTranslation();
    this.GetUserMasterData();
  }
  ngOnDestroy(): void {
    this.subscriptions.forEach(element => {
      element.unsubscribe();
    });
  }

  UserCompleteInformationForm() {
    this.UserInformationForm = this.formBuilder.group(
      {
        UserId: [''],
        UserName: ['', Validators.required],
        DateOfBirth: ['', Validators.required],
        Gender: ['', Validators.required],
        BloodGroup: ['',],
        MaritalStatus: ['', Validators.required],
        SpouseName: ['',],
        FatherName: ['',],
        MotherName: ['',],
        GuardianName: ['',],
        PassportNumber: ['',],
        AdhaarCardNumber: ['',],
        PANCardNumber: ['',],
        PrimaryContactNumber: ['', Validators.required],
        SecondryContactNumber: ['',],
        LandlineNumber: ['',],
        EmergencyContactNumber: ['', Validators.required],
        EmergencyContactName: ['', Validators.required],
        EmailId: ['',],
        PermanentAddress: ['',],
        LoginName: ['', Validators.required],
        Password: ['', Validators.required],
        PasswordConfirm: ['', Validators.required],
        Roles: this.formBuilder.array([], Validators.required),
        Centres: this.formBuilder.array([], Validators.required),
        LoggedInUserId: ['',]
      }
    );
  }



  get userInfoControls() {
    return this.UserInformationForm.controls;
  }
  SetTableColumns() {
    this.cols = [
      { field: 'userId', header: 'UserId', display: 'none' },
      { field: 'userName', header: 'User Name' },
      { field: 'dateOfBirth', header: 'Date Of Birth' },
      { field: 'gender', header: 'Gender' },
      { field: 'fatherName', header: 'Father Name' },
      { field: 'primaryContactNumber', header: 'Primary Contact Number' },
    ];
  }
  async GetTranslation() {
    this.requiredMessage = await this.translateService.get('RequiredMessage').toPromise();

  }

  ResetUserMasterForm() {
    this.UserInformationForm.reset();
    this.submitted = false;
  }
  async GetUserMasterForm() {
    this.ResetUserMasterForm();
    this.GetUsersForm();
    this.UserInformationForm.enable();
    this.showSaveResetButton = true;
    this.userTitle = await this.translateService.get('AddNewUser').toPromise();
    this.ResetCheckedCentres();
    this.ResetCheckedRoles();
  }
  GetUsersForm() {
    this.showUsersMasterForm = true;
    this.showAllUsers = false;
    this.showAddButton = false;
    this.showAllUsersButton = true;
  }
  GetAllUsersList() {
    this.showUsersMasterForm = false;
    this.showAllUsers = true;
    this.showAddButton = true;
    this.showAllUsersButton = false;
  }
  SetUserMasterForm(userInfo) {
    this.UserInformationForm.patchValue(
      {
        UserId: userInfo.userId,
        UserName: userInfo.userName,
        DateOfBirth: userInfo.dateOfBirth,
        Gender: userInfo.gender,
        BloodGroup: userInfo.bloodGroup,
        MaritalStatus: userInfo.maritalStatus,
        SpouseName: userInfo.spouseName || '',
        FatherName: userInfo.fatherName,
        MotherName: userInfo.motherName,
        GuardianName: userInfo.guardianName,
        PassportNumber: userInfo.passportNumber,
        AdhaarCardNumber: userInfo.adhaarCardNumber,
        PANCardNumber: userInfo.panCardNumber,
        PrimaryContactNumber: userInfo.primaryContactNumber,
        SecondryContactNumber: userInfo.secondryContactNumber,
        LandlineNumber: userInfo.landlineNumber,
        EmergencyContactName: userInfo.emergencyContactName,
        EmergencyContactNumber: userInfo.emergencyContactNumber,
        EmailId: userInfo.emailId,
        PermanentAddress: userInfo.permanentAddress,
        LoginName: userInfo.loginName,
        Password: userInfo.password,
        PasswordConfirm: userInfo.password
      }
    );
    //centre
    const centreId = userInfo.centres.split(',').map((tag) => tag.trim()).filter((tag) => tag.length !== 0);
    this.selectedCentreList = [];
    centreId.forEach(item => {
      this.selectedCentreList.push({ Id: item });
    });
    this.ResetCheckedCentres();
    this.centreList.filter(x => this.selectedCentreList.some(y => y.Id.includes(x.Id))).forEach(item => {
      this.checkedCentreArray.push(new FormControl(item.Id));
      item.checked = true;
    });
    //role
    const roleId = userInfo.roles.split(',').map((tag) => tag.trim()).filter((tag) => tag.length !== 0);
    this.selectedRoleList = [];
    roleId.forEach(item => {
      this.selectedRoleList.push({ Id: item });
    });
    this.ResetCheckedRoles();
    this.roleList.filter(x => this.selectedRoleList.some(y => y.Id.includes(x.Id))).forEach(item => {
      this.checkedRoleArray.push(new FormControl(item.Id));
      item.checked = true;
    });
  }
  GetCheckedCentres(id, isChecked, key) {

    this.checkedCentreArray = <FormArray>this.UserInformationForm.get(key);
    if (isChecked) {
      this.checkedCentreArray.push(new FormControl(id));
    } else {
      let idx = this.checkedCentreArray.controls.findIndex(x => x.value == id);
      this.checkedCentreArray.removeAt(idx);
    }
  }
  GetCheckedRoles(id, isChecked, key) {
    this.checkedRoleArray = <FormArray>this.UserInformationForm.get(key);
    if (isChecked) {
      this.checkedRoleArray.push(new FormControl(id));
    } else {
      let idx = this.checkedRoleArray.controls.findIndex(x => x.value == id);
      this.checkedRoleArray.removeAt(idx);
    }
  }
  ResetCheckedCentres() {
    this.centreList.forEach(item => {
      item.checked = false;
    });
    this.checkedCentreArray = <FormArray>this.UserInformationForm.get('Centres');
    this.checkedCentreArray.controls = [];
    this.checkedCentreArray.value = [];
  }
  ResetCheckedRoles() {
    this.roleList.forEach(item => {
      item.checked = false;
    });
    this.checkedRoleArray = <FormArray>this.UserInformationForm.get('Roles');
    this.checkedRoleArray.controls = [];
    this.checkedRoleArray.value = [];
  }

  async ViewUserMasterForm(userInfo) {
    this.GetUsersForm();
    this.SetUserMasterForm(userInfo);
    this.showSaveResetButton = false;
    this.UserInformationForm.disable();
    this.userTitle = await this.translateService.get('ViewRecord').toPromise();
  }
  async EditUserMasterForm(userInfo) {
    this.GetUsersForm();
    this.SetUserMasterForm(userInfo);
    this.showSaveResetButton = true;
    this.UserInformationForm.enable();
    this.oldDateOfBirth = userInfo.dateOfBirth;

    this.userTitle = await this.translateService.get('UpdateRecord').toPromise();
  }

  DeleteUserById(userId) {
    var details =
    {
      DeleteRecordId: userId,
      LoggedInUserId: this.userService.GetUserId(Global.UserDetails)
    }
    this.subscriptions.push(

      this.usersService.DeleteUserById(details).subscribe(
        response => {
          switch (response.responseStatus) {
            case CrudImplementation.Delete:
              this.GetAllUsers();
              this.DeleteToasterMessage();
              break;
            case Status.Error:
              this.ErrorToasterMessage();
              break;
          }
        },
        error => {
          this.ErrorToasterMessage();
          console.log('DeleteUserError', error)
        }
      )
    )
  }
  GetAllUsers() {
    this.subscriptions.push(
      this.usersService.GetAllUsers().subscribe(
        response => {
          this.allUsers = response.usersList;
          this.allUsers.forEach(element => {
            if (element.dateOfBirth) {
              element.dateOfBirth = this.datePipe.transform(element.dateOfBirth, GenericConstants.DateFormat);
            }            
          });
        },
        error => {
          this.ErrorToasterMessage();
          console.log('GetUsersError', error)
        }
      )
    )
  }

  GetUserMasterData() {
    this.subscriptions.push(
      this.usersService.GetUserMasterData().subscribe(
        response => {
          this.GenderList = [];
          this.GenderList.push({ 'label': "", value: GenericConstants.DefaultSelect })
          response.genderList.forEach((item: { genderCode: any; gender: any; }) => {
            this.GenderList.push({ 'label': item.genderCode, value: item.gender });
          });
          this.MaritalStatusList = [];
          this.MaritalStatusList.push({ 'label': "", value: GenericConstants.DefaultSelect })
          response.maritalStatusList.forEach((item: { maritalStatusCode: any; maritalStatus: any; }) => {
            this.MaritalStatusList.push({ 'label': item.maritalStatusCode, value: item.maritalStatus });
          });
          this.BloodGroupList = [];
          this.BloodGroupList.push({ 'label': "", value: GenericConstants.DefaultSelect })
          response.bloodGroupList.forEach((item: { bloodGroupCode: any; bloodGroup: any; }) => {
            this.BloodGroupList.push({ 'label': item.bloodGroupCode, value: item.bloodGroup });
          });
          this.roleList = [];
          response.roleList.forEach((item: { roleCode: any; roleName: any; IsChecked }) => {
            this.roleList.push({ Id: item.roleCode, name: item.roleName, checked: false });
          });
          this.centreList = [];
          response.centreList.forEach((item: { centreId: any; centreName: any; IsChecked }) => {
            this.centreList.push({ Id: item.centreId, name: item.centreName, checked: false });
          });

        },
        error => {
          console.log('GetUserMasterError', error)
        }

      )
    )
  }
  SaveUserPersonalInfo() {
    this.submitted = true;
    if (this.UserInformationForm.invalid) {
      return;
    }
    var userDetails =
    {
      UserId: this.UserInformationForm.value.UserId,
      UserName: this.sharedService.ConvertNullToBlank(this.UserInformationForm.value.UserName),
      DateOfBirth: this.UserInformationForm.value.DateOfBirth,
      Gender: this.UserInformationForm.value.Gender,
      BloodGroup: this.UserInformationForm.value.BloodGroup,
      MaritalStatus: this.UserInformationForm.value.MaritalStatus,
      SpouseName: this.sharedService.ConvertNullToBlank(this.UserInformationForm.value.SpouseName),
      FatherName: this.sharedService.ConvertNullToBlank(this.UserInformationForm.value.FatherName),
      MotherName: this.sharedService.ConvertNullToBlank(this.UserInformationForm.value.MotherName),
      GuardianName: this.sharedService.ConvertNullToBlank(this.UserInformationForm.value.GuardianName),
      PassportNumber: this.sharedService.ConvertNullToBlank(this.UserInformationForm.value.PassportNumber),
      AdhaarCardNumber: this.sharedService.ConvertNullToBlank(this.UserInformationForm.value.AdhaarCardNumber),
      PANCardNumber: this.sharedService.ConvertNullToBlank(this.UserInformationForm.value.PanCardNumber),
      PrimaryContactNumber: this.sharedService.ConvertNullToBlank(this.UserInformationForm.value.PrimaryContactNumber),
      SecondryContactNumber: this.sharedService.ConvertNullToBlank(this.UserInformationForm.value.SecondryContactNumber),
      LandlineNumber: this.sharedService.ConvertNullToBlank(this.UserInformationForm.value.LandlineNumber),
      EmergencyContactName: this.sharedService.ConvertNullToBlank(this.UserInformationForm.value.EmergencyContactName),
      EmergencyContactNumber: this.sharedService.ConvertNullToBlank(this.UserInformationForm.value.EmergencyContactNumber),
      EmailId: this.sharedService.ConvertNullToBlank(this.UserInformationForm.value.EmailId),
      PermanentAddress: this.sharedService.ConvertNullToBlank(this.UserInformationForm.value.PermanentAddress),
      LoginName: this.sharedService.ConvertNullToBlank(this.UserInformationForm.value.LoginName.trim()),
      Password: this.sharedService.ConvertNullToBlank(this.UserInformationForm.value.Password.trim()),
      PasswordConfirm: this.sharedService.ConvertNullToBlank(this.UserInformationForm.value.PasswordConfirm),
      Centres: this.UserInformationForm.value.Centres,
      Roles: this.UserInformationForm.value.Roles,
      LoggedInUserId: this.userService.GetUserId(Global.UserDetails)
    }

    if (userDetails.UserId == null) {
      userDetails.DateOfBirth = this.datePipe.transform(userDetails.DateOfBirth, GenericConstants.DateFormat);
    }
    else if ((userDetails.UserId != null) && userDetails.DateOfBirth != this.oldDateOfBirth) {
      userDetails.DateOfBirth = this.datePipe.transform(userDetails.DateOfBirth, GenericConstants.DateFormat);
    }
    this.subscriptions.push(
      this.usersService.SaveUserInformation(userDetails).subscribe(
        response => {
          if (response.responseStatus) {
            switch (response.responseStatus) {
              case CrudImplementation.Create:
                this.SaveToasterMessage();
                this.ResetUserMasterForm();
                this.GetAllUsers();
                this.ResetCheckedCentres();
                this.ResetCheckedRoles();
                break;
              case CrudImplementation.Update:
                this.GetAllUsersList();
                this.UpdateToasterMessage();
                this.GetAllUsers();
                break;
              case Status.Error:
                this.ErrorToasterMessage();
                break;
            }

          }
        },
        error => {
          this.ErrorToasterMessage();
          console.log('saveInfoError', error)
        }
      )
    )
  }

  async DeleteToasterMessage() {
    let title = await this.translateService.get('ToasterSuccess').toPromise();
    let message = await this.translateService.get('RecordDeleted').toPromise();
    this.toaster.SuccessToaster(message, title);
  }

  async SaveToasterMessage() {
    let title = await this.translateService.get('ToasterSuccess').toPromise();
    let message = await this.translateService.get('RecordSaved').toPromise();
    this.toaster.SuccessToaster(message, title);
  }
  async UpdateToasterMessage() {
    let title = await this.translateService.get('ToasterSuccess').toPromise();
    let message = await this.translateService.get('RecordUpdated').toPromise();
    this.toaster.SuccessToaster(message, title);
  }
  async ErrorToasterMessage() {
    let title = await this.translateService.get('ToasterError').toPromise();
    let message = await this.translateService.get('ToasterErrorMessage').toPromise();
    this.toaster.ErrorToaster(message, title);
  }

  async DeleteUserConfirmation(userId: any) {
    let deleteConfirmHeader = await this.translateService.get('DeleteConfirmHeader').toPromise();
    let deleteConfirmMessage = await this.translateService.get('DeleteConfirmMessage').toPromise();
    this.confirmationService.confirm({
      message: deleteConfirmMessage,
      header: deleteConfirmHeader,
      icon: GenericConstants.DeleteConfirmationIcon,
      accept: () => {
        this.DeleteUserById(userId);
      },
      reject: () => {

      }
    });
  }


  ExportExcelVisibleData(table: Table) {
    let excelData: any = [];
    if (table && !!table.filteredValue) {
      excelData = table.filteredValue;
    }
    else {
      excelData = table._value;
    }
    if (excelData && excelData.length > 0) {
      this.CreateExcel(excelData);
    }
    else {
      this.NoRecordInfo();
    }
  }

  async CreateExcel(excelData: any) {
    let objData: any = [];
    let dataForExcel: any = [];
    excelData.forEach((value: any) => {
      objData.push({
        "UserName": value.userName, "DateOfBirth": value.dateOfBirth,
        "SpouseName": value.spouseName,
        "FatherName": value.fatherName, "MotherName": value.motherName, "PassportNumber": value.passportNumber,
        "AdhaarCardNumber": value.adhaarCardNumber, "PANCardNumber": value.pANCardNumber, "PrimaryContactNumber": value.primaryContactNumber,
        "EmergencyContactNumber": value.emergencyContactNumber, "EmergencyContactName": value.emergencyContactName, "PermanentAddress": value.permanentAddress
      })
    })
    objData.forEach((row: any) => {
      dataForExcel.push(Object.values(row));
    })
    let fileName = await this.translateService.get('AllUsers').toPromise();
    this.excelService.GenerateExcel(dataForExcel, Object.keys(objData[0]), fileName);
    let message = await this.translateService.get('ExcelGenerateSuccess').toPromise();
    let title = await this.translateService.get('ToasterSuccess').toPromise();
    this.toaster.SuccessToaster(message, title);
  }
  async ExportPDFVisibleData(table: Table) {
    let excelData: any = [];
    if (table && !!table.filteredValue) {
      excelData = table.filteredValue;
    }
    else {
      excelData = table._value;
    }
    if (excelData && excelData.length > 0) {
      this.CreatePDF(excelData);
    }
    else {
      this.NoRecordInfo();
    }
  }
  async CreatePDF(pdfData) {
    let objData: any = [];
    let dataForPDF: any = [];
    pdfData.forEach((value: any) => {
      objData.push({
        "User": value.userName, "DOB": value.dateOfBirth,
        "Spouse": value.spouseName,
        "Father": value.fatherName, "Mother": value.motherName, "Passport": value.passportNumber,
        "Adhaar": value.adhaarCardNumber, "PAN": value.pANCardNumber, "Mobile": value.primaryContactNumber,
        "Address": value.permanentAddress
      })
    })
    objData.forEach((row: any) => {
      dataForPDF.push(Object.values(row));
    })
    let fileName = await this.translateService.get('AllUsers').toPromise();
    this.exportToPDFService.GeneratePDF(Object.keys(objData[0]), dataForPDF, fileName);
    let message = await this.translateService.get('PdfGenerateSuccess').toPromise();
    let title = await this.translateService.get('ToasterSuccess').toPromise();
    this.toaster.SuccessToaster(message, title);
  }

  async NoRecordInfo() {
    let message = await this.translateService.get('NoRecordFound').toPromise();
    let title = await this.translateService.get('ToasterInfo').toPromise();
    this.toaster.ShowToasterInfo(message, title);
  }
  HandleFirstPlaceSpace(event) {
    this.sharedService.HandleFirstPlaceSpace(event);
  }
  DenySpecialChar(event) {
    this.sharedService.DenySpecialCharacters(event);
  }
  NumberOnly(event) {
    this.sharedService.NumberOnly(event);
  }
}

export class Centres {
  public Id: string
  public name: string
  public checked: boolean
}
export class CheckedArray {
  public Id: string
}

export class Roles {
  public Id: string
  public name: string
  public checked: boolean
}



