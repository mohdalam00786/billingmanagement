import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Table } from 'primeng/table';
import { TranslateService } from '@ngx-translate/core';
import { ConfirmationService } from 'primeng/api';
import { Subscription } from 'rxjs';
import { GenericConstants } from 'src/app/modules/shared/constants/generic-constants';
import { CrudImplementation, Status } from 'src/app/modules/shared/constants/generic-numbers';
import { MastersService } from '../../services/masters.service';
import { ExportToExcelService } from 'src/app/modules/shared/services/export-to-excel.service';
import { ExportToPDFService } from 'src/app/modules/shared/services/export-to-pdf.service';
import { ToasterService } from 'src/app/modules/shared/services/toaster.service';
import { SharedService } from 'src/app/modules/shared/services/shared.service';
import { Global } from 'src/app/modules/shared/constants/global-config';
import { UserService } from 'src/app/modules/shared/services/user.service';
@Component({
  selector: 'app-config-master',
  templateUrl: './config-master.component.html',
  styleUrls: ['./config-master.component.scss']
})
export class ConfigMasterComponent implements OnInit {
  ConfigMasterForm: FormGroup;
  configs: any;
  showConfigMasterForm: boolean = false;
  showAllConfigs: boolean = true;
  showAddButton: boolean = true;
  showAllConfigButton: boolean = false;
  rows = GenericConstants.RowsPerPage;
  rowsPerPageOptions = GenericConstants.PagingConfiguration;
  totalRecords: number;
  showSaveResetButton: boolean = true;
  requiredMessage: string = '';
  cols: any[];
  exportColumns;
  submitted: boolean = false;
  configTitle: string;
  subscriptions: Subscription[] = [];

  //selectedConfig: string;

  //distinctConfig: any[];

  constructor(private formBuilder: FormBuilder,
    private mastersService: MastersService,
    private translateService: TranslateService,
    private excelService: ExportToExcelService,
    private exportToPDFService: ExportToPDFService,
    private toaster: ToasterService,
    private confirmationService: ConfirmationService,
    private sharedService: SharedService,
    private userService: UserService) {
  }

  ngOnInit(): void {
    this.ConfigMasterForm = this.formBuilder.group(
      {
        ConfigId: ['',],
        ConfigKey: ['', [Validators.required, Validators.minLength(2)]],
        ConfigValue: ['', [Validators.required, Validators.minLength(2)]],
        LoggedInUserId: ['',]
      }
    );
    this.GetAllConfig();
    this.SetTableColumns();
    this.GetTranslation();
    this.exportColumns = this.cols.map(col => ({
      title: col.header,
      dataKey: col.field
    }));
  }
  ngOnDestroy(): void {
    this.subscriptions.forEach(element => {
      element.unsubscribe();
    });
  }
  get configControls() {
    return this.ConfigMasterForm.controls;
  }

  async GetTranslation() {
    this.requiredMessage = await this.translateService.get('RequiredMessage').toPromise();
  }
  SetTableColumns() {
    this.cols = [
      { field: 'ConfigId', header: 'ConfigId', display: 'none' },
      { field: 'configKey', header: 'Config Key' },
      { field: 'configValue', header: 'Config Value' },
    ];
  }
  GetAllConfig() {
    this.subscriptions.push(
      this.mastersService.GetAllConfig().subscribe(
        response => {
          switch (response[0].responseStatus) {
            case CrudImplementation.Read:
              this.configs = response;
              //  this.distinctConfig = response.filter((a, i) => response.findIndex((s) => a.configKey === s.configKey) === i);
              break;
            case Status.Error:
              this.ErrorToasterMessage();
              break;
          }

        },
        error => {
          this.ErrorToasterMessage();
          console.log('GetconfigError', error)
        }

      )
    )
  }
  SaveConfigInformation() {

    this.submitted = true;
    if (this.ConfigMasterForm.invalid) {
      return;
    }

    var configDetails =
    {
      ConfigId: this.ConfigMasterForm.value.ConfigId,
      ConfigKey: this.sharedService.ConvertNullToBlank(this.ConfigMasterForm.value.ConfigKey),
      ConfigValue: this.sharedService.ConvertNullToBlank(this.ConfigMasterForm.value.ConfigValue),
      LoggedInUserId: this.userService.GetUserId(Global.UserDetails)
    }

    this.subscriptions.push(
      this.mastersService.SaveConfigInformation(configDetails).subscribe(
        response => {
          if (response.responseStatus) {
            switch (response.responseStatus) {
              case CrudImplementation.Create:
                this.SaveToasterMessage();
                this.ResetConfigMasterForm();
                this.GetAllConfig();
                break;
              case CrudImplementation.Update:
                this.GetAllConfig();
                this.GetAllConfigList();
                this.UpdateToasterMessage();
                break;
              case Status.AlreadyExist:
                this.RecordAlreadyExist();
                break;
              case Status.Error:
                this.ErrorToasterMessage();
                break;
            }

          }
        },
        error => {
          this.ErrorToasterMessage();
          console.log('SaveConfigError', error)
        }

      )
    )
  }
  async DeleteConfigById(configId) {
    var details =
    {
      DeleteRecordId: configId,
      LoggedInUserId: this.userService.GetUserId(Global.UserDetails)
    }
    this.subscriptions.push(
      this.mastersService.DeleteConfigById(details).subscribe(
        response => {
          switch (response.responseStatus) {
            case CrudImplementation.Delete:
              this.GetAllConfig();
              this.DeleteToasterMessage();
              break;
            case Status.Error:
              this.ErrorToasterMessage();
              break;
          }
        },
        error => {
          this.ErrorToasterMessage();
          console.log('DeleteConfigError', error)
        }
      )
    )
  }
  ResetConfigMasterForm() {
    this.ConfigMasterForm.reset();
    this.submitted = false;
  }

  async GetConfigMasterForm() {
    this.ResetConfigMasterForm();
    this.GetConfigForm();
    this.ConfigMasterForm.enable();
    this.showSaveResetButton = true;
    //this.selectedConfig = '';
    this.configTitle = await this.translateService.get('AddNewConfig').toPromise();
  }
  GetAllConfigList() {
    this.showConfigMasterForm = false;
    this.showAllConfigs = true;
    this.showAddButton = true;
    this.showAllConfigButton = false;
  }
  GetConfigForm() {
    this.showConfigMasterForm = true;
    this.showAllConfigs = false;
    this.showAddButton = false;
    this.showAllConfigButton = true;
  }
  async ViewConfigMasterForm(configInfo) {
    this.GetConfigForm();
    this.SetConfigMasterForm(configInfo);
    this.ConfigMasterForm.disable();
    this.showSaveResetButton = false;
    this.configTitle = await this.translateService.get('ViewRecord').toPromise();
  }
  async EditConfigMasterForm(configInfo) {
    this.GetConfigForm();
    this.SetConfigMasterForm(configInfo);
    this.ConfigMasterForm.controls['ConfigValue'].enable();
    this.ConfigMasterForm.controls['ConfigKey'].disable();
    this.showSaveResetButton = true;
    this.configTitle = await this.translateService.get('UpdateRecord').toPromise();
  }
  SetConfigMasterForm(configInfo) {
    this.ConfigMasterForm.patchValue(
      {
        ConfigId: configInfo.configId,
        ConfigKey: configInfo.configKey,
        ConfigValue: configInfo.configValue,
      }
    );
  }


  async DeleteToasterMessage() {
    let title = await this.translateService.get('ToasterSuccess').toPromise();
    let message = await this.translateService.get('RecordDeleted').toPromise();
    this.toaster.SuccessToaster(message, title);
  }

  async SaveToasterMessage() {
    let title = await this.translateService.get('ToasterSuccess').toPromise();
    let message = await this.translateService.get('RecordSaved').toPromise();
    this.toaster.SuccessToaster(message, title);
  }
  async UpdateToasterMessage() {
    let title = await this.translateService.get('ToasterSuccess').toPromise();
    let message = await this.translateService.get('RecordUpdated').toPromise();
    this.toaster.SuccessToaster(message, title);
  }
  async ErrorToasterMessage() {
    let title = await this.translateService.get('ToasterError').toPromise();
    let message = await this.translateService.get('ToasterErrorMessage').toPromise();
    this.toaster.ErrorToaster(message, title);
  }

  ExportExcelVisibleData(table: Table) {
    let excelData: any = [];
    if (table && !!table.filteredValue) {
      excelData = table.filteredValue;
    }
    else {
      excelData = table._value;
    }
    if (excelData && excelData.length > 0) {
      this.CreateExcel(excelData);
    }
    else {
      this.NoRecordInfo();
    }
  }

  async CreateExcel(excelData: any) {
    let objData: any = [];
    let dataForExcel: any = [];
    excelData.forEach((value: any) => {
      objData.push({
        "Config Key": value.configKey, "Config Value": value.configValue
      })
    })
    objData.forEach((row: any) => {
      dataForExcel.push(Object.values(row));
    })
    let fileName = await this.translateService.get('AllConfigs').toPromise();
    this.excelService.GenerateExcel(dataForExcel, Object.keys(objData[0]), fileName);
    let message = await this.translateService.get('ExcelGenerateSuccess').toPromise();
    let title = await this.translateService.get('ToasterSuccess').toPromise();
    this.toaster.SuccessToaster(message, title);
  }
  ExportPDFVisibleData(table: Table) {
    let excelData: any = [];
    if (table && !!table.filteredValue) {
      excelData = table.filteredValue;
    }
    else {
      excelData = table._value;
    }
    if (excelData && excelData.length > 0) {
      this.CreatePDF(excelData);
    }
    else {
      this.NoRecordInfo();
    }
  }
  async CreatePDF(pdfData) {
    let objData: any = [];
    let dataForPDF: any = [];
    pdfData.forEach((value: any) => {
      objData.push({
        "Config Key": value.configKey, "Config Value": value.configValue
      })
    })
    objData.forEach((row: any) => {
      dataForPDF.push(Object.values(row));
    })
    let fileName = await this.translateService.get('AllConfigs').toPromise();
    this.exportToPDFService.GeneratePDF(Object.keys(objData[0]), dataForPDF, fileName);
    let message = await this.translateService.get('PdfGenerateSuccess').toPromise();
    let title = await this.translateService.get('ToasterSuccess').toPromise();
    this.toaster.SuccessToaster(message, title);
  }
  async NoRecordInfo() {
    let message = await this.translateService.get('NoRecordFound').toPromise();
    let title = await this.translateService.get('ToasterInfo').toPromise();
    this.toaster.ShowToasterInfo(message, title);
  }
  async RecordAlreadyExist() {
    let message = await this.translateService.get('NoRecordFound').toPromise();
    let title = await this.translateService.get('ToasterInfo').toPromise();
    this.toaster.ShowToasterInfo(message, title);
  }

  async DeleteConfigConfirmation(configId: any) {
    let deleteConfirmHeader = await this.translateService.get('DeleteConfirmHeader').toPromise();
    let deleteConfirmMessage = await this.translateService.get('DeleteConfirmMessage').toPromise();
    this.confirmationService.confirm({
      message: deleteConfirmMessage,
      header: deleteConfirmHeader,
      icon: GenericConstants.DeleteConfirmationIcon,
      accept: () => {
        this.DeleteConfigById(configId);
      },
      reject: () => {

      }
    });
  }
  HandleFirstPlaceSpace(event) {
    this.sharedService.HandleFirstPlaceSpace(event);
  }
  DenySpecialChar(event) {
    this.sharedService.DenySpecialCharacters(event);
  }
}

