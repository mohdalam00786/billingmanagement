import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Table } from 'primeng/table';
import { TranslateService } from '@ngx-translate/core';
import { ConfirmationService } from 'primeng/api';
import { Subscription } from 'rxjs';
import { GenericConstants } from 'src/app/modules/shared/constants/generic-constants';
import { CrudImplementation, Status } from 'src/app/modules/shared/constants/generic-numbers';
import { MastersService } from '../../services/masters.service';
import { ExportToExcelService } from 'src/app/modules/shared/services/export-to-excel.service';
import { ExportToPDFService } from 'src/app/modules/shared/services/export-to-pdf.service';
import { ToasterService } from 'src/app/modules/shared/services/toaster.service';
import { SharedService } from 'src/app/modules/shared/services/shared.service';
import { UserService } from 'src/app/modules/shared/services/user.service';
import { Global } from 'src/app/modules/shared/constants/global-config';
@Component({
  selector: 'app-role-master',
  templateUrl: './role-master.component.html',
  styleUrls: ['./role-master.component.scss']
})
export class RoleMasterComponent implements OnInit {
  RoleMasterForm: FormGroup;
  roles: any;
  showRoleMasterForm: boolean = false;
  showAllRoles: boolean = true;
  showAddButton: boolean = true;
  showAllRoleButton: boolean = false;
  rows = GenericConstants.RowsPerPage;
  rowsPerPageOptions=GenericConstants.PagingConfiguration;
  totalRecords: number;
  showSaveResetButton: boolean = true;
  requiredMessage: string = '';
  cols: any[];
  exportColumns;
  submitted: boolean = false;
  roleTitle: string;
  subscriptions: Subscription[] = [];

  constructor(private formBuilder: FormBuilder,
    private mastersService: MastersService,
    private translateService: TranslateService,
    private excelService: ExportToExcelService,
    private exportToPDFService: ExportToPDFService,
    private toaster: ToasterService,
    private confirmationService: ConfirmationService,
    private sharedService:SharedService,
    private userService:UserService) { }

  ngOnInit(): void {
    this.RoleMasterForm = this.formBuilder.group(
      {
        RoleId: ['',],
        RoleName: ['', [Validators.required, Validators.minLength(5)]],
        RoleDescription: ['',],
        LoggedInUserId: ['',]
      }
    );
    this.GetAllRoles();
    this.SetTableColumns();
    this.GetTranslation();
    this.exportColumns = this.cols.map(col => ({
      title: col.header,
      dataKey: col.field
    }));
  }
  ngOnDestroy(): void {
    this.subscriptions.forEach(element => {
      element.unsubscribe();
    });
  }
  get roleControls() {
    return this.RoleMasterForm.controls;
  }

  async GetTranslation() {
    this.requiredMessage = await this.translateService.get('RequiredMessage').toPromise();
  }
  SetTableColumns() {
    this.cols = [
      { field: 'RoleId', header: 'RoleId', display: 'none' },
      { field: 'roleName', header: 'Role Name' },
      { field: 'roleDescription', header: 'Role Description' },
    ];
  }
  GetAllRoles() {
    this.subscriptions.push(
      this.mastersService.GetAllRoles().subscribe(
        response => {
          switch (response[0].responseStatus) {
            case CrudImplementation.Read:
              this.roles = response;
              break;
            case Status.Error:
              this.ErrorToasterMessage();
              break;
          }

        },
        error => {
          this.ErrorToasterMessage();
          console.log('GetRoleError', error)
        }

      )
    )
  }
  SaveRoleInformation() {
    this.submitted = true;
    if (this.RoleMasterForm.invalid) {
      return;
    }
    var roleDetails=
    {
      RoleId:this.RoleMasterForm.value.RoleId,
      RoleName:this.sharedService.ConvertNullToBlank(this.RoleMasterForm.value.RoleName),
      RoleDescription:this.sharedService.ConvertNullToBlank(this.RoleMasterForm.value.RoleDescription),
      LoggedInUserId: this.userService.GetUserId(Global.UserDetails)
    }
    this.subscriptions.push(
      this.mastersService.SaveRoleInformation(roleDetails).subscribe(
        response => {
          if (response.responseStatus) {
            switch (response.responseStatus) {
              case CrudImplementation.Create:
                this.SaveToasterMessage();
                this.ResetRoleMasterForm();
                this.GetAllRoles();
                break;
              case CrudImplementation.Update:
                this.GetAllRoles();
                this.GetAllRolesList();
                this.UpdateToasterMessage();
                break;
              case Status.Error:
                this.ErrorToasterMessage();
                break;
            }

          }
        },
        error => {
          this.ErrorToasterMessage();
          console.log('SaveRoleError', error)
        }

      )
    )
  }
  async DeleteRoleById(roleId) {
    var details=
    {
      DeleteRecordId:roleId,
      LoggedInUserId:this.userService.GetUserId(Global.UserDetails)
    }
    this.subscriptions.push(
      this.mastersService.DeleteRoleById(details).subscribe(
        response => {
          switch (response.responseStatus) {
            case CrudImplementation.Delete:
              this.GetAllRoles();
              this.DeleteToasterMessage();
              break;
            case Status.Error:
              this.ErrorToasterMessage();
              break;
          }
        },
        error => {
          this.ErrorToasterMessage();
          console.log('DeleteRoleError', error)
        }
      )
    )
  }  
  ResetRoleMasterForm() {
    this.RoleMasterForm.reset();
    this.submitted = false;
  }

  async GetRoleMasterForm() {
    this.ResetRoleMasterForm();
    this.GetRoleForm();
    this.RoleMasterForm.enable();
    this.roleTitle = await this.translateService.get('AddNewRole').toPromise();
  }
  GetAllRolesList() {
    this.showRoleMasterForm = false;
    this.showAllRoles = true;
    this.showAddButton = true;
    this.showAllRoleButton = false;
  }
  GetRoleForm() {
    this.showRoleMasterForm = true;
    this.showAllRoles = false;
    this.showAddButton = false;
    this.showAllRoleButton = true;
  }
  async ViewRoleMasterForm(roleInfo) {
    this.GetRoleForm();
    this.SetRoleMasterForm(roleInfo);
    this.RoleMasterForm.disable();
    this.showSaveResetButton = false;
    this.roleTitle = await this.translateService.get('ViewRecord').toPromise();

  }
  async EditRoleMasterForm(roleInfo) {
    this.GetRoleForm();
    this.SetRoleMasterForm(roleInfo);
    this.RoleMasterForm.enable();
    this.showSaveResetButton = true;
    this.roleTitle = await this.translateService.get('UpdateRecord').toPromise();
  }
  SetRoleMasterForm(roleInfo) {
    this.RoleMasterForm.patchValue(
      {
        RoleId: roleInfo.roleId,
        RoleName: roleInfo.roleName,
        RoleDescription: roleInfo.roleDescription,
      }
    );
  }


  async DeleteToasterMessage() {
    let title = await this.translateService.get('ToasterSuccess').toPromise();
    let message = await this.translateService.get('RecordDeleted').toPromise();
    this.toaster.SuccessToaster(message, title);
  }

  async SaveToasterMessage() {
    let title = await this.translateService.get('ToasterSuccess').toPromise();
    let message = await this.translateService.get('RecordSaved').toPromise();
    this.toaster.SuccessToaster(message, title);
  }
  async UpdateToasterMessage() {
    let title = await this.translateService.get('ToasterSuccess').toPromise();
    let message = await this.translateService.get('RecordUpdated').toPromise();
    this.toaster.SuccessToaster(message, title);
  }
  async ErrorToasterMessage() {
    let title = await this.translateService.get('ToasterError').toPromise();
    let message = await this.translateService.get('ToasterErrorMessage').toPromise();
    this.toaster.ErrorToaster(message, title);
  }

  ExportExcelVisibleData(table: Table) {
    let excelData: any = [];
    if (table && !!table.filteredValue) {
      excelData = table.filteredValue;
    }
    else {
      excelData = table._value;
    }
    if (excelData && excelData.length > 0) {
      this.CreateExcel(excelData);
    }
    else {
      this.NoRecordInfo();
    }
  }

  async CreateExcel(excelData: any) {
    let objData: any = [];
    let dataForExcel: any = [];
    excelData.forEach((value: any) => {
      objData.push({
        "Role Name": value.roleName, "Role Description": value.roleDescription
      })
    })
    objData.forEach((row: any) => {
      dataForExcel.push(Object.values(row));
    })
    let fileName = await this.translateService.get('AllRoles').toPromise();
    this.excelService.GenerateExcel(dataForExcel, Object.keys(objData[0]), fileName);
    let message = await this.translateService.get('ExcelGenerateSuccess').toPromise();
    let title = await this.translateService.get('ToasterSuccess').toPromise();
    this.toaster.SuccessToaster(message, title);
  }
  ExportPDFVisibleData(table: Table) {
    let excelData: any = [];
    if (table && !!table.filteredValue) {
      excelData = table.filteredValue;
    }
    else {
      excelData = table._value;
    }
    if (excelData && excelData.length > 0) {
      this.CreatePDF(excelData);
    }
    else {
      this.NoRecordInfo();
    }
  }
  async CreatePDF(pdfData) {
    let objData: any = [];
    let dataForPDF: any = [];
    pdfData.forEach((value: any) => {
      objData.push({
        "Role Name": value.roleName, "Role Description": value.roleDescription
      })
    })
    objData.forEach((row: any) => {
      dataForPDF.push(Object.values(row));
    })
    let fileName = await this.translateService.get('AllRoles').toPromise();
    this.exportToPDFService.GeneratePDF(Object.keys(objData[0]), dataForPDF, fileName);
    let message = await this.translateService.get('PdfGenerateSuccess').toPromise();
    let title = await this.translateService.get('ToasterSuccess').toPromise();
    this.toaster.SuccessToaster(message, title);
  }
  async NoRecordInfo() {
    let message = await this.translateService.get('NoRecordFound').toPromise();
    let title = await this.translateService.get('ToasterInfo').toPromise();
    this.toaster.ShowToasterInfo(message, title);
  }
  async DeleteRoleConfirmation(roleId: any) {
    let deleteConfirmHeader = await this.translateService.get('DeleteConfirmHeader').toPromise();
    let deleteConfirmMessage = await this.translateService.get('DeleteConfirmMessage').toPromise();
    this.confirmationService.confirm({
      message: deleteConfirmMessage,
      header: deleteConfirmHeader,
      icon: GenericConstants.DeleteConfirmationIcon,
      accept: () => {
        this.DeleteRoleById(roleId);
      },
      reject: () => {

      }
    });
  }
  HandleFirstPlaceSpace(event) {
    this.sharedService.HandleFirstPlaceSpace(event);
  }
  DenySpecialChar(event) {
    this.sharedService.DenySpecialCharacters(event);
  }
}
