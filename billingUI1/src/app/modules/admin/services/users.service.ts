import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EncryptionService } from '../../shared/services/encryption.service';
import { HttpService } from '../../shared/services/http.service';
import { AdminAPIAction, AdminApiController } from '../configuration/admin-api-configuration';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private httpService: HttpService,
    private encryptionService: EncryptionService) { }

  GetAllUsers(): Observable<any> {
    return this.httpService.GetApiReq(AdminApiController.Users, AdminAPIAction.GetAllUsers);
  }
  GetUserMasterData(): Observable<any> {
    return this.httpService.GetApiReq(AdminApiController.Users, AdminAPIAction.GetUserMasterData);
  }
  
  SaveUserInformation(userPersonalInfo): Observable<any> {
    return this.httpService.PostApiReq(AdminApiController.Users, AdminAPIAction.SaveUserInformation,
      this.encryptionService.EncryptObject(userPersonalInfo));
  } 
  

  DeleteUserById(userDetails): Observable<any> {
    return this.httpService.PostApiReq(AdminApiController.Users, AdminAPIAction.DeleteUserById, this.encryptionService.EncryptObject(userDetails));
  }
}
