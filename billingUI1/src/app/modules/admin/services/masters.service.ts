import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EncryptionService } from '../../shared/services/encryption.service';
import { HttpService } from '../../shared/services/http.service';
import { AdminAPIAction, AdminApiController } from '../configuration/admin-api-configuration';

@Injectable({
  providedIn: 'root'
})
export class MastersService {

  constructor(private httpService: HttpService,
    private encryptionService: EncryptionService) { }

  //Centre master
  GetAllCentres(): Observable<any> {
    return this.httpService.GetApiReq(AdminApiController.Master, AdminAPIAction.GetAllCentres);
  }

  SaveCentreInformation(centreInformation): Observable<any> {
    return this.httpService.PostApiReq(AdminApiController.Master, AdminAPIAction.SaveCentreInformation,
      this.encryptionService.EncryptObject(centreInformation));
  }
  DeleteCentreById(centreDetails): Observable<any> {
    return this.httpService.PostApiReq(AdminApiController.Master, AdminAPIAction.DeleteCentreById, this.encryptionService.EncryptObject(centreDetails));
  }
  // this.httpService.GetApiReq(AdminApiController.Master, AdminAPIAction.DeleteCentreById + "/" + centreId);
  //Role master
  GetAllRoles(): Observable<any> {
    return this.httpService.GetApiReq(AdminApiController.Master, AdminAPIAction.GetAllRoles);
  }

  SaveRoleInformation(roleInformation): Observable<any> {
    return this.httpService.PostApiReq(AdminApiController.Master, AdminAPIAction.SaveRoleInformation,
      this.encryptionService.EncryptObject(roleInformation));
  }
  DeleteRoleById(roleDetails): Observable<any> {
    return this.httpService.PostApiReq(AdminApiController.Master, AdminAPIAction.DeleteRoleById, this.encryptionService.EncryptObject(roleDetails));    
  }

  //Role master
  GetAllConfig(): Observable<any> {
    return this.httpService.GetApiReq(AdminApiController.Master, AdminAPIAction.GetAllConfig);
  }

  SaveConfigInformation(configInformation): Observable<any> {
    return this.httpService.PostApiReq(AdminApiController.Master, AdminAPIAction.SaveConfigInformation,
      this.encryptionService.EncryptObject(configInformation));
  }
  DeleteConfigById(configDetails): Observable<any> {
    return this.httpService.PostApiReq(AdminApiController.Master, AdminAPIAction.DeleteConfigById, this.encryptionService.EncryptObject(configDetails));    
  }

}


