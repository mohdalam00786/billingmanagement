import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './modules/shared/components/login/login.component';
import { AuthGuard } from './modules/shared/constants/auth-guard';

const routes: Routes = [
  {
    path: 'Login',
    component: LoginComponent
  },
  {
    path: '',
    redirectTo: 'Login',
    pathMatch: 'full'
  },
  // {
  //   path: '**',
  //   redirectTo: 'Login'
  // },  
  {
    path: 'Admin',
    canActivate: [AuthGuard],
    loadChildren: () => import('../app/modules/admin/admin.module').then(
      module => module.AdminModule),
  },
  {
    path: 'Procurement',
    canActivate: [AuthGuard],
    loadChildren: () => import('../app/modules/procurement/procurement.module').then(
      module => module.ProcurementModule
    )
  },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
